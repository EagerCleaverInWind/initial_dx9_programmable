#include "WaveParticles.h"
#include "Camera.h"

WaveParticleSystem::WaveParticleSystem():mNumVertsRow(128),mNumVertsCol(128),mGridStep(D3DXVECTOR2(20.0f,20.0f)),
	mMaxNumParticles(4000),mParticleRadiusL(64),mTime(0.0f)
{
	//实验性质
	e_pCamera->SetPosition(D3DXVECTOR3(300,750,0));

	// Allocate memory for maximum number of particles.
	mWaveParticles.resize(mMaxNumParticles);
	for(int i=0;i<mWaveParticles.size();i++)
		mDeadParticles.push_back(&mWaveParticles[i]);

	mHalfWidthWorld=(mNumVertsRow-1)*mGridStep.x/2.0f;
	mHalfDepthWorld=(mNumVertsCol-1)*mGridStep.y/2.0f;
	mHalfWidthGrid=(mNumVertsRow-1)/2.0f;
	mHalfHeightGrid=(mNumVertsCol-1)/2.0f;
	mNum2._i1=int(mParticleRadiusL/mGridStep.x);
	mNum2._i2=int(mParticleRadiusL/mGridStep.y);

	D3DXMatrixIdentity(&mMatWorld);
	GenGridMesh(mWater,mNumVertsCol,mNumVertsRow,mGridStep.x,mGridStep.y,false);

	mLight.ambient=D3DXCOLOR(0.55,0.55,0.55,1.0f);
	mLight.light=D3DXVECTOR4(1,-1,1,0.0f);

	CreateTexture();
	CreateQuad();
	CreateParticleVertex();
	BuildFX();
}


WaveParticleSystem::~WaveParticleSystem()
{
	SAFE_RELEASE(mPointsMapSurface);
	SAFE_RELEASE(mPointsMap);
	SAFE_RELEASE(mDisplacementMapSurface);
	SAFE_RELEASE(mDisplacementMap);
	SAFE_RELEASE(mNormalMapSurface);
	SAFE_RELEASE(mNormalMap);
	SAFE_RELEASE(mParticlesVB);
	SAFE_RELEASE(m_pDecl);
	SAFE_RELEASE(mFullScreenQuad);
	SAFE_RELEASE(mQuadDecl);
	SAFE_RELEASE(mBackBufferRenderTarget);
	SAFE_RELEASE(mEnvMap);
	SAFE_RELEASE(mWPFX);
}




void WaveParticleSystem::CreateQuad()
{
	e_pd3dDevice->CreateVertexDeclaration(VertexPTElements,&mQuadDecl);

	//full screen quad
	e_pd3dDevice->CreateVertexBuffer(4*sizeof(VertexPT),D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, &mFullScreenQuad, 0);
	VertexPT* vpt=nullptr;
	mFullScreenQuad->Lock(0,0,(void**)&vpt,0);
	vpt[0].pos=D3DXVECTOR3(-1,1,0);
	vpt[0].tex0=D3DXVECTOR2(0,0);
	vpt[1].pos=D3DXVECTOR3(1,1,0);
	vpt[1].tex0=D3DXVECTOR2(1,0);
	vpt[2].pos=D3DXVECTOR3(1,-1,0);
	vpt[2].tex0=D3DXVECTOR2(1,1);
	vpt[3].pos=D3DXVECTOR3(-1,-1,0);
	vpt[3].tex0=D3DXVECTOR2(0,1);
	mFullScreenQuad->Unlock();
}

void WaveParticleSystem::CreateParticleVertex()
{
	D3DVERTEXELEMENT9 ParticleElements[] = 
	{
		{0, 0,  D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		{0, 8, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
		{0, 16, D3DDECLTYPE_FLOAT1, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1},
		//{0, 20, D3DDECLTYPE_FLOAT1, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0},
		//color semantic会自动将负amptitude clamp to 0
		//{0, 20, D3DDECLTYPE_FLOAT1, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2},
		//特么开点精灵时TEXCOORD也会clamp到0~1！！！！
		{0, 20, D3DDECLTYPE_FLOAT1, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT, 0},
		//make it!		——》后来发现shader3.0以上版本COLOR也行了，但开点精灵时TEXCOORD还是会自动clamp
		D3DDECL_END()
	};	
	e_pd3dDevice->CreateVertexDeclaration(ParticleElements,&m_pDecl);

	e_pd3dDevice->CreateVertexBuffer(mMaxNumParticles*sizeof(WaveParticleVertex),
		D3DUSAGE_DYNAMIC|D3DUSAGE_WRITEONLY|D3DUSAGE_POINTS,
		0, D3DPOOL_DEFAULT, &mParticlesVB, 0);
}

void WaveParticleSystem::CreateTexture()
{
	e_pd3dDevice->GetRenderTarget(0,&mBackBufferRenderTarget);
	D3DXCreateTexture(e_pd3dDevice, mNumVertsRow, mNumVertsCol, 1, D3DUSAGE_RENDERTARGET, 
		D3DFMT_R32F, D3DPOOL_DEFAULT, &mPointsMap);
	mPointsMap->GetSurfaceLevel(0, &mPointsMapSurface);
	e_pd3dDevice->SetRenderTarget(0, mPointsMapSurface);
	e_pd3dDevice->Clear(0L, NULL, D3DCLEAR_TARGET, D3DCOLOR_RGBA(0, 0, 0, 0), 1.0f, 0L);
	e_pd3dDevice->SetRenderTarget(0, mBackBufferRenderTarget);

	D3DXCreateTexture(e_pd3dDevice, mNumVertsRow, mNumVertsCol, 1, D3DUSAGE_RENDERTARGET, 
		D3DFMT_A32B32G32R32F, D3DPOOL_DEFAULT, &mDisplacementMap);
	mDisplacementMap->GetSurfaceLevel(0, &mDisplacementMapSurface);
	D3DXCreateTexture(e_pd3dDevice, mNumVertsRow, mNumVertsCol, 1, D3DUSAGE_RENDERTARGET, 
		//D3DFMT_A8B8G8R8, D3DPOOL_DEFAULT, &mNormalMap);
		D3DFMT_A32B32G32R32F, D3DPOOL_DEFAULT, &mNormalMap);
	mNormalMap->GetSurfaceLevel(0, &mNormalMapSurface);

	D3DXCreateCubeTextureFromFile(e_pd3dDevice,"Texture/CubeMap/CubeSceneForWater.dds",&mEnvMap);

}

void WaveParticleSystem::BuildFX()
{
	ID3DXBuffer *errors;
	HR(D3DXCreateEffectFromFile(e_pd3dDevice, "shader/WaveParticles.fx",
		0, 0, D3DXSHADER_DEBUG, 0, &mWPFX, &errors));
	if(errors)
		MessageBox(0, (char*)errors->GetBufferPointer(), 0, 0);

	//point
	mhTime=mWPFX->GetParameterByName(0,"gTime");
	mhHalfW=mWPFX->GetParameterByName(0,"gHalfW");
	mWPFX->SetValue(mhHalfW,&D3DXVECTOR2(mHalfWidthWorld,mHalfDepthWorld),sizeof(D3DXVECTOR2));
	//filter
	mhRadiusL=mWPFX->GetParameterByName(0,"gRadiusL");
	mhGridStep=mWPFX->GetParameterByName(0,"gStepLength");
	mhNum=mWPFX->GetParameterByName(0,"gN");
	mhResolutionInv=mWPFX->GetParameterByName(0,"resolutionInverse");
	mhPointsTexture=mWPFX->GetParameterByName(0,"gPointsTex");
	mWPFX->SetFloat(mhRadiusL,mParticleRadiusL);
	mWPFX->SetValue(mhGridStep,&mGridStep,sizeof(D3DXVECTOR2));
	mWPFX->SetValue(mhNum,&mNum2,sizeof(mInt2));
	mWPFX->SetValue(mhResolutionInv,&D3DXVECTOR2(1.0f/mNumVertsCol,1.0f/mNumVertsCol),sizeof(D3DXVECTOR2));
	//normal
	mhDisplacementTex=mWPFX->GetParameterByName(0,"gDisplacementTex");
	//water surface
	mhWorld=mWPFX->GetParameterByName(0,"gWorld");
	mhWorldInvTrans=mWPFX->GetParameterByName(0,"gWorldInvTrans");
	mhWVP=mWPFX->GetParameterByName(0,"gWVP");
	mhLight=mWPFX->GetParameterByName(0,"gLight");
	mhMtrl=mWPFX->GetParameterByName(0,"gMtrl");
	mhEyePosW=mWPFX->GetParameterByName(0,"gEyePosW");
	mhNormalTex=mWPFX->GetParameterByName(0,"gDisplacementNormalTex");
	mhEnvMap=mWPFX->GetParameterByName(0,"gEnvMap");
	mWPFX->SetValue(mhLight,&mLight,sizeof(Light));
	mWPFX->SetValue(mhMtrl,&mMtrl,sizeof(Mtrl));
	mWPFX->SetMatrix(mhWorld,&mMatWorld);
	mWPFX->SetMatrix(mhWorldInvTrans,&mMatWorld);
	mWPFX->SetTexture(mhEnvMap,mEnvMap);
}



void WaveParticleSystem::GenerateOneParticle(D3DXVECTOR2 initialPostion,D3DXVECTOR2 velocity, float initialTime,float amptitude,float dispersionAngle)
{
	if(mDeadParticles.empty())
		return;

	WaveParticle* pNewParticle=mDeadParticles.back();
	pNewParticle->_initialPostion=initialPostion;
	pNewParticle->_velocity=velocity;
	pNewParticle->_initialTime=initialTime;
	pNewParticle->_amptitude=amptitude;
	pNewParticle->_dispersionAngle=dispersionAngle;
	mAliveParticles.push_back(pNewParticle);
	mDeadParticles.pop_back();

	float dSubdivisionTime=mParticleRadiusL/(2.0f*dispersionAngle*D3DXVec2Length(&velocity));
	std::list<WaveParticle*>::iterator position=mAliveParticles.end();
	position--;
	mSubdivisionTimetable.insert(TimetableElement(position, initialTime+dSubdivisionTime));
}

D3DXVECTOR2 WaveParticleSystem::Vec2RotationCW(D3DXVECTOR2 src,float angle)
{
	D3DXVECTOR2 out;
	out.x=cos(angle)*src.x-sin(angle)*src.y;
	out.y=sin(angle)*src.x+cos(angle)*src.y;
	return out;
}

void WaveParticleSystem::Update(float dt)
{
	mTime+=dt;
	mWPFX->SetFloat(mhTime,mTime);

	//while(mSubdivisionTimetable.begin()->_actTime<=mTime && !mSubdivisionTimetable.empty())
	while(!mSubdivisionTimetable.empty() && mSubdivisionTimetable.begin()->_actTime<=mTime)
	{
		std::list<WaveParticle*>::iterator currentPos=mSubdivisionTimetable.begin()->_position;
		mSubdivisionTimetable.erase(mSubdivisionTimetable.begin());
		WaveParticle* pCurrentParticle=*currentPos;
		mAliveParticles.erase(currentPos);
		//mDeadParticles.push_back(pCurrentParticle);
		//哈哈！这里有个陷阱！如果这么早push_back，GenerateOneParticle会覆盖掉原粒子数据
		float subAmptitude=pCurrentParticle->_amptitude/3.0f;
		//实验性质，出界了就让其消亡，不然粒子数不够用
		D3DXVECTOR2 position=pCurrentParticle->_initialPostion+(mTime-pCurrentParticle->_initialTime)*pCurrentParticle->_velocity;
		//if(subAmptitude>0.01f)
		//一开始忽略了负的情况，测试时负amptitude特么没反应。。。
		if (abs(subAmptitude)>0.01f && abs(position.x)<500 && abs(position.y)<500)
		{
			float subDispersionAngle=pCurrentParticle->_dispersionAngle/3.0f;
			GenerateOneParticle(pCurrentParticle->_initialPostion,pCurrentParticle->_velocity,
				pCurrentParticle->_initialTime,subAmptitude,subDispersionAngle);
			//CW
			GenerateOneParticle(pCurrentParticle->_initialPostion,Vec2RotationCW(pCurrentParticle->_velocity,subDispersionAngle),
				pCurrentParticle->_initialTime,subAmptitude,subDispersionAngle);
			//CCW
			GenerateOneParticle(pCurrentParticle->_initialPostion,Vec2RotationCW(pCurrentParticle->_velocity,-subDispersionAngle),
				pCurrentParticle->_initialTime,subAmptitude,subDispersionAngle);
		}
		mDeadParticles.push_back(pCurrentParticle);
	}

	//实验性质
	static bool isKeyDown=false;
	if(e_pDInput->IsKeyDown(DIK_P))
	{
		if(!isKeyDown)
		{
			isKeyDown=true;
			GenerateOneParticle(D3DXVECTOR2(0,0),D3DXVECTOR2(-100,0),mTime,-50.0f,2*D3DX_PI);
		}
	}
	else
		isKeyDown=false;

}

void WaveParticleSystem::Draw()
{
	UINT numPasses=0;

	//points
	mWPFX->SetTechnique("PointTech");
	e_pd3dDevice->SetRenderTarget(0,mPointsMapSurface);
	e_pd3dDevice->Clear(0,NULL,D3DCLEAR_TARGET,BLACK,1.0f,0);//一开始忘了clear，又开了add，amptitude突破天际。。。
	WaveParticleVertex* vp=0;
	mParticlesVB->Lock(0,0,(void**)&vp,D3DLOCK_DISCARD);
	int vbIndex=0;
	for(std::list<WaveParticle*>::iterator iter=mAliveParticles.begin();iter!=mAliveParticles.end();iter++)
	{
		vp[vbIndex]._amptitude=(*iter)->_amptitude;
		vp[vbIndex]._initialPosL=(*iter)->_initialPostion;
		vp[vbIndex]._initialTime=(*iter)->_initialTime;
		vp[vbIndex]._velocityL=(*iter)->_velocity;
		vbIndex++;
	}
	mParticlesVB->Unlock();
	if(vbIndex>0)
	{
		mWPFX->Begin(&numPasses,0);
		mWPFX->BeginPass(0);
		e_pd3dDevice->SetVertexDeclaration(m_pDecl);
		e_pd3dDevice->SetStreamSource(0,mParticlesVB,0,sizeof(WaveParticleVertex));
		e_pd3dDevice->DrawPrimitive(D3DPT_POINTLIST, 0, vbIndex);
		mWPFX->EndPass();
		mWPFX->End();
	}

	//filter
	mWPFX->SetTechnique("BidirectionalFilter");
	e_pd3dDevice->SetRenderTarget(0,mDisplacementMapSurface);
	mWPFX->SetTexture(mhPointsTexture,mPointsMap);
	mWPFX->Begin(&numPasses,0);
	mWPFX->BeginPass(0);
	e_pd3dDevice->SetVertexDeclaration(mQuadDecl);
	e_pd3dDevice->SetStreamSource(0,mFullScreenQuad,0,sizeof(VertexPT));
	e_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLEFAN, 0, 2);
	mWPFX->EndPass();
	mWPFX->End();

	//normal
	mWPFX->SetTechnique("CalculateNormal");
	e_pd3dDevice->SetRenderTarget(0,mNormalMapSurface);
	mWPFX->SetTexture(mhDisplacementTex,mDisplacementMap);
	mWPFX->Begin(&numPasses,0);
	mWPFX->BeginPass(0);
	e_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLEFAN, 0, 2);
	mWPFX->EndPass();
	mWPFX->End();

	//water surface
	mWPFX->SetTechnique("Water");
	e_pd3dDevice->SetRenderTarget(0,mBackBufferRenderTarget);
	mWPFX->SetTexture(mhNormalTex,mNormalMap);
	//note这里assume local space与world space重合，所以没更新world matrix
	D3DXMATRIX matViewProj;
	e_pCamera->GetViewProj(matViewProj);
	mWPFX->SetMatrix(mhWVP,&matViewProj);
	mWPFX->SetValue(mhEyePosW,&(e_pCamera->GetPosition()),sizeof(D3DXVECTOR3));
	mWPFX->Begin(&numPasses,0);
	mWPFX->BeginPass(0);
	mWater->DrawSubset(0);
	mWPFX->EndPass();
	mWPFX->End();

}

