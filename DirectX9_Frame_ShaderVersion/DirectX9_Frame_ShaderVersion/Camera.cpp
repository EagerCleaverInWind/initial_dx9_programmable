#include "Camera.h"
#include "ArcBall.h"

Camera* e_pCamera=0;

Camera::Camera()
{
	m_up=D3DXVECTOR3(0,1,0);
	m_look=D3DXVECTOR3(0,0,1);
	m_right=D3DXVECTOR3(1,0,0);
	m_position=D3DXVECTOR3(0,0,-250);
	CalculateViewMatrix();
	UpdateWorldFrustumPlanes();

	D3DXMatrixPerspectiveFovLH(&m_ProjMatrix,D3DX_PI/4.0f,(float)(WINDOW_WIDTH/WINDOW_HEIGHT),1.0f,20000.0f);
	//日了狗了，一开始错自动补全成 D3DXMatrixPerspectiveLH()，看东西成了扁的……
}

void	Camera::CalculateViewMatrix()
{
	//规范化m_up，m_look和m_right
	D3DXVec3Normalize(&m_look,&m_look);
	//LANDOBJECT左右转动后，”脱轨“的是UpVector:
	D3DXVec3Cross(&m_up,&m_look,&m_right);//注意D3D坐标系满足的是左手定则
	D3DXVec3Normalize(&m_up,&m_up);
	D3DXVec3Cross(&m_right,&m_up,&m_look);
	D3DXVec3Normalize(&m_right,&m_right);

	//第一行			RX	UX	LX	0
	m_ViewMatrix._11=m_right.x;			
	m_ViewMatrix._12=m_up.x;
	m_ViewMatrix._13=m_look.x;			
	m_ViewMatrix._14=0.0f;
	//第二行			RY	UY	LY		0
	m_ViewMatrix._21=m_right.y;
	m_ViewMatrix._22=m_up.y;	
	m_ViewMatrix._23=m_look.y;	
	m_ViewMatrix._24=0.0f;
	//第三行			RZ	UZ	LZ		0
	m_ViewMatrix._31=m_right.z;
	m_ViewMatrix._32=m_up.z;	
	m_ViewMatrix._33=m_look.z;	
	m_ViewMatrix._34=0.0f;
	//第四行			-P*R	-P*U	-P*L	1
	m_ViewMatrix._41=-D3DXVec3Dot(&m_position,&m_right);
	m_ViewMatrix._42=-D3DXVec3Dot(&m_position,&m_up);
	m_ViewMatrix._43=-D3DXVec3Dot(&m_position,&m_look);
	m_ViewMatrix._44=1.0f;
}

void Camera::UpdateWorldFrustumPlanes()
{
	D3DXMATRIX VP=m_ViewMatrix*m_ProjMatrix;

	//Extract the frustum planes in world space from VP
	D3DXVECTOR4 col0(VP(0,0), VP(1,0), VP(2,0), VP(3,0));
	D3DXVECTOR4 col1(VP(0,1), VP(1,1), VP(2,1), VP(3,1));
	D3DXVECTOR4 col2(VP(0,2), VP(1,2), VP(2,2), VP(3,2));
	D3DXVECTOR4 col3(VP(0,3), VP(1,3), VP(2,3), VP(3,3));
	// Planes face inward.
	m_FrustumPlanes[0] = (D3DXPLANE)(col2);        // near
	m_FrustumPlanes[1] = (D3DXPLANE)(col3 - col2); // far
	m_FrustumPlanes[2] = (D3DXPLANE)(col3 + col0); // left
	m_FrustumPlanes[3] = (D3DXPLANE)(col3 - col0); // right
	m_FrustumPlanes[4] = (D3DXPLANE)(col3 - col1); // top
	m_FrustumPlanes[5] = (D3DXPLANE)(col3 + col1); // bottom

	for(int i = 0; i < 6; i++)
		D3DXPlaneNormalize(&m_FrustumPlanes[i], &m_FrustumPlanes[i]);
}


void	Camera::SetPosition(D3DXVECTOR3	new_position)	
{
	m_position=new_position;
	CalculateViewMatrix();
	UpdateWorldFrustumPlanes();
}

void	Camera::SetLens(float fov, float aspect, float nearZ, float farZ)
{
	D3DXMatrixPerspectiveFovLH(&m_ProjMatrix,fov,aspect,nearZ,farZ);
}

void Camera::LookAt(D3DXVECTOR3& pos, D3DXVECTOR3& target, D3DXVECTOR3& up)
{
	m_position=pos;
	m_look=target-pos;
	D3DXVec3Normalize(&m_look,&m_look);
	D3DXVec3Cross(&m_right,&m_up,&m_look);

	CalculateViewMatrix();
	UpdateWorldFrustumPlanes();
}


bool Camera::IsVisible(const BoundingBox &box)
{
	// Test assumes frustum planes face inward.

	D3DXVECTOR3 P;
	D3DXVECTOR3 Q;

	// PQ forms diagonal most closely aligned with plane normal.
	// For each frustum plane, find the box diagonal (there are four main
	// diagonals that intersect the box center point) that points in the
	// same direction as the normal along each axis (i.e., the diagonal 
	// that is most aligned with the plane normal).  Then test if the box
	// is in front of the plane or not.
	for(int i = 0; i < 6; ++i)
	{
		// For each coordinate axis x, y, z...
		for(int j = 0; j < 3; ++j)
		{
			// Make PQ point in the same direction as the plane normal on this axis.
			if( m_FrustumPlanes[i][j] >= 0.0f )
			{
				P[j] = box._min[j];
				Q[j] = box._max[j];
			}
			else 
			{
				P[j] = box._max[j];
				Q[j] = box._min[j];
			}
		}

		// If box is in negative half space, it is behind the plane, and thus, completely
		// outside the frustum.  Note that because PQ points roughly in the direction of the 
		// plane normal, we can deduce that if Q is outside then P is also outside--thus we
		// only need to test Q.
		if( D3DXPlaneDotCoord(&m_FrustumPlanes[i], &Q) < 0.0f  ) // outside
			return false;
	}
	return true;
}

Camera::~Camera()
{

}

//-----------------------------------【air craft】---------------------------------------
void	FPAirCamera::Update(float dt,Terrain* terrain)
{
	//平移
	D3DXVECTOR3 direction=D3DXVECTOR3(0,0,0);
	if(e_pDInput->IsKeyDown(DIK_W))
		direction+=m_look;
	if(e_pDInput->IsKeyDown(DIK_S))
		direction-=m_look;
	if(e_pDInput->IsKeyDown(DIK_A))
		direction-=m_right;
	if(e_pDInput->IsKeyDown(DIK_D))
		direction+=m_right;
	D3DXVec3Normalize(&direction,&direction);
	m_position+=direction*m_speed*dt;
	//升降
	if(e_pDInput->IsKeyDown(DIK_SPACE))
	{
		if(e_pDInput->IsKeyDown(DIK_LSHIFT))
			m_position.y-=m_speed*dt;
		else
			m_position.y+=m_speed*dt;
	}
	//旋转
	D3DXMATRIX  R;

	float angleZ=0.0f;
	if(e_pDInput->IsKeyDown(DIK_Q))
		angleZ+=0.001f;
	if(e_pDInput->IsKeyDown(DIK_E))
		angleZ-=0.001f;
	D3DXMatrixRotationAxis(&R,&m_look,angleZ);
	D3DXVec3TransformNormal(&m_up,&m_up,&R);
	D3DXVec3TransformNormal(&m_right,&m_right,&R);
	if(e_pDInput->MouseButtonDown(MOUSERIGHTBUTTON))
	{
		float angleY=e_pDInput->MouseDX()/150.0f;
		float angleX=e_pDInput->MouseDY()/150.0f;
		//up axis
		D3DXMatrixRotationAxis(&R,&m_up,angleY);
		D3DXVec3TransformNormal(&m_right,&m_right,&R);
		D3DXVec3TransformNormal(&m_look,&m_look,&R);
		//right axis
		D3DXMatrixRotationAxis(&R,&m_right,angleX);
		D3DXVec3TransformNormal(&m_up,&m_up,&R);
		D3DXVec3TransformNormal(&m_look,&m_look,&R);
	}

	CalculateViewMatrix();
	UpdateWorldFrustumPlanes();
}
//------------------------------------------------------------------------------------------------    



//-----------------------------------【land object】---------------------------------------
void FPLandCamera::Update(float dt,Terrain* terrain)
{
	//平移
	D3DXVECTOR3 direction=D3DXVECTOR3(0,0,0);
	if(e_pDInput->IsKeyDown(DIK_W))
		direction+=m_look;
	if(e_pDInput->IsKeyDown(DIK_S))
		direction-=m_look;
	if(e_pDInput->IsKeyDown(DIK_A))
		direction-=m_right;
	if(e_pDInput->IsKeyDown(DIK_D))
		direction+=m_right;
	D3DXVec3Normalize(&direction,&direction);
	D3DXVECTOR3 newPos = m_position + direction*m_speed*dt;

	if( terrain != 0)
	{
		// New position might not be on terrain, so project the
		// point onto the terrain.
		float ref= terrain->getHeight(newPos.x, newPos.z) + m_height;
		if(newPos.y < ref)
		{
			newPos.y=ref;
		}

		// Now the difference of the new position and old (current) 
		// position approximates a tangent vector on the terrain.
		D3DXVECTOR3 tangent = newPos - m_position;
		D3DXVec3Normalize(&tangent, &tangent);

		if(e_pDInput->IsKeyDown(DIK_SPACE)&&!m_isJump)
		{
			m_isJump=true;
			m_Vy=D3DXVECTOR3(0,m_jumpPower,0);
		}

		D3DXVECTOR3 averageVy=D3DXVECTOR3(0,0,0);
		if(m_isJump)
		{
			averageVy=m_Vy;
			m_Vy+=GRAVITY_ACCERATION*dt;
			averageVy=0.5*(averageVy+m_Vy);
		}

		// Now move camera along tangent vector.
		m_position +=( tangent*m_speed+averageVy)*dt;

		// After update, there may be errors in the camera height since our
		// tangent is only an approximation.  So force camera to correct height,
		// and offset by the specified amount so that camera does not sit
		// exactly on terrain, but instead, slightly above it.
		//m_position.y = terrain->getHeight(m_position.x, m_position.z) + m_height;
		float ref2= terrain->getHeight(m_position.x, m_position.z) + m_height;
		if(m_position.y<=ref2)
		{
			m_isJump=false;
			m_Vy.y=0.0f;
			m_position.y=ref2;
		}
		else
		{
			m_isJump=true;
		}


	}
	else
	{
		m_position = newPos;
	}
	//旋转
	D3DXMATRIX  R;
	if(e_pDInput->MouseButtonDown(MOUSERIGHTBUTTON))
	{
		float angleY=e_pDInput->MouseDX()/150.0f;
		float angleX=e_pDInput->MouseDY()/150.0f;
		//not up but y axis
		D3DXMatrixRotationY(&R,angleY);
		D3DXVec3TransformNormal(&m_right,&m_right,&R);
		D3DXVec3TransformNormal(&m_look,&m_look,&R);
		//right axis
		D3DXMatrixRotationAxis(&R,&m_right,angleX);
		D3DXVec3TransformNormal(&m_up,&m_up,&R);
		D3DXVec3TransformNormal(&m_look,&m_look,&R);
	}

	CalculateViewMatrix();
	UpdateWorldFrustumPlanes();
}
//------------------------------------------------------------------------------------------------    


//-----------------------------------【Arcball show】---------------------------------------
ArcballCamera::ArcballCamera(float distance,D3DXVECTOR3 target)
	:mRotateObject(false),mRotateCamera(false),mDistance(distance),mTarget(target),
	mWindowWidth(WINDOW_WIDTH),mWindowHeight(WINDOW_HEIGHT)
{
	D3DXQuaternionIdentity(&mCamCurrentQuaternion);
	D3DXQuaternionIdentity(&mObjCurrentQuaternion);
	CalculateCamera();
	CalculateObject();
}

void ArcballCamera::CalculateCamera()
{
	D3DXMATRIX rotate;
	//D3DXQuaternionInverse(&mCamCurrentQuaternion,&mCamCurrentQuaternion);
	//本来是为了抵消后面MatrixInverse引起的旋转反向，但是每次松开鼠标右键时却会出现问题，因为不能反整个，保留前面继承的，只反后来新生成的那一段旋转
	D3DXMatrixRotationQuaternion(&rotate,&mCamCurrentQuaternion);
	D3DXMatrixInverse(&rotate,NULL,&rotate);//求个逆矩阵就解决了camera的arcball旋转从view space到world space的差异性问题，为什么？！
																	//为什么用直接转换arcball上点(V到W)的方法对object有用对camera却旋转过快？！

	D3DXVECTOR3 up(0,1,0);
	D3DXVec3TransformNormal(&m_up,&up,&rotate);
	D3DXVECTOR3 look(0,0,1);
	D3DXVec3TransformNormal(&m_look,&look,&rotate);
	m_position=mTarget-m_look*mDistance;
	D3DXMatrixLookAtLH(&m_ViewMatrix,&m_position,&mTarget,&m_up);

	//每次更新完m_ViewMatrix后，都要更新mViewInverse for object's arcball：
	//D3DXMatrixInverse(&mViewInverse,0,&m_ViewMatrix);
	//要保证arcball上的点转换后依然在arcball上，所以不能包括位移部分，需要转换的只是旋转部分
	D3DXMATRIX viewR;
	D3DXMatrixIdentity(&viewR);
	viewR._11=m_ViewMatrix._11;viewR._12=m_ViewMatrix._12;viewR._13=m_ViewMatrix._13;
	viewR._21=m_ViewMatrix._21;viewR._22=m_ViewMatrix._22;viewR._23=m_ViewMatrix._23;
	viewR._31=m_ViewMatrix._31;viewR._32=m_ViewMatrix._32;viewR._33=m_ViewMatrix._33;
	D3DXMatrixInverse(&mViewInverse,0,&viewR);
}

void ArcballCamera::CalculateObject()
{
	D3DXMATRIX T;
	D3DXMatrixTranslation(&T,mTarget.x,mTarget.y,mTarget.z);
	D3DXMATRIX R;
	D3DXMatrixRotationQuaternion(&R,&mObjCurrentQuaternion);
	mObjMatWorld=R*T;

}


void ArcballCamera::Update(float dt,Terrain* terrain)
{
	RECT rect;
	GetWindowRect(e_hMainWnd,&rect);
	mWindowWidth=rect.right;
	mWindowHeight=rect.bottom;

	UpdateWorldFrustumPlanes();//一开始忘了这个，看到了裁剪边角。。。
}


void  ArcballCamera::HandleMessages(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam)
{
	int mouseX=(short)LOWORD(lParam);
	int mouseY=(short)HIWORD(lParam);
	switch(message)
	{
		//for camera
	case WM_RBUTTONDOWN:
		if(mouseX>0&&mouseX<mWindowWidth&&mouseY>0&&mouseY<mWindowHeight)
		{
			mRotateCamera=true;
			mCamBeginPoint=e_pArcball->ScreenToBall(mouseX,mouseY);//in view space
			//D3DXVec3TransformCoord(&mCamBeginPoint,&mCamBeginPoint,&mViewInverse);//in world space
			//不知道为什么这样相机会旋转得非常快，然后又不好缩小旋转角度。。。
			mCamLastQuaternion=mCamCurrentQuaternion;
			SetCapture(hwnd);
		}
		break;

	case WM_RBUTTONUP:
		mRotateCamera=false;
		ReleaseCapture();
		break;

	case WM_MOUSEWHEEL:
		mDistance-=short(HIWORD(wParam))*mDistance/3600.0f;
		mDistance=max(mDistance,MIN_DISTANCE);
		mDistance=min(mDistance,MAX_DISTANCE);
		CalculateCamera();
		break;

		//for object
	case WM_LBUTTONDOWN:
		if(mouseX>0&&mouseX<mWindowWidth&&mouseY>0&&mouseY<mWindowHeight)
		{
			mRotateObject=true;
			mObjBeginPoint=e_pArcball->ScreenToBall(mouseX,mouseY);//in view space
			D3DXVec3TransformCoord(&mObjBeginPoint,&mObjBeginPoint,&mViewInverse);//in world space
			mObjLastQuaternion=mObjCurrentQuaternion;
			SetCapture(hwnd);
		}
		break;

	case WM_LBUTTONUP:
		mRotateObject=false;
		ReleaseCapture();
		break;

		//for both
	case WM_MOUSEMOVE:
		if(mRotateCamera)
		{
			mCamCurrentPoint=e_pArcball->ScreenToBall(mouseX,mouseY);//in view space
			//D3DXVec3TransformCoord(&mCamCurrentPoint,&mCamCurrentPoint,&mViewInverse);//in world space

			//mCamCurrentQuaternion=e_pArcball->QuatFromBallPoints(mCamBeginPoint,mCamCurrentPoint)*mCamLastQuaternion;
			//会出错，因为Q1*Q2其实表示的就是Q2Q1
			mCamCurrentQuaternion=mCamLastQuaternion*e_pArcball->QuatFromBallPoints(mCamBeginPoint,mCamCurrentPoint);
			//如果要旋转反向的话：
			//D3DXQUATERNION Q=e_pArcball->QuatFromBallPoints(mCamBeginPoint,mCamCurrentPoint);
			//D3DXQuaternionInverse(&Q,&Q);
			//mCamCurrentQuaternion=mCamLastQuaternion*Q;
			CalculateCamera();
		}
		if(mRotateObject)
		{
			mObjCurrentPoint=e_pArcball->ScreenToBall(mouseX,mouseY);//in view space
			D3DXVec3TransformCoord(&mObjCurrentPoint,&mObjCurrentPoint,&mViewInverse);//in world space
			mObjCurrentQuaternion=mObjLastQuaternion*e_pArcball->QuatFromBallPoints(mObjBeginPoint,mObjCurrentPoint);
			CalculateObject();
		}
		//break;
	}

}


void ArcballCamera::SetTarget(D3DXVECTOR3 target)
{
	mTarget=target;
	D3DXQuaternionIdentity(&mCamCurrentQuaternion);
	D3DXQuaternionIdentity(&mObjCurrentQuaternion);
	CalculateCamera();
	CalculateObject();
}
//------------------------------------------------------------------------------------------------    