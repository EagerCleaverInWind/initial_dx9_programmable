#pragma  once
#include "Utility.h"
#include "Camera.h"


struct Particle
{
	D3DXVECTOR3 initialPos;
	D3DXVECTOR3 initialVelocity;
	float       initialSize; // In pixels after multiplied by viewport size
	float       initialTime;
	float       lifeTime;
	float       mass;
	D3DCOLOR    initialColor;
};
//-----------------------------------【basic class】---------------------------------------
class PSystem
{
protected:
	LPDIRECT3DVERTEXBUFFER9 m_pVB;
	DWORD	mVBSize;
	DWORD	mVBOffset;
	DWORD	mVBBatchSize;
	IDirect3DVertexDeclaration9* m_pDecl;

	ID3DXEffect* m_pFX;
	D3DXHANDLE mhTech;
	D3DXHANDLE mhWVP;
	D3DXHANDLE mhEyePosL;
	D3DXHANDLE mhTex;
	D3DXHANDLE mhTime;
	D3DXHANDLE mhAccel;
	D3DXHANDLE mhViewportHeight;
	LPDIRECT3DTEXTURE9 m_pTexture;
	D3DXMATRIX mWorldMat;
	D3DXMATRIX mInvWorld;
	float mTime;
	D3DXVECTOR3 mAccel;

	BoundingBox mSystemBox;

	int mMaxNumParticles;
	float mTimePerParticle;

	std::vector<Particle> mParticles;
	std::vector<Particle*> mAliveParticles;
	std::vector<Particle*> mDeadParticles; 


public:
	PSystem(	
		const std::string& fxName, //除非是模版化的.fx,一般情况下效果文件不好当参数传进来（因为数据传输的不确定），只好单一地内定
		const std::string& techName, 
		const std::string& texName, 
		const D3DXVECTOR3& accel, 
		const BoundingBox& box,
		int maxNumParticles,
		float timePerParticle);

	~PSystem();

public:
	virtual void InitialParticle(Particle& out)=0;
	void AddParticle();

	void SetWorldMatrix(D3DXMATRIX& world);
	void SetTime(float time) { mTime=time; }//意义在于通过配合SetWorldMatrix()以及Update(0)的使用，
															//能让Psystem just one instance serves as a multitude,analogous to one mesh,one boundingbox etc.

	virtual void Update(float dt);
	virtual void Draw(HWND hwnd);

};
//------------------------------------------------------------------------------------------------    

//-----------------------------------【Rain】---------------------------------------
class Rain:public PSystem
{
public:
	Rain(const std::string& fxName, 
		const std::string& techName, 
		const std::string& texName, 
		const D3DXVECTOR3& accel, 
		int maxNumParticles,
		float timePerParticle):PSystem(fxName,techName,texName,accel,
		BoundingBox(D3DXVECTOR3(-INFINITY,-INFINITY,-INFINITY),D3DXVECTOR3(INFINITY,INFINITY,INFINITY)),
		maxNumParticles,timePerParticle)
	{

	}

	void InitialParticle(Particle& out);

};

//------------------------------------------------------------------------------------------------    


//-----------------------------------【snow】---------------------------------------
class Snow:public PSystem
{
public:
	Snow(
		const std::string& fxName, 
		const std::string& techName, 
		const std::string& texName, 
		int maxNumParticles,
		float timePerParticle
		):PSystem(fxName,techName,texName,
		D3DXVECTOR3(0,0,0),//雪花飘落很快达到匀速阶段
		BoundingBox(D3DXVECTOR3(-INFINITY,-INFINITY,-INFINITY),D3DXVECTOR3(INFINITY,INFINITY,INFINITY)),
		maxNumParticles,timePerParticle)
	{
		
	}

	void InitialParticle(Particle&out);
	void Update(float dt);
};
//------------------------------------------------------------------------------------------------    


//-----------------------------------【FireWork】---------------------------------------
class FireWork:public PSystem
{
private:
	D3DXVECTOR3 mInitialPoint;

private:
	bool IsDead(){return mAliveParticles.empty();}
	void Reset();


public:
	FireWork(
		const std::string& fxName, 
		const std::string& techName, 
		const std::string& texName, 
		int maxNumParticles,
		D3DXVECTOR3& initialPoint
		):PSystem(fxName,techName,texName,
		D3DXVECTOR3(0,0,0),//暂且匀速吧
		BoundingBox(D3DXVECTOR3(-INFINITY,-INFINITY,-INFINITY),D3DXVECTOR3(INFINITY,INFINITY,INFINITY)),
		maxNumParticles,-1),	mInitialPoint(initialPoint)
	{
		Reset();
	}


public:
	void InitialParticle(Particle& out);
	void Update(float dt);
};
//------------------------------------------------------------------------------------------------    
//-----------------------------------【Trail】---------------------------------------
typedef void (*FunType)(D3DXVECTOR3& ,float );//定义特定函数的指针类型
void HelixL(D3DXVECTOR3& p,float dt );//螺旋，待向类中再添加一个动点，还原笛子魔童的“魔贯光杀炮”哈哈！
void HelixS(D3DXVECTOR3& p,float dt );
void HelixCircle(D3DXVECTOR3& p,float dt);
class Trail:public PSystem
{
private: 
	D3DXVECTOR3 mPoint;
	FunType mPointUpdate;//声明一个函数指针，轨迹关于时间t的参数方程
public:
	Trail(
		FunType f,
		const std::string& fxName, 
		const std::string& techName, 
		const std::string& texName, 
		int maxNumParticles,
		float timePerParticle
		):PSystem(fxName,techName,texName,
		D3DXVECTOR3(0,0,0),//动点周围溢出的粒子基本不运动
		BoundingBox(D3DXVECTOR3(-INFINITY,-INFINITY,-INFINITY),D3DXVECTOR3(INFINITY,INFINITY,INFINITY)),
		maxNumParticles,timePerParticle),
		mPointUpdate(f),mPoint(D3DXVECTOR3(0,0,0))
	{

	}

		void InitialParticle(Particle& out);
		void Update(float dt);

};
//------------------------------------------------------------------------------------------------    