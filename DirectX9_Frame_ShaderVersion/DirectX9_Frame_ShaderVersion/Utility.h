#pragma  once

#include <d3d9.h>
#include <d3dx9.h>
#include <tchar.h>
#include <stdio.h>
#include <vector>

#define  WINDOW_WIDTH 800
#define  WINDOW_HEIGHT 600

#define  SAFE_RELEASE(p) { if(p)		{ (p)->Release();		(p)=NULL; }  }
#define  SAFE_DELETE(p) { if(p)		{ delete(p);	(p)=NULL; }  }

#define HR(x) x;

extern LPDIRECT3DDEVICE9	e_pd3dDevice;
extern HWND e_hMainWnd;

//-----------------------------------【Vertex】---------------------------------------
struct VertexPNT
{
	D3DXVECTOR3 pos;
	D3DXVECTOR3 normal;
	D3DXVECTOR2 tex0;
};
extern D3DVERTEXELEMENT9 VertexPNTElements[];

struct VertexPT
{
	D3DXVECTOR3 pos;
	D3DXVECTOR2 tex0;
};
extern D3DVERTEXELEMENT9 VertexPTElements[];

struct VertexPN
{
	D3DXVECTOR3 pos;
	D3DXVECTOR3 normal;
};
extern D3DVERTEXELEMENT9 VertexPNElements[];
//------------------------------------------------------------------------------------------------    



//===============================================================
// Math 

const float INFINITY = FLT_MAX;
const float EPSILON  = 0.001f;

DWORD FtoDw(float f);

float GetRandomFloat(float lowBound,float highBound);
void GetRandomVector(D3DXVECTOR3& out,D3DXVECTOR3& min,D3DXVECTOR3& max);
//================================================================

//-----------------------------------【Physics】---------------------------------------
const D3DXVECTOR3 GRAVITY_ACCERATION=D3DXVECTOR3(0,-500,0);
//------------------------------------------------------------------------------------------------    

//===============================================================
// Colors and Materials
const D3DXCOLOR WHITE(1.0f, 1.0f, 1.0f, 1.0f);
const D3DXCOLOR BLACK(0.0f, 0.0f, 0.0f, 1.0f);
const D3DXCOLOR RED(1.0f, 0.0f, 0.0f, 1.0f);
const D3DXCOLOR GREEN(0.0f, 1.0f, 0.0f, 1.0f);
const D3DXCOLOR BLUE(0.0f, 0.0f, 1.0f, 1.0f);

//本来想加个const，但是只有变量才能先声明后定义,况且它需要在main文件中真正意义上初始化（修改填充）
extern IDirect3DTexture9* e_pWhiteTexture;

struct Mtrl
{
	Mtrl()
		:ambient(WHITE), diffuse(WHITE), spec(WHITE), specPower(8.0f){}
	Mtrl(const D3DXCOLOR& a, const D3DXCOLOR& d, 
		const D3DXCOLOR& s, float power)
		:ambient(a), diffuse(d), spec(s), specPower(power){}

	D3DXCOLOR ambient;
	D3DXCOLOR diffuse;
	D3DXCOLOR spec;
	float specPower;
};

struct DirLight
{
	D3DXCOLOR ambient;
	D3DXCOLOR diffuse;
	D3DXCOLOR spec;
	D3DXVECTOR3 dirW;
};

struct SpotLight
{
	D3DXCOLOR ambient;
	D3DXCOLOR diffuse;
	D3DXCOLOR spec;
	D3DXVECTOR3 posW;
	D3DXVECTOR3 dirW;  
	float  spotPower;
};

struct PointLight
{
	D3DXCOLOR ambient;
	D3DXCOLOR diffuse;
	D3DXCOLOR spec;
	D3DXVECTOR3 posW;
	float distAttenuation;
};

struct Light
{
	Light()
		:ambient(D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f)),
		diffuse(D3DXCOLOR(0.8f, 0.8f, 0.8f, 1.0f)), spec(D3DXCOLOR(0.8f, 0.8f, 0.8f, 1.0f)), 
		light(D3DXVECTOR4(1,-2,-1,0)){}

	D3DXCOLOR ambient;
	D3DXCOLOR diffuse;
	D3DXCOLOR spec;
	D3DXVECTOR4 light;
};
//===============================================================
// .X Files



//-----------------------------------【Bounding】---------------------------------------
struct BoundingBox
{
	BoundingBox(){}
	BoundingBox(D3DXVECTOR3 min,D3DXVECTOR3 max):_min(min),_max(max),_center((min+max)*0.5){}

	bool IfPointInside(D3DXVECTOR3 &p);
	void LocalToWorld(BoundingBox &out,D3DXMATRIX &worldMat);

	D3DXVECTOR3 _min;
	D3DXVECTOR3 _max;
	D3DXVECTOR3 _center;
};
struct BoundingSphere
{

	D3DXVECTOR3		_center;
	float _radius;
};
//------------------------------------------------------------------------------------------------    



//-----------------------------------【Screen】---------------------------------------
void	ScreenShot();

void ShowFPS(LPD3DXFONT	pFont,D3DCOLOR fpsColor,int windowWidth,int windowHeight,float _dt,bool _ifShowMspf=false);

void GetWorldPickingRay(D3DXVECTOR3& originW, D3DXVECTOR3& dirW,D3DXMATRIX &proj,D3DXMATRIX &view);
//------------------------------------------------------------------------------------------------    



//-----------------------------------【Grid & Mesh】---------------------------------------
void GenTriGrid(int numVertRows, int numVertCols,
				float dx, float dz, 
				const D3DXVECTOR3& center, 
				std::vector<D3DXVECTOR3>& verts,
				std::vector<DWORD>& indices);

void GenGridMesh(ID3DXMesh* &pMesh,
				 int numVertRows, int numVertCols,
				 float dx, float dz,bool hasNormal,bool ifOptimize=true);//注意优化会修改顶点缓存！

void LoadXFile(
	const std::string& filename, 
	ID3DXMesh** meshOut,
	std::vector<Mtrl>& mtrls, 
	std::vector<IDirect3DTexture9*>& texs);
//------------------------------------------------------------------------------------------------    



//-----------------------------------【 Table Template 】---------------------------------------
//vector<T>拓为Table<T>
template <typename T>
class Table
{
public:
	Table()
		: mRows(0), mCols(0)
	{
	}

	Table(int m, int n)
		: mRows(m), mCols(n), mMatrix(m*n)
	{
	}

	Table(int m, int n, const T& value)
		: mRows(m), mCols(n), mMatrix(m*n, value)
	{
	}

	// For non-const objects
	T& operator()(int i, int j)
	{
		return mMatrix[i*mCols+j];
	}

	// For const objects
	const T& operator()(int i, int j)const
	{
		return mMatrix[i*mCols+j];
	}

	// Add typename to let compiler know type and not static variable.
	typedef typename std::vector<T>::iterator iter;
	typedef typename std::vector<T>::const_iterator citer;

	// For non-const objects
	iter begin(){ return mMatrix.begin(); }
	iter end()	{ return mMatrix.end();   }

	// For const objects
	citer begin() const { return mMatrix.begin(); }
	citer end() const { return mMatrix.end();   }

	int numRows() const	{ return mRows;	}
	int numCols() const	{ return mCols;	}

	void resize(int m, int n)
	{
		mRows = m;
		mCols = n;
		mMatrix.resize(m*n);
	}

	void resize(int m, int n, const T& value)
	{
		mRows = m;
		mCols = n;
		mMatrix.resize(m*n, value);
	}

private:
	int mRows;
	int mCols;
	std::vector<T> mMatrix;
};
//------------------------------------------------------------------------------------------------    