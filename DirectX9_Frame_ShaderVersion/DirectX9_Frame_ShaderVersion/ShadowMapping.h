#pragma  once
#include "Utility.h"
#include "RenderToTexture.h"

//-----------------------------------【shadow】---------------------------------------
//说明:	与其说是类，不如说是个demo，耦合性简直不要太大，到底怎么设计才能支持灵活地往场景中增减物体呢？！
//绘制时使用函数指针或抽象类能一定程度降低耦合


class ShadowMap
{
protected:
	DrawableTex2D* mShadowMap;

	D3DXMATRIX mLightVP;

	ID3DXEffect*	mFX;
	D3DXHANDLE   mhBuildShadowMapTech;
	D3DXHANDLE   mhLightWVP;

	D3DXHANDLE   mhTech;
	D3DXHANDLE   mhWVP;
	D3DXHANDLE   mhEyePosW;
	D3DXHANDLE   mhWorld;
	D3DXHANDLE   mhTex;
	D3DXHANDLE   mhShadowMap;
	D3DXHANDLE   mhMtrl;
	D3DXHANDLE   mhLight;

	SpotLight mLight;


protected:
	void drawShadowMap();
	void buildFX();

	virtual void DrawObjectsToShadowMap()=0;
	virtual void DrawObjectsToBackBuffer()=0;
public:
	ShadowMap();
	~ShadowMap();

	void drawScene();
};
//------------------------------------------------------------------------------------------------    




//-----------------------------------【demo】---------------------------------------
class ShadowMapDemo:public ShadowMap
{
private:
	ID3DXMesh* mSceneMesh;
	D3DXMATRIX mSceneWorld;
	std::vector<Mtrl> mSceneMtrls;
	std::vector<IDirect3DTexture9*> mSceneTextures;

	ID3DXMesh* mCarMesh;
	D3DXMATRIX mCarWorld;
	std::vector<Mtrl> mCarMtrls;
	std::vector<IDirect3DTexture9*> mCarTextures;

	IDirect3DTexture9* mWhiteTex;

private:
	void DrawObjectsToShadowMap();
	void DrawObjectsToBackBuffer();

public:
	ShadowMapDemo();
	~ShadowMapDemo();

	void Draw();
	void Update(float dt);
};

//------------------------------------------------------------------------------------------------    