#include "Utility.h"
#include <string>//for std::string::operator+

LPDIRECT3DDEVICE9	e_pd3dDevice=0;
HWND e_hMainWnd;

IDirect3DTexture9* e_pWhiteTexture=0;

//-----------------------------------【Vertex】---------------------------------------
D3DVERTEXELEMENT9 VertexPNTElements[]=
{
	{0, 0,  D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
	{0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
	{0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
	D3DDECL_END()
};

D3DVERTEXELEMENT9 VertexPTElements[]=
{
	{0, 0,  D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
	{0, 12, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
	D3DDECL_END()
};

D3DVERTEXELEMENT9 VertexPNElements[]=
{
	{0, 0,  D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
	{0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
	D3DDECL_END()
};

//------------------------------------------------------------------------------------------------    
DWORD FtoDw(float f)
{
	return	*((DWORD*)&f);
}

bool BoundingBox::IfPointInside(D3DXVECTOR3 &p)
{
	if(p.x <=_max.x  &&p.y <=_max.y  &&p.z <=_max.z
		&&p.x >=_min.x  &&p.y >=_min.y  &&p.z >=_min.z)
		return true;
	else
		return false;
}
void BoundingBox::LocalToWorld(BoundingBox &out,D3DXMATRIX &worldMat)
{
	D3DXVec3TransformCoord(&out._min,&_min,&worldMat);
	D3DXVec3TransformCoord(&out._max,&_max,&worldMat);
}

float GetRandomFloat(float lowBound,float highBound)
{
	if(lowBound>highBound)
	{
		float temp;
		temp=highBound;
		highBound=lowBound;
		lowBound=temp;
	}
	float t=(rand()%10000)*0.0001f;
	return lowBound+t*(highBound-lowBound);//原理同hlsl中的lerp
}
void GetRandomVector(D3DXVECTOR3& out,D3DXVECTOR3& min,D3DXVECTOR3& max)
{
	out.x=GetRandomFloat(min.x,max.x);
	out.y=GetRandomFloat(min.y,max.y);
	out.z=GetRandomFloat(min.z,max.z);
}


void	ScreenShot()
{
	static bool push_down=false;
	if(GetKeyState(VK_F9)&0x80)
	{
		if(!push_down)
		{
			push_down=true;
			LPDIRECT3DSURFACE9	surface=NULL;
			e_pd3dDevice->GetBackBuffer(0,0,D3DBACKBUFFER_TYPE_MONO,&surface);
			static int	screenshots=0;
			if(screenshots==0)
			{
				CreateDirectory(_T("MyScreenShots"),NULL);
			}
			SYSTEMTIME syst;   
			GetLocalTime( &syst);
			char screenshotName[50];
			sprintf_s(screenshotName,_T("MyScreenShots/%4d%02d%02d%02d%02d%02d.jpg"),
				syst.wYear,syst.wMonth,syst.wDay,syst.wHour,syst.wMinute,syst.wSecond);
			D3DXSaveSurfaceToFile(screenshotName,D3DXIFF_JPG,surface,NULL,NULL);
			screenshots++;
			SAFE_RELEASE(surface);
		}
		
	}
	else
	{
		push_down=false;
	}

}

void ShowFPS(LPD3DXFONT	pFont,D3DCOLOR fpsColor,int windowWidth,int windowHeight,float _dt,bool _ifShowMspf)
{
	//calculate
	static float fps=0;
	static float mspf=0;//milliseconds per frame
	static int frameCount=0;
	static float totalTime=0;
	totalTime+=_dt;
	++frameCount;
	if(totalTime>1.0f)
	{
		fps=frameCount/totalTime;
		mspf=1000.0f/fps;
		frameCount=0;
		totalTime=0;
	}

	//show
	static char	strFps[50];
	RECT  formatRect={0,0,windowWidth,windowHeight};
	//如果用GetClientRect(hwnd,&formatRect);改变窗口大小反而不能自适应，固定长度能自适应是windows内部机制（DefWindowProc）的关系么…
	int charCount=_ifShowMspf?sprintf_s(strFps,_T("fps:%0.3f\nmsps:%0.3f"),fps,mspf):sprintf_s(strFps, _T("FPS:%0.3f"),fps);
	pFont->DrawText(NULL,strFps,charCount,&formatRect,DT_TOP | DT_RIGHT, fpsColor);

}

void GetWorldPickingRay(D3DXVECTOR3& originW, D3DXVECTOR3& dirW,D3DXMATRIX &proj,D3DXMATRIX &view)
{
	// Get the screen point clicked.
	POINT s;
	GetCursorPos(&s);

	// Make it relative to the client area window.
	ScreenToClient(e_hMainWnd, &s);

	// By the way we've been constructing things, the entire 
	// backbuffer is the viewport.
	D3DVIEWPORT9 vp;
	e_pd3dDevice->GetViewport(&vp);


	float x = (2.0f*s.x/vp.Width - 1.0f) / proj(0,0);
	float y = (-2.0f*s.y/vp.Height+ 1.0f) / proj(1,1);

	// Build picking ray in view space.
	D3DXVECTOR3 origin(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 dir(x, y, 1.0f);

	// So if the view matrix transforms coordinates from 
	// world space to view space, then the inverse of the
	// view matrix transforms coordinates from view space
	// to world space.
	D3DXMATRIX invView;
	D3DXMatrixInverse(&invView, 0, &view);

	// Transform picking ray to world space.
	D3DXVec3TransformCoord(&originW, &origin, &invView);
	D3DXVec3TransformNormal(&dirW, &dir, &invView);
	D3DXVec3Normalize(&dirW, &dirW);
}

void GenTriGrid(int numVertRows, int numVertCols,
				float dx, float dz, 
				const D3DXVECTOR3& center, 
				std::vector<D3DXVECTOR3>& verts,
				std::vector<DWORD>& indices)
{
	int numVertices = numVertRows*numVertCols;
	int numCellRows = numVertRows-1;
	int numCellCols = numVertCols-1;

	int numTris = numCellRows*numCellCols*2;

	float width = (float)numCellCols * dx;
	float depth = (float)numCellRows * dz;

	//===========================================
	// Build vertices.

	verts.resize( numVertices );

	// Offsets to translate grid from quadrant 4 to center of 
	// coordinate system.
	float xOffset = -width * 0.5f; 
	float zOffset =  depth * 0.5f;

	int k = 0;
	for(float i = 0; i < numVertRows; ++i)
	{
		for(float j = 0; j < numVertCols; ++j)
		{
			// Negate the depth coordinate to put in quadrant four.  
			// Then offset to center about coordinate system.
			verts[k].x =  j * dx + xOffset;
			verts[k].z = -i * dz + zOffset;
			verts[k].y =  0.0f;

			// Translate so that the center of the grid is at the
			// specified 'center' parameter.
			D3DXMATRIX T;
			D3DXMatrixTranslation(&T, center.x, center.y, center.z);
			D3DXVec3TransformCoord(&verts[k], &verts[k], &T);

			++k; // Next vertex
		}
	}

	//===========================================
	// Build indices.

	indices.resize(numTris * 3);

	// Generate indices for each quad.
	k = 0;
	for(DWORD i = 0; i < (DWORD)numCellRows; ++i)
	{
		for(DWORD j = 0; j < (DWORD)numCellCols; ++j)
		{
			indices[k]     =   i   * numVertCols + j;
			indices[k + 1] =   i   * numVertCols + j + 1;
			indices[k + 2] = (i+1) * numVertCols + j;

			indices[k + 3] = (i+1) * numVertCols + j;
			indices[k + 4] =   i   * numVertCols + j + 1;
			indices[k + 5] = (i+1) * numVertCols + j + 1;

			// next quad
			k += 6;
		}
	}
}

void GenGridMesh(ID3DXMesh* &pMesh,
				 int numVertRows, int numVertCols,
				 float dx, float dz,bool hasNormal,bool ifOptimize)
{

	std::vector<D3DXVECTOR3> verts;
	std::vector<DWORD> indices;
	GenTriGrid(numVertRows,numVertCols,dx,dz,D3DXVECTOR3(0,0,0),verts,indices);


	DWORD numTris=(numVertCols-1)*(numVertRows-1)*2;
	DWORD numVerts=numVertRows*numVertCols;
	if(hasNormal)
	{
		D3DXCreateMesh(numTris,numVerts,D3DXMESH_MANAGED|D3DXMESH_32BIT,VertexPNTElements,e_pd3dDevice,&pMesh);

		VertexPNT* v=NULL;
		pMesh->LockVertexBuffer(0,(void**)&v);
		float w=(numVertCols-1)*dx;
		float d=(numVertRows-1)*dz;
		for(int i=0;i<pMesh->GetNumVertices();i++)
		{
			v[i].pos=verts[i];
			v[i].tex0.x = (v[i].pos.x + (0.5f*w)) / w;
			v[i].tex0.y = (v[i].pos.z - (0.5f*d)) / -d;
		}
		pMesh->UnlockVertexBuffer();
	}
	else
	{
		D3DXCreateMesh(numTris,numVerts,D3DXMESH_MANAGED|D3DXMESH_32BIT,VertexPTElements,e_pd3dDevice,&pMesh);

		VertexPT* v=NULL;
		pMesh->LockVertexBuffer(0,(void**)&v);
		float w=(numVertCols-1)*dx;
		float d=(numVertRows-1)*dz;
		for(int i=0;i<pMesh->GetNumVertices();i++)
		{
			v[i].pos=verts[i];
			v[i].tex0.x = (v[i].pos.x + (0.5f*w)) / w;
			v[i].tex0.y = (v[i].pos.z - (0.5f*d)) / -d;
		}
		pMesh->UnlockVertexBuffer();
	}
	

	DWORD* indexBuffPtr = 0;
	DWORD* attBuff = 0;
	pMesh->LockIndexBuffer(0, (void**)&indexBuffPtr);
	pMesh->LockAttributeBuffer(0, &attBuff);
	for(UINT i = 0; i < pMesh->GetNumFaces(); ++i)
	{
		indexBuffPtr[i*3+0] = indices[i*3+0];
		indexBuffPtr[i*3+1] = indices[i*3+1];
		indexBuffPtr[i*3+2] = indices[i*3+2];

		attBuff[i]=0;// All in subset 0.
	}
	pMesh->UnlockIndexBuffer();
	pMesh->UnlockAttributeBuffer();

	// Compute Vertex Normals.
	if(hasNormal)
		D3DXComputeNormals(pMesh, 0);

	// Optimize for the vertex cache and build attribute table.
	if(ifOptimize)
	{
		DWORD* adj = new DWORD[pMesh->GetNumFaces()*3];
		pMesh->GenerateAdjacency(EPSILON, adj);
		//注意优化会修改顶点缓存！
		pMesh->OptimizeInplace(D3DXMESHOPT_VERTEXCACHE|D3DXMESHOPT_ATTRSORT,
			adj, 0, 0, 0);
		delete[] adj;
	}
	
}


void LoadXFile(
	const std::string& filename, 
	ID3DXMesh** meshOut,
	std::vector<Mtrl>& mtrls, 
	std::vector<IDirect3DTexture9*>& texs)
{
	// Step 1: Load the .x file from file into a system memory mesh.

	ID3DXMesh* meshSys      = 0;
	ID3DXBuffer* adjBuffer  = 0;
	ID3DXBuffer* mtrlBuffer = 0;
	DWORD numMtrls          = 0;

	HR(D3DXLoadMeshFromX(filename.c_str(), D3DXMESH_SYSTEMMEM, e_pd3dDevice,
		&adjBuffer,	&mtrlBuffer, 0, &numMtrls, &meshSys));


	// Step 2: Find out if the mesh already has normal info?

	D3DVERTEXELEMENT9 elems[MAX_FVF_DECL_SIZE];
	HR(meshSys->GetDeclaration(elems));

	bool hasNormals = false;
	D3DVERTEXELEMENT9 term = D3DDECL_END();
	for(int i = 0; i < MAX_FVF_DECL_SIZE; ++i)
	{
		// Did we reach D3DDECL_END() {0xFF,0,D3DDECLTYPE_UNUSED, 0,0,0}?
		if(elems[i].Stream == 0xff )
			break;

		if( elems[i].Type == D3DDECLTYPE_FLOAT3 &&
			elems[i].Usage == D3DDECLUSAGE_NORMAL &&
			elems[i].UsageIndex == 0 )
		{
			hasNormals = true;
			break;
		}
	}


	// Step 3: Change vertex format to VertexPNT.

	ID3DXMesh* temp = 0;
	HR(meshSys->CloneMesh(D3DXMESH_SYSTEMMEM, 
		VertexPNTElements, e_pd3dDevice, &temp));
	SAFE_RELEASE(meshSys);
	meshSys = temp;


	// Step 4: If the mesh did not have normals, generate them.

	if( hasNormals == false)
		HR(D3DXComputeNormals(meshSys, 0));


	// Step 5: Optimize the mesh.

	HR(meshSys->Optimize(D3DXMESH_MANAGED | 
		D3DXMESHOPT_COMPACT | D3DXMESHOPT_ATTRSORT | D3DXMESHOPT_VERTEXCACHE, 
		(DWORD*)adjBuffer->GetBufferPointer(), 0, 0, 0, meshOut));
	SAFE_RELEASE(meshSys); // Done w/ system mesh.
	SAFE_RELEASE(adjBuffer); // Done with buffer.

	// Step 6: Extract the materials and load the textures.

	if( mtrlBuffer != 0 && numMtrls != 0 )
	{
		D3DXMATERIAL* d3dxmtrls = (D3DXMATERIAL*)mtrlBuffer->GetBufferPointer();

		for(DWORD i = 0; i < numMtrls; ++i)
		{
			// Save the ith material.  Note that the MatD3D property does not have an ambient
			// value set when its loaded, so just set it to the diffuse value.
			Mtrl m;
			m.ambient   = d3dxmtrls[i].MatD3D.Diffuse;
			m.diffuse   = d3dxmtrls[i].MatD3D.Diffuse;
			m.spec      = d3dxmtrls[i].MatD3D.Specular;
			m.specPower = d3dxmtrls[i].MatD3D.Power;
			mtrls.push_back( m );

			// Check if the ith material has an associative texture
			if( d3dxmtrls[i].pTextureFilename != 0 )
			{
				// Yes, load the texture for the ith subset
				IDirect3DTexture9* tex = 0;
				//char* texFN = d3dxmtrls[i].pTextureFilename;
				//HR(D3DXCreateTextureFromFile(e_pd3dDevice, texFN, &tex));

				std::string texPath=d3dxmtrls[i].pTextureFilename;
				int index=filename.rfind('/');
				if(index!=std::string::npos)
				{
					std::string path=filename.substr(0,index+1);
					texPath=path+texPath;
				}
				D3DXCreateTextureFromFile(e_pd3dDevice, texPath.c_str(), &tex);
				// Save the loaded texture
				texs.push_back( tex );
			}
			else
			{
				// No texture for the ith subset
				texs.push_back( 0 );
			}
		}
	}
	SAFE_RELEASE(mtrlBuffer); // done w/ buffer
}
