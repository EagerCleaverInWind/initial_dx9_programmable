#pragma  once
#include "Utility.h"

#define  WAVE_BUFFER_SIZE 10

class FunctionModeWater
{
	struct Wave
	{
		D3DXVECTOR2 pos;//on water plane
		float amp;
		float w;//角速度
		float age;
		int alive;
		
		Wave():pos(0,0),amp(12),w(D3DX_PI),age(0),alive(0)
		{

		}
	};

private:
	ID3DXMesh* mWater;
	D3DXMATRIX mMatWorld;
	int mNumVertsRow;
	int mNumVertsCol;
	D3DXVECTOR2 mGridStep;
	float mHalfWidth;
	float mHalfDepth;

	Wave mWaveBuffer[WAVE_BUFFER_SIZE];
	std::vector<Wave*> mDeadWave;
	float mAttenuation;//振幅的衰减系数，就用指数函数拟合吧
	float mWaveSpeed;

	IDirect3DCubeTexture9* mEnvMap;
	Light mLight;
	Mtrl mMtrl;

	ID3DXEffect* mFX;
	D3DXHANDLE	mhWorld;
	D3DXHANDLE	mhWorldInvTrans;
	D3DXHANDLE  mhWVP;
	D3DXHANDLE  mhLight;
	D3DXHANDLE	mhMtrl;
	D3DXHANDLE	mhEyePosW;
	D3DXHANDLE	mhEnvMap; 
	D3DXHANDLE mhWaveArray;
	D3DXHANDLE mhWaveSpeed;


private:
	void BuildFX();
	void Push(float x,float z,float depth);//硬要说push的话，其实初相应该定为-PI/2的


public:
	FunctionModeWater();
	~FunctionModeWater();

	void Update(float dt);
	void Draw();
};