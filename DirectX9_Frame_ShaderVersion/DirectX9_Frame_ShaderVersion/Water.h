#pragma  once
#include "Utility.h"

//-----------------------------------��base��---------------------------------------
class Water
{
public:
	struct InitInfo
	{
		DirLight dirLight;
		Mtrl     mtrl;
		int      vertRows;
		int      vertCols;
		float    dx;
		float    dz;
		std::string waveMapFilename0;
		std::string waveMapFilename1;
		std::string dmapFilename0;
		std::string dmapFilename1;
		D3DXVECTOR2 waveNMapVelocity0;
		D3DXVECTOR2 waveNMapVelocity1;
		D3DXVECTOR2 waveDMapVelocity0;
		D3DXVECTOR2 waveDMapVelocity1;
		D3DXVECTOR2 scaleHeights;
		float texScale;
		D3DXMATRIX toWorld;
	};


public:
	Water(InitInfo& initInfo);
	~Water();

	void update(float dt);
	virtual void draw()=0;

	virtual void setEnvMap(IDirect3DCubeTexture9* envMap){}

protected:
	virtual void buildFX()=0;


protected:
	ID3DXMesh* mMesh;

	// The two normal maps to scroll.
	IDirect3DTexture9* mWaveMap0;
	IDirect3DTexture9* mWaveMap1;

	// The two displacement maps to scroll.
	IDirect3DTexture9* mDispMap0;
	IDirect3DTexture9* mDispMap1;

	// Offset of normal maps for scrolling (vary as a function of time)
	D3DXVECTOR2 mWaveNMapOffset0;
	D3DXVECTOR2 mWaveNMapOffset1;

	// Offset of displacement maps for scrolling (vary as a function of time)
	D3DXVECTOR2 mWaveDMapOffset0;
	D3DXVECTOR2 mWaveDMapOffset1;

	InitInfo mInitInfo;
	float mWidth;
	float mDepth;

	ID3DXEffect* mFX;
	D3DXHANDLE mhTech;
	D3DXHANDLE mhWVP;
	D3DXHANDLE mhWorld;
	D3DXHANDLE mhWorldInv;
	D3DXHANDLE mhLight;
	D3DXHANDLE mhMtrl;
	D3DXHANDLE mhEyePosW;
};

//------------------------------------------------------------------------------------------------    

void InitWater(Water::InitInfo& out,bool ifbrook);


//-----------------------------------��stream��---------------------------------------
class Stream:public Water
{
private:
	D3DXHANDLE mhTexScale;
	D3DXHANDLE mhWaveMap0;
	D3DXHANDLE mhWaveMap1;
	D3DXHANDLE mhWaveMapOffset0;
	D3DXHANDLE mhWaveMapOffset1;
	D3DXHANDLE mhEnvMap;

protected:
	void buildFX();

public:
	Stream(InitInfo& initInfo);
	~Stream();

	void draw();
	void setEnvMap(IDirect3DCubeTexture9* envMap);
};
//------------------------------------------------------------------------------------------------    

//-----------------------------------��sea��---------------------------------------
class Sea:public Water
{
private:
	D3DXHANDLE mhWaveMap0;
	D3DXHANDLE mhWaveMap1;
	D3DXHANDLE mhWaveNMapOffset0;
	D3DXHANDLE mhWaveNMapOffset1;
	D3DXHANDLE mhWaveDMapOffset0;
	D3DXHANDLE mhWaveDMapOffset1;
	D3DXHANDLE mhWaveDispMap0;
	D3DXHANDLE mhWaveDispMap1;
	D3DXHANDLE mhScaleHeights;
	D3DXHANDLE mhGridStepSizeL;
	D3DXHANDLE mhTexScale;

protected:
	void buildFX();

public:
	Sea(InitInfo& initInfo);
	~Sea();

	void draw();
};
//------------------------------------------------------------------------------------------------    