//已是OGRE阶段，回来D3D9实现，initial
#pragma  once
#include "Utility.h"

class RippleWater
{
private:
	ID3DXMesh* mWater;
	D3DXMATRIX mMatWorld;

	int mCurrentSimulation;
	float mAnimationTime;//dt，即规定的完成一次状态池更替所需时间
	float mTimeFragment;//来自上一帧不足以完成一次状态池更换的残留时间
	float mLastFrameTime;//just for push

	//算法参数
	float C; // ripple speed
	float D; // distance
	float U; // viscosity
	float T; // time
	float TERM1;
	float TERM2;
	float TERM3;

	//gpu version
	LPDIRECT3DTEXTURE9       mWaveSimulationTexture[3];
	LPDIRECT3DSURFACE9       mWaveSimulationSurface[3];
	LPDIRECT3DTEXTURE9       mDynamicDisplacementMap;
	LPDIRECT3DSURFACE9       mDynamicDisplacementSurface;
	LPDIRECT3DVERTEXBUFFER9 mFullScreenQuad;
	LPDIRECT3DVERTEXBUFFER9 mLocalQuad;//for push
	IDirect3DVertexDeclaration9* mQuadDecl;
	LPDIRECT3DSURFACE9		mBackBufferRenderTarget;

	//cpu version
	//D3DXVECTOR3* mPosBuffer[3];
	//D3DXVECTOR3* mNormalBuffer;//有D3DXComputeNormals()不需要
	//D3DXComputeNormals()慢得一逼，还是手写compute normals吧
	//并且因为每帧copy到vertex buffer时，用的是for循环，不如用内存换时间（格式一致直接memcpy）：
	VertexPNT* mStatusBuffer[3];
	int mTotalNumVertices;//for memcpy的调用 per frame
	bool mCPUUpdate;

	int mNumVertsRow;
	int mNumVertsCol;
	D3DXVECTOR2 mGridStep;
	//变换坐标系用
	float mHalfWidthWorld;
	float mHalfDepthWorld;
	float mHalfWidthGrid;
	float mHalfHeightGrid;


	IDirect3DCubeTexture9* mEnvMap;//实验性质，到时候重新实现一下reflect&refrect
	IDirect3DTexture9* mStreamBottomTex;
	Light mLight;
	Mtrl mMtrl;

	ID3DXEffect* mSimulationFX;
	D3DXHANDLE mhPreviousHeightTex;
	D3DXHANDLE mhCurrentHeightTex;
	D3DXHANDLE mhWidth;
	D3DXHANDLE mhHeight;
	D3DXHANDLE mhGridStep;
	D3DXHANDLE mhPushCenter;

	ID3DXEffect* mWaterFX;
	D3DXHANDLE	mhWorld;
	D3DXHANDLE	mhWorldInvTrans;
	D3DXHANDLE  mhWVP;
	D3DXHANDLE  mhLight;
	D3DXHANDLE	mhMtrl;
	D3DXHANDLE	mhEyePosW;
	D3DXHANDLE  mhDisplacementTex;
	D3DXHANDLE	mhEnvMap;
	D3DXHANDLE mhStreamBottomTex;

	
private:
	void CreateTexture();
	void CreateQuad();
	void BuildFX();

	//gpu version
	void PushGPU(float x,float z,float depth);
	void UpdateGPU(float dt);
	void DrawGPU();
	//之前push是在VS中，push信息由顶点携带
	//试试直接在PS中push
	void PushAtPS(float x,float z,float depth);

	//cpu version
	void PushCPU(float x,float z,float depth);
	void UpdateCPU(float dt);
	void DrawCPU();
	void ComputeNormals();


public:
	RippleWater();
	~RippleWater();

	void Push(float x,float z,float depth)
	{
		if(mCPUUpdate)
		{
			PushCPU(x,z,depth);
		}
		else
		{
			PushGPU(x,z,depth);
		}
	}

	void Update(float dt)
	{
		if(mCPUUpdate)
		{
			UpdateCPU(dt);
		}
		else
		{
			UpdateGPU(dt);
		}
	}

	void Draw()
	{
		if(mCPUUpdate)
		{
			DrawCPU();
		}
		else
		{
			DrawGPU();
		}
	}

};