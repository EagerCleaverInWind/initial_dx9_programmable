#include "ShadowMapping.h"
#include "Camera.h"

//-----------------------------------【shadow】---------------------------------------
ShadowMap::ShadowMap()
{
	mLight.ambient   = D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
	mLight.diffuse   = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mLight.spec      = D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
	mLight.spotPower = 32.0f;

	// Create shadow map.
	D3DVIEWPORT9 vp = {0, 0, 512, 512, 0.0f, 1.0f};
	mShadowMap = new DrawableTex2D(512, 512, 1, D3DFMT_R32F, true, D3DFMT_D24X8, vp, false);

	buildFX();
}

ShadowMap::~ShadowMap()
{
	SAFE_RELEASE(mFX);
	SAFE_DELETE(mShadowMap);
}

void ShadowMap::buildFX()
{
	// Create the FX from a .fx file.
	ID3DXBuffer* errors = 0;
	HR(D3DXCreateEffectFromFile(e_pd3dDevice, "shader/LightShadow.fx", 
		0, 0, D3DXSHADER_DEBUG, 0, &mFX, &errors));
	if( errors )
		MessageBox(0, (char*)errors->GetBufferPointer(), 0, 0);

	// Obtain handles.
	mhTech               = mFX->GetTechniqueByName("LightShadowTech");
	mhBuildShadowMapTech = mFX->GetTechniqueByName("BuildShadowMapTech");
	mhLightWVP           = mFX->GetParameterByName(0, "gLightWVP");
	mhWVP                = mFX->GetParameterByName(0, "gWVP");
	mhWorld              = mFX->GetParameterByName(0, "gWorld");
	mhMtrl               = mFX->GetParameterByName(0, "gMtrl");
	mhLight              = mFX->GetParameterByName(0, "gLight");
	mhEyePosW            = mFX->GetParameterByName(0, "gEyePosW");
	mhTex                = mFX->GetParameterByName(0, "gTex");
	mhShadowMap          = mFX->GetParameterByName(0, "gShadowMap");
}

void ShadowMap::drawShadowMap()
{
	mShadowMap->beginScene();
	HR(e_pd3dDevice->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0x00000000, 1.0f, 0));

	HR(mFX->SetTechnique(mhBuildShadowMapTech));

	UINT numPasses = 0;
	HR(mFX->Begin(&numPasses, 0));
	HR(mFX->BeginPass(0));

	DrawObjectsToShadowMap();

	HR(mFX->EndPass());
	HR(mFX->End());

	mShadowMap->endScene();
}

void ShadowMap::drawScene()
{
	//drawShadowMap();
	//放到了子类的Update()中,为了避免写入纹理的begin()、end()混入到写入backbuffer的D3D设备->begin(),end()之间（main文件中）
	HR(mFX->SetValue(mhEyePosW, &e_pCamera->GetPosition(), sizeof(D3DXVECTOR3)));
	HR(mFX->SetTexture(mhShadowMap, mShadowMap->GetTex()));

	mFX->SetTechnique(mhTech);
	UINT numPasses=0;
	mFX->Begin(&numPasses,0);
	mFX->BeginPass(0);

	DrawObjectsToBackBuffer();

	mFX->EndPass();
	mFX->End();
}

//------------------------------------------------------------------------------------------------    



//-----------------------------------【demo】---------------------------------------
ShadowMapDemo::ShadowMapDemo()
{
	LoadXFile("Mesh/BasicColumnScene/BasicColumnScene.x", &mSceneMesh, mSceneMtrls, mSceneTextures);
	D3DXMatrixIdentity(&mSceneWorld);

	LoadXFile("Mesh/car.x", &mCarMesh, mCarMtrls, mCarTextures);
	D3DXMATRIX S, T;
	D3DXMatrixScaling(&S, 1.5f, 1.5f, 1.5f);
	D3DXMatrixTranslation(&T, 6.0f, 3.5f, -3.0f);
	mCarWorld = S*T;

	HR(D3DXCreateTextureFromFile(e_pd3dDevice, "Texture/whitetex.dds", &mWhiteTex));

}

ShadowMapDemo::~ShadowMapDemo()
{
	SAFE_RELEASE(mCarMesh);
	SAFE_RELEASE(mSceneMesh);
	SAFE_RELEASE(mWhiteTex);
}

void ShadowMapDemo::DrawObjectsToShadowMap()
{
	// Draw scene mesh.
	HR(mFX->SetMatrix(mhLightWVP, &(mSceneWorld*mLightVP)));
	HR(mFX->CommitChanges());
	for(UINT j = 0; j < mSceneMtrls.size(); ++j)
	{
		HR(mSceneMesh->DrawSubset(j));
	}

	// Draw car mesh.
	HR(mFX->SetMatrix(mhLightWVP, &(mCarWorld*mLightVP)));
	HR(mFX->CommitChanges());
	for(UINT j = 0; j < mCarMtrls.size(); ++j)
	{
		HR(mCarMesh->DrawSubset(j));
	}
}

void ShadowMapDemo::DrawObjectsToBackBuffer()
{
	D3DXMATRIX matViewProj;
	e_pCamera->GetViewProj(matViewProj);

	// Draw Scene mesh.
	HR(mFX->SetMatrix(mhLightWVP, &(mSceneWorld*mLightVP)));
	HR(mFX->SetMatrix(mhWVP, &(mSceneWorld*matViewProj)));
	HR(mFX->SetMatrix(mhWorld, &mSceneWorld));	
	for(UINT j = 0; j < mSceneMtrls.size(); ++j)
	{
		HR(mFX->SetValue(mhMtrl, &mSceneMtrls[j], sizeof(Mtrl)));

		if(mSceneTextures[j] != 0)
		{
			HR(mFX->SetTexture(mhTex, mSceneTextures[j]));
		}
		else
		{
			HR(mFX->SetTexture(mhTex, mWhiteTex));
		}

		HR(mFX->CommitChanges());
		HR(mSceneMesh->DrawSubset(j));
	}

	// Draw car mesh.
	HR(mFX->SetMatrix(mhLightWVP, &(mCarWorld*mLightVP)));
	HR(mFX->SetMatrix(mhWVP, &(mCarWorld*matViewProj)));
	HR(mFX->SetMatrix(mhWorld, &mCarWorld));	
	for(UINT j = 0; j < mCarMtrls.size(); ++j)
	{
		HR(mFX->SetValue(mhMtrl, &mCarMtrls[j], sizeof(Mtrl)));

		if(mCarTextures[j] != 0)
		{
			HR(mFX->SetTexture(mhTex, mCarTextures[j]));
		}
		else
		{
			HR(mFX->SetTexture(mhTex, mWhiteTex));
		}

		HR(mFX->CommitChanges());
		HR(mCarMesh->DrawSubset(j));
	}
}

void ShadowMapDemo::Update(float dt)
{
	// Animate spot light by rotating it on y-axis with respect to time.
	D3DXMATRIX lightView;
	D3DXVECTOR3 lightPosW(125.0f, 50.0f, 0.0f);
	D3DXVECTOR3 lightTargetW(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 lightUpW(0.0f, 1.0f, 0.0f);

	static float t = 0.0f;
	t += dt;
	if( t >= 2.0f*D3DX_PI )
		t = 0.0f;
	D3DXMATRIX Ry;
	D3DXMatrixRotationY(&Ry, t);
	D3DXVec3TransformCoord(&lightPosW, &lightPosW, &Ry);

	D3DXMatrixLookAtLH(&lightView, &lightPosW, &lightTargetW, &lightUpW);

	D3DXMATRIX lightLens;
	float lightFOV = D3DX_PI*0.25f;
	D3DXMatrixPerspectiveFovLH(&lightLens, lightFOV, 1.0f, 1.0f, 200.0f);

	mLightVP = lightView*lightLens;

	// Setup a spotlight corresponding to the projector.
	D3DXVECTOR3 lightDirW = lightTargetW - lightPosW;
	D3DXVec3Normalize(&lightDirW, &lightDirW);
	mLight.posW      = lightPosW;
	mLight.dirW      = lightDirW;
	mFX->SetValue(mhLight,&mLight,sizeof(SpotLight));

	drawShadowMap();
}

void ShadowMapDemo::Draw()
{
	drawScene();
}
//------------------------------------------------------------------------------------------------    