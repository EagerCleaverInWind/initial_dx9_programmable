#include "InteractionalWater.h"
#include "Camera.h"
#include "DirectInput.h"

#define ANIMATIONS_PER_SECOND 100.0f

//除了绝对的速度优势外，相比cpu version，gpu ps实现版本有两个不足：
//每行和每列的顶点个数（也就是resolution)必须是2的幂，否则push后会出现奇怪的现象，只有右下四分之一quad有反应，且反应不正常
//涟漪效果，本已经散开的内部会出现不该有的残留波动，从而出现不该有的波光粼粼，不平滑
//波光粼粼debug了无数次，没找到问题，也许是系统问题吧，这种实现方式固有的误差，嗯，我也很绝望，告一段落了
//待用11的cs实现
//——》在shadertoy上搜到一个2D版本的，rippling效果与那个古老的NVIDIA WaterInteraction demo一样完美！
//我看了下，所用公式还没我的正宗……
//受平台所限直接在PS中push，于是我的也试试在PS中push？！——》他妈果然是push quad的问题！
//将push quad改大点，发现波光粼粼实质是内圈有锯齿，这正是push不够平滑造成的！
RippleWater::RippleWater():mNumVertsRow(256),mNumVertsCol(256),mGridStep(D3DXVECTOR2(10.0f,10.0f)),
	mCurrentSimulation(0),mAnimationTime(1.0f/ANIMATIONS_PER_SECOND),mTimeFragment(0.0f),mLastFrameTime(0.0f),
	mCPUUpdate(false)
{
	//实验性质
	e_pCamera->SetPosition(D3DXVECTOR3(300,750,0));
	mLight.ambient=D3DXCOLOR(0.15,0.15,0.15,1.0f);
	mLight.light=D3DXVECTOR4(1,-1,1,0.0f);

	mMtrl.diffuse=mMtrl.ambient=D3DXCOLOR(0.35f,0.35f,0.35f,1.0f);
	mMtrl.spec=D3DXCOLOR(0.02f,0.02f,0.02f,1.0f);

	mHalfWidthWorld=(mNumVertsRow-1)*mGridStep.x/2.0f;
	mHalfDepthWorld=(mNumVertsCol-1)*mGridStep.y/2.0f;
	mHalfWidthGrid=(mNumVertsRow-1)/2.0f;
	mHalfHeightGrid=(mNumVertsCol-1)/2.0f;

	D3DXMatrixIdentity(&mMatWorld);
	//GenGridMesh(mWater,mNumVertsCol,mNumVertsRow,mGridStep.x,mGridStep.y,false);
	//GenGridMesh(mWater,mNumVertsCol,mNumVertsRow,mGridStep.x,mGridStep.y,true);//为了拓进cpu版本
	//不能优化！优化会修改vertex buffer！害得我一开始测试cpu version时，push后出现分tile传递的情况！debug郁闷了一天QAQ
	GenGridMesh(mWater,mNumVertsCol,mNumVertsRow,mGridStep.x,mGridStep.y,true,false);

	//cpu version
	mTotalNumVertices=mNumVertsCol*mNumVertsRow;
	VertexPNT* vp=nullptr;
	mWater->LockVertexBuffer(0,(void**)&vp);
	for(int i=0;i<3;i++)
	{
		//mPosBuffer[i]=new D3DXVECTOR3[mNumVertsRow*mNumVertsCol];
		mStatusBuffer[i]=new VertexPNT[mNumVertsRow*mNumVertsCol];
		//memcpy(mStatusBuffer[i],vp,sizeof(mStatusBuffer[i]));//不行，是指针不是数组
		memcpy(mStatusBuffer[i],vp,sizeof(VertexPNT)*mTotalNumVertices);
	}
	/*for(int i=0;i<mWater->GetNumVertices();i++)
	{
		mPosBuffer[0][i]=vp[i].pos;
		mPosBuffer[1][i]=vp[i].pos;
		mPosBuffer[2][i]=vp[i].pos;
	}*/
	mWater->UnlockVertexBuffer();
	//mNormalBuffer=new D3DXVECTOR3[mNumVertsRow*mNumVertsCol];
	//算法参数
	C = 0.3f ; // ripple speed
	D = 0.4f ; // distance
	U = 0.05f ; // viscosity
	T = 0.13f ; // time
	TERM1 = ( 4.0f - 8.0f*C*C*T*T/(D*D) ) / (U*T+2) ;
	TERM2 = ( U*T-2.0f ) / (U*T+2.0f) ;
	TERM3 = ( 2.0f * C*C*T*T/(D*D) ) / (U*T+2) ;

	CreateTexture();
	CreateQuad();
	BuildFX();

}

RippleWater::~RippleWater()
{
	SAFE_RELEASE(mWater);
	SAFE_RELEASE(mDynamicDisplacementSurface);
	SAFE_RELEASE(mDynamicDisplacementMap);
	SAFE_RELEASE(mFullScreenQuad);
	SAFE_RELEASE(mLocalQuad);
	SAFE_RELEASE(mQuadDecl);
	SAFE_RELEASE(mBackBufferRenderTarget);
	SAFE_RELEASE(mEnvMap);
	SAFE_RELEASE(mSimulationFX);
	SAFE_RELEASE(mWaterFX);
	for(int i=0;i<3;i++)
	{
		SAFE_RELEASE(mWaveSimulationSurface[i]);
		SAFE_RELEASE(mWaveSimulationTexture[i]);
		//SAFE_DELETE(mPosBuffer[i]);
		SAFE_DELETE(mStatusBuffer[i]);
	}
	//SAFE_DELETE(mNormalBuffer);
}


void RippleWater::CreateTexture()
{
	e_pd3dDevice->GetRenderTarget(0,&mBackBufferRenderTarget);
	for(int i=0;i<3;i++)
	{
		D3DXCreateTexture(e_pd3dDevice, mNumVertsRow, mNumVertsCol, 1, D3DUSAGE_RENDERTARGET, 
			D3DFMT_A16B16G16R16F, D3DPOOL_DEFAULT, &mWaveSimulationTexture[i]);
		mWaveSimulationTexture[i]->GetSurfaceLevel(0, &mWaveSimulationSurface[i]);
		e_pd3dDevice->SetRenderTarget(0, mWaveSimulationSurface[i]);
		e_pd3dDevice->Clear(0L, NULL, D3DCLEAR_TARGET, D3DCOLOR_RGBA(0, 0, 0, 0), 1.0f, 0L);
	}
	e_pd3dDevice->SetRenderTarget(0, mBackBufferRenderTarget);

	D3DXCreateTexture(e_pd3dDevice, mNumVertsRow, mNumVertsCol, 1, D3DUSAGE_RENDERTARGET, D3DFMT_A32B32G32R32F, D3DPOOL_DEFAULT, 
		&mDynamicDisplacementMap);
	mDynamicDisplacementMap->GetSurfaceLevel(0,&mDynamicDisplacementSurface);

	D3DXCreateCubeTextureFromFile(e_pd3dDevice,"Texture/CubeMap/CubeSceneForWater.dds",&mEnvMap);
	D3DXCreateTextureFromFile(e_pd3dDevice,"Texture/Water/streamBottom.png",&mStreamBottomTex);
}

void RippleWater::CreateQuad()
{
	e_pd3dDevice->CreateVertexDeclaration(VertexPTElements,&mQuadDecl);

	//full screen quad
	e_pd3dDevice->CreateVertexBuffer(4*sizeof(VertexPT),D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, &mFullScreenQuad, 0);
	VertexPT* vpt=nullptr;
	mFullScreenQuad->Lock(0,0,(void**)&vpt,0);
	//D3DPRIMITIVETYPE用的是Triangle Fans，即围绕顶点0转的那种
	vpt[0].pos=D3DXVECTOR3(-1,1,0);
	vpt[0].tex0=D3DXVECTOR2(0,0);
	vpt[1].pos=D3DXVECTOR3(1,1,0);
	vpt[1].tex0=D3DXVECTOR2(1,0);
	vpt[2].pos=D3DXVECTOR3(1,-1,0);
	vpt[2].tex0=D3DXVECTOR2(1,1);
	vpt[3].pos=D3DXVECTOR3(-1,-1,0);
	vpt[3].tex0=D3DXVECTOR2(0,1);
	mFullScreenQuad->Unlock();

	//push quad
	e_pd3dDevice->CreateVertexBuffer(4*sizeof(VertexPT),D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY,
		0,D3DPOOL_DEFAULT,&mLocalQuad,0);

}

void RippleWater::BuildFX()
{
	//water effect
	ID3DXBuffer *errors;
	HR(D3DXCreateEffectFromFile(e_pd3dDevice, "shader/RippleWater.fx",
		0, 0, D3DXSHADER_DEBUG, 0, &mWaterFX, &errors));
	if(errors)
		MessageBox(0, (char*)errors->GetBufferPointer(), 0, 0);

	mhWorld=mWaterFX->GetParameterByName(0,"gWorld");
	mhWorldInvTrans=mWaterFX->GetParameterByName(0,"gWorldInvTrans");
	mhWVP=mWaterFX->GetParameterByName(0,"gWVP");
	mhLight=mWaterFX->GetParameterByName(0,"gLight");
	mhMtrl=mWaterFX->GetParameterByName(0,"gMtrl");
	mhEyePosW=mWaterFX->GetParameterByName(0,"gEyePosW");
	mhDisplacementTex=mWaterFX->GetParameterByName(0,"gDisplacementTex");
	mhEnvMap=mWaterFX->GetParameterByName(0,"gEnvMap");
	mhStreamBottomTex=mWaterFX->GetParameterByName(0,"gStreamBottomTex");

	mWaterFX->SetTechnique("Water");
	mWaterFX->SetValue(mhLight,&mLight,sizeof(Light));
	mWaterFX->SetValue(mhMtrl,&mMtrl,sizeof(Mtrl));
	mWaterFX->SetMatrix(mhWorld,&mMatWorld);
	mWaterFX->SetMatrix(mhWorldInvTrans,&mMatWorld);
	mWaterFX->SetTexture(mhEnvMap,mEnvMap);
	mWaterFX->SetTexture(mhStreamBottomTex,mStreamBottomTex);


	//simulation effect
	HR(D3DXCreateEffectFromFile(e_pd3dDevice, "shader/RippleSimulation.fx",
		0, 0, D3DXSHADER_DEBUG, 0, &mSimulationFX, &errors));
	if(errors)
		MessageBox(0, (char*)errors->GetBufferPointer(), 0, 0);

	mhPreviousHeightTex=mSimulationFX->GetParameterByName(0,"gPreviousHeightTex");
	mhCurrentHeightTex=mSimulationFX->GetParameterByName(0,"gCurrentHeightTex");
	mhWidth=mSimulationFX->GetParameterByName(0,"gWidth");
	mhHeight=mSimulationFX->GetParameterByName(0,"gHeight");
	mhGridStep=mSimulationFX->GetParameterByName(0,"gGridStep");
	mhPushCenter=mSimulationFX->GetParameterByName(0,"gPushCenter");

	mSimulationFX->SetInt(mhWidth,mNumVertsRow);
	mSimulationFX->SetInt(mhHeight,mNumVertsCol);
	mSimulationFX->SetValue(mhGridStep,&mGridStep,sizeof(D3DXVECTOR2));

}

void RippleWater::PushGPU(float x,float z,float depth)
{
	// scale pressure according to time passed
	depth = depth * mLastFrameTime * ANIMATIONS_PER_SECOND;
	VertexPT temp[4];
	//假定没变动过world matrix即local space与world space重合,不然要先变换到local space
	//先变换到grid space求weight，注意grid space的原点设在左下，为了方便转换（world space、grid space、投影面都是这个朝向）
	float xGrid=(x+mHalfWidthWorld)/mGridStep.x;
	float yGrid=(z+mHalfDepthWorld)/mGridStep.y;
	//D3DPRIMITIVETYPE用的是Triangle Strips，即每画一个三角形进一个顶点且cull mode自动flip、专为画连续quad的那种
	temp[0].pos=D3DXVECTOR3(floor(xGrid),floor(yGrid),0);
	temp[1].pos=temp[0].pos+D3DXVECTOR3(0,1,0);
	temp[2].pos=temp[0].pos+D3DXVECTOR3(1,0,0);
	temp[3].pos=temp[0].pos+D3DXVECTOR3(1,1,0);
	for(int i=0;i<4;i++)
	{
		D3DXVECTOR2 difference=D3DXVECTOR2(temp[i].pos.x-xGrid,temp[i].pos.y-yGrid);
		float power=1.0f-D3DXVec2Length(&difference)/mGridStep.x;//这里没考虑xy步长不等的情况
		if(power<0)
			power=0;
		temp[i].tex0=D3DXVECTOR2(power*depth,0);

		//再变换到投影面
		temp[i].pos.x=(temp[i].pos.x-mHalfWidthGrid)/mHalfWidthGrid;
		temp[i].pos.y=(temp[i].pos.y-mHalfHeightGrid)/mHalfHeightGrid;

		//D3DXVECTOR3 debugPushPos=temp[i].pos;
		//int hehe=1+1;
	}
	
	//写入到vertex buffer
	VertexPT* vpt;
	mLocalQuad->Lock(0,0,(void**)&vpt,D3DLOCK_DISCARD);
	memcpy(vpt,temp,sizeof(temp));
	mLocalQuad->Unlock();

	//呦西，开始push了
	e_pd3dDevice->SetRenderTarget(0,mWaveSimulationSurface[mCurrentSimulation]);
	e_pd3dDevice->BeginScene();

	mSimulationFX->SetTechnique("SimulatePush");
	UINT numPasses=0;
	mSimulationFX->Begin(&numPasses,0);
	mSimulationFX->BeginPass(0);
	e_pd3dDevice->SetVertexDeclaration(mQuadDecl);
	e_pd3dDevice->SetStreamSource(0,mLocalQuad,0,sizeof(VertexPT));
	e_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP,0,2);
	//e_pd3dDevice->DrawPrimitive(D3DPT_POINTLIST,0,1);
	mSimulationFX->EndPass();
	mSimulationFX->End();

	e_pd3dDevice->EndScene();
	e_pd3dDevice->SetRenderTarget(0,mBackBufferRenderTarget);

}

void RippleWater::PushAtPS(float x,float z,float depth)
{
	float u=(x+mHalfWidthWorld)/(2.0f*mHalfWidthWorld);
	float v=-(z-mHalfDepthWorld)/(2.0f*mHalfDepthWorld);
	//实验性质，待exposed to set
	float clickPushRadius=0.018f;//texture space

	//呦西，开始push了
	e_pd3dDevice->SetRenderTarget(0,mWaveSimulationSurface[mCurrentSimulation]);
	e_pd3dDevice->BeginScene();

	mSimulationFX->SetTechnique("PushAtPSTech");
	//上次递推完shader中previous没跟上
	//mSimulationFX->SetTexture(mhPreviousHeightTex,mWaveSimulationTexture[(mCurrentSimulation+2)%3]);
	D3DXVECTOR4 pushCenter=D3DXVECTOR4(u,v,depth,clickPushRadius);
	//mSimulationFX->SetVector(mhPushCenter,&pushCenter);
	mSimulationFX->SetValue(mhPushCenter,&pushCenter,sizeof(D3DXVECTOR4));
	UINT numPasses=0;
	mSimulationFX->Begin(&numPasses,0);
	mSimulationFX->BeginPass(0);
	e_pd3dDevice->SetVertexDeclaration(mQuadDecl);
	e_pd3dDevice->SetStreamSource(0,mFullScreenQuad,0,sizeof(VertexPT));
	e_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLEFAN, 0, 2);
	mSimulationFX->EndPass();
	mSimulationFX->End();
	
	e_pd3dDevice->EndScene();
	e_pd3dDevice->SetRenderTarget(0,mBackBufferRenderTarget);
}

void RippleWater::UpdateGPU(float dt)
{
	mLastFrameTime=dt;
	mTimeFragment+=dt;

	//实验性质，到时候改成下雨之类，即外部来push
	static bool isKeyDown=false;
	if(e_pDInput->IsKeyDown(DIK_P))
	{
		if(!isKeyDown)
		{
			isKeyDown=true;
			//PushGPU(0,0,-250);
			PushAtPS(0,0,-30);
		}
	}
	else
		isKeyDown=false;

}

void RippleWater::DrawGPU()
{
	//状态池的递推
	mSimulationFX->SetTechnique("SimulateRecursion");
	e_pd3dDevice->SetVertexDeclaration(mQuadDecl);
	e_pd3dDevice->SetStreamSource(0,mFullScreenQuad,0,sizeof(VertexPT));
	while(mTimeFragment>=mAnimationTime)
	{
		mSimulationFX->SetTexture(mhCurrentHeightTex,mWaveSimulationTexture[mCurrentSimulation]);
		mSimulationFX->SetTexture(mhPreviousHeightTex,mWaveSimulationTexture[(mCurrentSimulation+2)%3]);
		e_pd3dDevice->SetRenderTarget(0,mWaveSimulationSurface[(mCurrentSimulation+1)%3]);
		
		UINT numPasses=0;
		mSimulationFX->Begin(&numPasses,0);
		mSimulationFX->BeginPass(0);
		e_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLEFAN, 0, 2);
		mSimulationFX->EndPass();
		mSimulationFX->End();

		mCurrentSimulation=(mCurrentSimulation+1)%3;
		mTimeFragment-=mAnimationTime;
	}

	//copy to displacement map
	mSimulationFX->SetTechnique("BuildDisplacementMap");
	mSimulationFX->SetTexture(mhCurrentHeightTex,mWaveSimulationTexture[mCurrentSimulation]);
	e_pd3dDevice->SetRenderTarget(0,mDynamicDisplacementSurface);
	UINT numPasses=0;
	mSimulationFX->Begin(&numPasses,0);
	mSimulationFX->BeginPass(0);
	e_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLEFAN, 0, 2);
	mSimulationFX->EndPass();
	mSimulationFX->End();

	//render water!
	mWaterFX->SetTechnique("Water");
	//debug用
	//mWaterFX->SetTechnique("DebugTech");
	//mWaterFX->SetTechnique("Debug2Tech");
	//note这里assume local space与world space重合，所以没更新world matrix
	D3DXMATRIX matViewProj;
	e_pCamera->GetViewProj(matViewProj);
	mWaterFX->SetMatrix(mhWVP,&matViewProj);
	mWaterFX->SetValue(mhEyePosW,&(e_pCamera->GetPosition()),sizeof(D3DXVECTOR3));
	mWaterFX->SetTexture(mhDisplacementTex,mDynamicDisplacementMap);
	//debug用，会不会是copy问题即DisplacementTex的问题
	//mWaterFX->SetTexture(mhDisplacementTex,mWaveSimulationTexture[mCurrentSimulation]);
	e_pd3dDevice->SetRenderTarget(0,mBackBufferRenderTarget);
	mWaterFX->Begin(&numPasses,0);
	mWaterFX->BeginPass(0);
	mWater->DrawSubset(0);
	mWaterFX->EndPass();
	mWaterFX->End();


}

void RippleWater::PushCPU(float x,float z,float depth)
{
	// scale pressure according to time passed
	depth = depth * mLastFrameTime * ANIMATIONS_PER_SECOND;
	VertexPT temp[4];
	//假定没变动过world matrix即local space与world space重合,不然要先变换到local space
	//先变换到grid space求weight
	float xGrid=(x+mHalfWidthWorld)/mGridStep.x;
	float yGrid=(z+mHalfDepthWorld)/mGridStep.y;
	temp[0].pos=D3DXVECTOR3(floor(xGrid),floor(yGrid),0);
	temp[1].pos=temp[0].pos+D3DXVECTOR3(0,1,0);
	temp[2].pos=temp[0].pos+D3DXVECTOR3(1,0,0);
	temp[3].pos=temp[0].pos+D3DXVECTOR3(1,1,0);
	for(int i=0;i<4;i++)
	{
		D3DXVECTOR2 difference=D3DXVECTOR2(temp[i].pos.x-xGrid,temp[i].pos.y-yGrid);
		float power=1.0f-D3DXVec2Length(&difference)/mGridStep.x;//这里没考虑xy步长不等的情况
		if(power<0)
			power=0;
		temp[i].tex0=D3DXVECTOR2(power*depth,0);

		//再变换到投影面
		//temp[i].pos.x=(temp[i].pos.x-mHalfWidthGrid)/mHalfWidthGrid;
		//temp[i].pos.y=(temp[i].pos.y-mHalfHeightGrid)/mHalfHeightGrid;

		int indexToPush=int(mNumVertsCol-1-temp[i].pos.y)*mNumVertsRow+int(temp[i].pos.x);
		//absolute，不停push会因为突然改变位置造成不连续，进而递推出巨大高低差能量爆炸
		//(mPosBuffer[mCurrentSimulation][indexToPush]).y=temp[i].tex0.x;
		//叠加才是合理的push方式，并且因为散失得更快反而看不出叠加效果偏向保持
		//(mPosBuffer[mCurrentSimulation][indexToPush]).y+=temp[i].tex0.x;
		(mStatusBuffer[mCurrentSimulation][indexToPush]).pos.y+=temp[i].tex0.x;
	}

}

void RippleWater::UpdateCPU(float dt)
{
	mLastFrameTime=dt;
	mTimeFragment+=dt;

	//实验性质，到时候改成下雨之类，即外部来push
	static bool isKeyDown=false;
	if(e_pDInput->IsKeyDown(DIK_P))
	{
		if(!isKeyDown)
		{
			isKeyDown=true;
			PushCPU(0,0,-15);
		}
	}
	else
		isKeyDown=false;

	//状态池的递推
	while(mTimeFragment>=mAnimationTime)
	{
		//D3DXVECTOR3* previousBuf=mPosBuffer[(mCurrentSimulation+2)%3];
		//D3DXVECTOR3* currentBuf=mPosBuffer[mCurrentSimulation];
		//D3DXVECTOR3* nextBuf=mPosBuffer[(mCurrentSimulation+1)%3];
		VertexPNT* previousBuf=mStatusBuffer[(mCurrentSimulation+2)%3];
		VertexPNT* currentBuf=mStatusBuffer[mCurrentSimulation];
		VertexPNT* nextBuf=mStatusBuffer[(mCurrentSimulation+1)%3];
		// don't do anything with border values
		for(int y=1;y<mNumVertsCol-1;y++)
		{
			/*D3DXVECTOR3* row = nextBuf + y*mNumVertsRow ;
			D3DXVECTOR3* row1 = currentBuf + y*mNumVertsRow ;
			D3DXVECTOR3* row1up = currentBuf + (y-1)*mNumVertsRow ;
			D3DXVECTOR3* row1down = currentBuf+ (y+1)*mNumVertsRow ;
			D3DXVECTOR3* row2 = previousBuf + y*mNumVertsRow ;*/
			VertexPNT* row = nextBuf + y*mNumVertsRow ;
			VertexPNT* row1 = currentBuf + y*mNumVertsRow ;
			VertexPNT* row1up = currentBuf + (y-1)*mNumVertsRow ;
			VertexPNT* row1down = currentBuf+ (y+1)*mNumVertsRow ;
			VertexPNT* row2 = previousBuf + y*mNumVertsRow ;
			for(int x=1;x<mNumVertsRow-1;x++) 
			{
				/*row[x].y = TERM1 * row1[x].y
					+ TERM2 * row2[x].y
					+ TERM3 * ( row1[x-1].y + row1[x+1].y + row1up[x].y+row1down[x].y ) ;*/
				row[x].pos.y= TERM1 * row1[x].pos.y
					+ TERM2 * row2[x].pos.y
					+ TERM3 * ( row1[x-1].pos.y + row1[x+1].pos.y + row1up[x].pos.y+row1down[x].pos.y ) ;
			}
		}

		mCurrentSimulation=(mCurrentSimulation+1)%3;
		mTimeFragment-=mAnimationTime;
	}

	//更新vertex buffer
	VertexPNT* vp=nullptr;
	mWater->LockVertexBuffer(0,(void**)&vp);
	/*for(int i=0;i<mWater->GetNumVertices();i++)
	{
		vp[i].pos=mPosBuffer[mCurrentSimulation][i];
	}*/
	ComputeNormals();
	memcpy(vp,mStatusBuffer[mCurrentSimulation],sizeof(VertexPNT)*mTotalNumVertices);
	mWater->UnlockVertexBuffer();
	//D3DXComputeNormals(mWater,0);//一般用在初始化时，每帧用它卡得一逼
}

void RippleWater::ComputeNormals()
{
	//clear normal sum
	for(int i=0;i<mTotalNumVertices;i++)
	{
		(mStatusBuffer[mCurrentSimulation][i]).normal=D3DXVECTOR3(0,0,0);
	}

	DWORD* indexBuffPtr = 0;
	mWater->LockIndexBuffer(0, (void**)&indexBuffPtr);
	for(UINT i=0;i< mWater->GetNumFaces();i++)
	{
		int p0=indexBuffPtr[i*3];
		int p1=indexBuffPtr[i*3+1];
		int p2=indexBuffPtr[i*3+2];

		D3DXVECTOR3 diff1=(mStatusBuffer[mCurrentSimulation][p1]).pos-(mStatusBuffer[mCurrentSimulation][p0]).pos;
		D3DXVECTOR3 diff2=(mStatusBuffer[mCurrentSimulation][p2]).pos-(mStatusBuffer[mCurrentSimulation][p0]).pos;
		D3DXVECTOR3 crossValue;
		D3DXVec3Cross(&crossValue,&diff1,&diff2);

		(mStatusBuffer[mCurrentSimulation][p0]).normal+=crossValue;
		(mStatusBuffer[mCurrentSimulation][p1]).normal+=crossValue;
		(mStatusBuffer[mCurrentSimulation][p2]).normal+=crossValue;
	}
	mWater->UnlockIndexBuffer();

	//normalize
	//clear normal sum
	for(int i=0;i<mTotalNumVertices;i++)
	{
		D3DXVECTOR3* normalPtr=&( (mStatusBuffer[mCurrentSimulation][i]).normal );
		D3DXVec3Normalize(normalPtr,normalPtr);
	}

}

void RippleWater::DrawCPU()
{
	mWaterFX->SetTechnique("WaterCPUVersion");
	D3DXMATRIX matViewProj;
	e_pCamera->GetViewProj(matViewProj);
	mWaterFX->SetMatrix(mhWVP,&matViewProj);
	mWaterFX->SetValue(mhEyePosW,&(e_pCamera->GetPosition()),sizeof(D3DXVECTOR3));
	UINT numPasses=0;
	mWaterFX->Begin(&numPasses,0);
	mWaterFX->BeginPass(0);
	mWater->DrawSubset(0);
	mWaterFX->EndPass();
	mWaterFX->End();
}