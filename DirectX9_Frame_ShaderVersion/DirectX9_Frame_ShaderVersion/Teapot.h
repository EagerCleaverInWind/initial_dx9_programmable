#pragma once
#include "Utility.h"
#include "Sky.h"

#define ENVMAPSIZE 256

class Teapot
{
private:
	ID3DXMesh* mTeapot;
	Mtrl			mMtrl;
	Light			mLight;	

	ID3DXMesh* mOrbiter;
	D3DXMATRIX mOrb1InitWorld;
	D3DXMATRIX mOrb2InitWorld;
	D3DXMATRIX mOrb1MatWorld;
	D3DXMATRIX mOrb2MatWorld;
	std::vector<Mtrl> mOrbMtrls;
	std::vector<IDirect3DTexture9*> mOrbTextures;

	IDirect3DCubeTexture9* mCubeTex;
	IDirect3DSurface9* mDepthStencilSurface;

	ID3DXEffect*			mFX;
	D3DXHANDLE         mhTech;
	D3DXHANDLE			mhWorld;
	D3DXHANDLE			mhWorldInvTrans;
	D3DXHANDLE         mhWVP;
	D3DXHANDLE         mhLight;
	D3DXHANDLE			mhMtrl;
	D3DXHANDLE			mhEyePosW;
	D3DXHANDLE         mhTex0;
	D3DXHANDLE			mhEnvMap;

private:
	void BuildFX();
	void genSphericalTexCoords();
	D3DXMATRIX GetCubeMapViewMatrix( DWORD dwFace );//note默认的前提是cubemapped mesh位于世界坐标系原点

	void DrawOrbiters(D3DXMATRIX& matViewProj);

public:
	Teapot();
	~Teapot();

	void SetEnvMap(IDirect3DCubeTexture9* envmap);
	void SetLight(Light& newLight);
	void SetMtrl(Mtrl& newMtrl);

	void Update(float dt);
	void Draw(Sky* pSky);
};