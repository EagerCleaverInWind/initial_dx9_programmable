#include "DirectInput.h"

DirectInput* e_pDInput = 0;

DirectInput::DirectInput(HINSTANCE hInstance, HWND hwnd):
	m_pDirectInput(NULL),m_pKeyboardDevice(NULL),m_pMouseDevice(NULL)
{
	ZeroMemory(m_keyBuffer,sizeof(m_keyBuffer));
	ZeroMemory(&m_MouseState,sizeof(m_MouseState));

	DirectInput8Create(hInstance,DIRECTINPUT_VERSION,IID_IDirectInput8,(LPVOID*)&m_pDirectInput,NULL);
	m_pDirectInput->CreateDevice(GUID_SysKeyboard, &m_pKeyboardDevice,NULL);
	m_pDirectInput->CreateDevice(GUID_SysMouse, &m_pMouseDevice,NULL);

	m_pKeyboardDevice->SetDataFormat(&c_dfDIKeyboard);
	m_pKeyboardDevice->SetCooperativeLevel(hwnd,DISCL_FOREGROUND|DISCL_NONEXCLUSIVE);
	//如果设置独占模式消息处理就没反应啦！

	m_pMouseDevice->SetDataFormat(&c_dfDIMouse);
	m_pMouseDevice->SetCooperativeLevel(hwnd,DISCL_FOREGROUND|DISCL_NONEXCLUSIVE);
	//如果设置独占模式鼠标指针将隐藏

}



void	DirectInput::Poll()
{
	// Poll keyboard.
	HRESULT hr = m_pKeyboardDevice->GetDeviceState(sizeof(m_keyBuffer),m_keyBuffer); 
	if( FAILED(hr) )
	{
		// Keyboard lost, zero out keyboard data structure.
		ZeroMemory(m_keyBuffer, sizeof(m_keyBuffer));

		// Try to acquire for next time we poll.
		hr = m_pKeyboardDevice->Acquire();
	}

	// Poll mouse.
	hr = m_pMouseDevice->GetDeviceState(sizeof(m_MouseState),&m_MouseState); 
	if( FAILED(hr) )
	{
		// Mouse lost, zero out mouse data structure.
		ZeroMemory(&m_MouseState, sizeof(m_MouseState));

		// Try to acquire for next time we poll.
		hr =m_pMouseDevice->Acquire(); 
	}
}



DirectInput::~DirectInput()
{
	m_pKeyboardDevice->Unacquire();
	m_pKeyboardDevice->Release();

	m_pMouseDevice->Unacquire();
	m_pMouseDevice->Release();

	m_pDirectInput->Release();
}