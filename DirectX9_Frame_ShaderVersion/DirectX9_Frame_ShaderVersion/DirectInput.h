#pragma  once
#include "Utility.h"
#include <dinput.h>
// 使用DirectInput必须包含的库文件:
#pragma comment(lib, "dinput8.lib")     
#pragma comment(lib,"dxguid.lib")

enum    MouseButton  {MOUSELEFTBUTTON=0,MOUSERIGHTBUTTON=1};//防止填入范围外的int导致在方法内部出错

class DirectInput
{
private:
	IDirectInput8					*m_pDirectInput;//DI接口
	IDirectInputDevice8			*m_pKeyboardDevice;//DI键盘设备
	char									m_keyBuffer[256];//键盘缓存

	IDirectInputDevice8			*m_pMouseDevice;//DI鼠标设备
	DIMOUSESTATE              m_MouseState;//鼠标缓存结构体

public:
	DirectInput(HINSTANCE hInstance,HWND hwnd);
	~DirectInput();

public:
	void				Poll();

	bool				IsKeyDown(int iKey)	{return	(m_keyBuffer[iKey]&0x80);}

	bool				MouseButtonDown(MouseButton button)	{return	(m_MouseState.rgbButtons[button]&0x80);}
	float				MouseDX()	{return m_MouseState.lX;	}
	float				MouseDY()	{return m_MouseState.lY;	}
	float				MouseDZ()	{return m_MouseState.lZ;	}

};

extern DirectInput* e_pDInput;