uniform extern texture  gPreviousHeightTex;
uniform extern texture  gCurrentHeightTex;
uniform extern int gWidth;
uniform extern int gHeight;

sampler PreviousHeightS=sampler_state
{
	Texture = <gPreviousHeightTex>;
	MinFilter = POINT;
	MagFilter = POINT;
	MipFilter=NONE;
	AddressU  = CLAMP;
    	AddressV  = CLAMP;
};

sampler CurrrentHeightS=sampler_state
{
	Texture = <gCurrentHeightTex>;
	MinFilter = POINT;
	MagFilter = POINT;
	MipFilter=NONE;
	AddressU  = CLAMP;
    	AddressV  = CLAMP;
};

//algorithm parameters
static const float C = 0.5f ; // ripple speed
static const float D = 0.18f ; // distance
static const float U = 0.1f ; // viscosity
static const float T = 0.13f ; // time
static const float TERM1 = ( 4.0f - 8.0f*C*C*T*T/(D*D) ) / (U*T+2) ;
static const float TERM2 = ( U*T-2.0f ) / (U*T+2.0f) ;
static const float TERM3 = ( 2.0f * C*C*T*T/(D*D) ) / (U*T+2) ;
//another
static const float DAMPENING=0.97f;
static const float WAVE_SPEED_SQUARED=10.0f;
static const float2 POSITION_WEIGHT=float2(1.99f,0.99f);
static const float DELTA_TIME_SQUARED=0.01f;

void SimulationVS(float3 posL :POSITION,
		float2 tex0 :TEXCOORD0,
		out float4 posH :POSITION,
		out float2 oTex0 :TEXCOORD0)
{
	posH=float4(posL,1.0f);
	oTex0=tex0;
}

half4 SimulationPS(float2 tex0 :TEXCOORD0):COLOR
{
	float Uunit=1.0f/(float)gWidth;
	float Vunit=1.0f/(float)gHeight;
	half previousHeight = tex2D(PreviousHeightS, tex0); 
	half currentHeight = tex2D(CurrrentHeightS,tex0);
	//看看是不是边界问题
	float epsilon=0.1*Uunit;
	if(abs(tex0.x- Uunit)<epsilon || abs(tex0.x- 1.0f)<epsilon || abs(tex0.y- Vunit)<epsilon || abs(tex0.y- 1.0f)<epsilon)
	{
		return  half4(currentHeight,0.0f,1.0f,0.0f);
	}
	half height_left= tex2D(CurrrentHeightS, tex0+float2(-Uunit,0.0f));
	half height_right= tex2D(CurrrentHeightS, tex0+float2(Uunit,0.0f));
	half height_up= tex2D(CurrrentHeightS, tex0+float2(0.0f,-Vunit));
	half height_down= tex2D(CurrrentHeightS, tex0+float2(0.0f,Vunit));

	//float acceleration=DAMPENING*WAVE_SPEED_SQUARED*(height_left+height_right+height_up+height_down-4.0*currentHeight);
	//half nextHeight=POSITION_WEIGHT.x*currentHeight- POSITION_WEIGHT.y*previousHeight+DELTA_TIME_SQUARED*acceleration;
	//试试另一个公式(其实就是最正宗的推导式)
	half nextHeight=TERM1*currentHeight+TERM2*previousHeight+TERM3*(height_left+height_right+height_up+height_down);

	//debug用
	//half nextHeight=currentHeight*0.5+height_right*0.5;
	//half nextHeight=-currentHeight;
	
	return half4(nextHeight,0.0f,1.0f,0.0f);//normal copy的时候再来算
}

technique SimulateRecursion
{
	pass P0
	{
		VertexShader = compile vs_3_0 SimulationVS();
        		PixelShader = compile ps_3_0 SimulationPS();

        		ZEnable=FALSE;
        		ZWriteEnable=FALSE;
	}
}






void PushVS(float3 posL :POSITION,
		float2 pushDepth :TEXCOORD0,
		out float4 posH:POSITION,
		out float2 oPushDepth:TEXCOORD0)
{
	posH=float4(posL,1.0f);
	oPushDepth=pushDepth;
}

half4 PushPS(float2 pushDepth:TEXCOORD0):COLOR
{
	return half4(pushDepth.x,0,0,0);
}

technique SimulatePush
{
	pass P0
	{
		VertexShader = compile vs_3_0 PushVS();
        		PixelShader = compile ps_3_0 PushPS();
        		ZEnable=FALSE;
        		ZWriteEnable=FALSE;
        		AlphaBlendEnable=TRUE;
        		DestBlend=ONE;
        		SrcBlend=ONE;
	}
}




//之前push是在VS中，push信息由顶点携带
//试试直接在PS中push
uniform extern float4 gPushCenter;//z component表示push depth,w component表示push radius,假定都已经转换到了texture space
half4 PushAtPS(float2 tex0:TEXCOORD0):COLOR
{
	float dist=distance(tex0,gPushCenter.xy);
	if(dist<gPushCenter.w)
	{
		float pushDepth=gPushCenter.z*smoothstep(gPushCenter.w,0,dist);
		//return half4(pushDepth,0,1,0);
		return half4(pushDepth,0,0,0);

	}
	//return tex2D(CurrrentHeightS,tex0);//好像不支持同时RW？
	//return tex2D(PreviousHeightS,tex0);//不行不行！这样会逆一下！
	//开alpha blend就好了！而不是覆盖
	return half4(0,0,0,0);
}

technique PushAtPSTech
{
	pass P0
	{
		VertexShader = compile vs_3_0 SimulationVS();
        		PixelShader = compile ps_3_0 PushAtPS();

        		ZEnable=FALSE;
        		ZWriteEnable=FALSE;
        		//不停push会因为突然改变位置造成不连续，进而递推出巨大高低差能量爆炸
        		//叠加才是合理的push方式，并且因为散失得更快反而看不出叠加效果偏向保持
        		AlphaBlendEnable=TRUE;
        		DestBlend=ONE;
        		SrcBlend=ONE;
	}
}



uniform extern float2 gGridStep;

float4 CopyPS(float2 tex0:TEXCOORD0):COLOR
{

	float Uunit=1.0f/(float)gWidth;
	float Vunit=1.0f/(float)gHeight;
	//half currentHeight = tex2D(CurrrentHeightS,tex0);
	//half height_left= tex2D(CurrrentHeightS, tex0+float2(-Uunit,0.0f));
	//half height_right= tex2D(CurrrentHeightS, tex0+float2(Uunit,0.0f));
	//half height_up= tex2D(CurrrentHeightS, tex0+float2(0.0f,-Vunit));
	//half height_down= tex2D(CurrrentHeightS, tex0+float2(0.0f,Vunit));
	float currentHeight = tex2D(CurrrentHeightS,tex0).r;
	float height_left= tex2D(CurrrentHeightS, tex0+float2(-Uunit,0.0f)).r;
	float height_right= tex2D(CurrrentHeightS, tex0+float2(Uunit,0.0f)).r;
	float height_up= tex2D(CurrrentHeightS, tex0+float2(0.0f,-Vunit)).r;
	float height_down= tex2D(CurrrentHeightS, tex0+float2(0.0f,Vunit)).r;

	//calculate normal
	float3 normal=float3(0,0,0);
	//左上
	float3 dydx=float3(-gGridStep.x,height_left- currentHeight,0.0f);
	float3 dydz=float3(0.0f,height_up- currentHeight,gGridStep.y);
	normal+=cross(dydx,dydz);
	//右上
	dydx=float3(gGridStep.x,height_right- currentHeight,0.0f);
	dydz=float3(0.0f,height_up- currentHeight,gGridStep.y);
	normal+=cross(dydz,dydx);
	//左下
	dydx=float3(-gGridStep.x,height_left- currentHeight,0.0f);
	dydz=float3(0.0f,height_down- currentHeight,-gGridStep.y);
	normal+=cross(dydz,dydx);
	//右下
	dydx=float3(gGridStep.x,height_right- currentHeight,0.0f);
	dydz=float3(0.0f,height_down- currentHeight,-gGridStep.y);
	normal+=cross(dydx,dydz);
	normal=normalize(normal);

	return float4(currentHeight,normal);
}

technique BuildDisplacementMap
{
	pass P0
	{
		VertexShader = compile vs_3_0 SimulationVS();
        		PixelShader = compile ps_3_0 CopyPS();

        		ZEnable=FALSE;
        		ZWriteEnable=FALSE;
	}
}


