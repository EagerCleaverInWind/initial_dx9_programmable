uniform extern float4x4 gWVP;
uniform extern texture  gTex;
uniform extern float3   gEyePosL;
uniform extern float3   gAccel;
uniform extern float    gTime;
uniform extern int      gViewportHeight;

sampler TexS = sampler_state
{
	Texture = <gTex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = POINT;
	AddressU  = CLAMP;
    AddressV  = CLAMP;
};
 
struct OutputVS
{
    float4 posH  : POSITION0;
    float2 tex0  : TEXCOORD0; // D3D fills automatically in for point sprites.
    float4 color :COLOR0;//一开始用的是TEXCOORD1，发现传递不了颜色，D3D还是会当作纹理坐标自动填
    float size   : PSIZE; // In pixels.
};

OutputVS FireWorkVS(float3 posL    : POSITION0, 
                float3 vel     : TEXCOORD0,
                float size     : TEXCOORD1,
                float time     : TEXCOORD2,
                float lifeTime : TEXCOORD3,
                float mass     : TEXCOORD4,
                float4 color   : COLOR0)
{
    // Zero out our output.
	OutputVS outVS = (OutputVS)0;

	outVS.color=color;

	posL+=vel*(gTime-time);
	
	// Transform to homogeneous clip space.
	outVS.posH = mul(float4(posL, 1.0f), gWVP);

	// Compute size as a function of the distance from the camera,
	// and the viewport heights.  The constants were found by 
	// experimenting.
	float d = distance(posL, gEyePosL);
	outVS.size = gViewportHeight*size/(1.0f + 8.0f*d);

	// Done--return the output.
    	return outVS;
}

float4 FireWorkPS(float2 tex0 : TEXCOORD0,float4 color:COLOR0) : COLOR
{
	return tex2D(TexS, tex0)*color;
}

technique FireWorkTech
{
    pass P0
    {
        vertexShader = compile vs_2_0 FireWorkVS();
        pixelShader  = compile ps_2_0 FireWorkPS();
        
        PointSpriteEnable = true;
        AlphaBlendEnable = true;
	      SrcBlend     = ONE;
	      DestBlend    =ONE;
    }
}