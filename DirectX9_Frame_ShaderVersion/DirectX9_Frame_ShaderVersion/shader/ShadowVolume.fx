struct Mtrl
{
	float4 ambient;
	float4 diffuse;
	float4 specular;
	float  specPower;
};

struct Light
{
	float4 ambient;
	float4 diffuse;
	float4 specular;
	float4 lightW;//direction or postion in world space  
};

uniform extern float4x4 gWorld;
uniform extern float4x4 gWorldInvTrans;
uniform extern float4x4 gView;
uniform extern float4x4 gProj;
uniform extern float4x4 gWV;
uniform extern float4x4 gWVP;
uniform extern Mtrl     gMtrl;
uniform extern Light     gLight;
uniform extern float3   gEyePosW;
uniform extern texture  gTex;
uniform extern float gFarClip;
uniform extern float4 gShadowVolumeColor;

sampler Tex0S = sampler_state
{
	Texture = <gTex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = WRAP;
    	AddressV  = WRAP;
};

//render scene with ambient only
void SceneAmbientVS(float3 posL :POSITION,
	float2 tex0 :TEXCOORD0,
	out float4 posH:POSITION,
	out float2 oTex0:TEXCOORD0)
{
	posH=mul(float4(posL,1.0f),gWVP);
	oTex0=tex0;
}

float4 SceneAmbientPS(float2 tex0:TEXCOORD0):COLOR0
{
	return gMtrl.ambient*gLight.ambient* tex2D(Tex0S, tex0);
}

technique RenderSceneAmbient
{
	pass P0
	{
		VertexShader=compile vs_2_0 SceneAmbientVS();
		PixelShader=compile ps_2_0 SceneAmbientPS();

		StencilEnable=FALSE;
		ZFunc=LESSEQUAL;

	}
}


//render shadow volume, i.e. mark the pixels in the shadow volume in the stencil buffer
void ShadowVolumeVS(float3 posL:POSITION,
	float3 normalL:NORMAL,
	out float4 posH:POSITION)
{
	float3 normalV=mul(float4(normalL,0.0f),gWV).xyz;

	float3  posV=mul(float4(posL,1.0f),gWV).xyz;
	float4 lightV=mul(gLight.lightW,gView);
	float3 lightVecV;
	if(lightV.w==0.0f)
	{
		lightVecV=lightV.xyz;
	}
	else
	{
		lightVecV=posV-lightV.xyz;
	}
	lightVecV=normalize(lightVecV);

	// Perform reverse vertex extrusion
   	// Extrude the vertex away from light if it's facing away from the light.
	if(dot(normalV,-lightVecV) < 0.0f)
	{
		//if(lightVecV.z>0)
		//{
			//posV += lightVecV * ( gFarClip - posV.z ) / lightVecV.z;
			//posH=mul(float4(posV,1.0f),gProj);

		//}
		//else
			//posH=mul(float4(lightVecV,0.0f),gProj);//求极限可知两者(lightVecV和延伸至无穷远处的顶点)在近平面的投影是相同的


		//哈哈，奇淫技巧不如来蛮的直接延伸
		posV+=lightVecV*10000.0f;
		//posV+=lightVecV*20000.0f;
		//posV+=lightVecV*200000.0f;
		//这里涉及的是延伸过长越界问题，上面的trick单独考虑lightVecV.z>0时的情况也是为了解决这个问题
		posH=mul(float4(posV,1.0f),gProj);
	}
	else
	{
		posH=mul(float4(posL,1.0f),gWVP);
	}

}

float4 ShadowVolumePS():COLOR0
{
	return float4(gShadowVolumeColor.rgb,0.1f);
}

technique RenderShadowVolume
{
 pass P0
    {
        VertexShader = compile vs_2_0 ShadowVolumeVS();
        PixelShader  = compile ps_2_0 ShadowVolumePS();
        CullMode = Ccw;
        // Disable writing to the frame buffer
        AlphaBlendEnable = true;
        SrcBlend = Zero;
        DestBlend = One;
        // Disable writing to depth buffer
        ZWriteEnable = false;
        ZFunc = Less;
        // Setup stencil states
        StencilEnable = true;
        StencilRef = 1;
        StencilMask = 0xFFFFFFFF;
        StencilWriteMask = 0xFFFFFFFF;
        StencilFunc = Always;
        StencilZFail = Decr;
        StencilPass = Keep;
    }
    pass P1
    {
        VertexShader = compile vs_2_0 ShadowVolumeVS();
        PixelShader  = compile ps_2_0 ShadowVolumePS();
        CullMode = Cw;
        StencilZFail = Incr;
    }
}

technique ShowShadowVolume
{
    pass P0
    {
        VertexShader = compile vs_2_0 ShadowVolumeVS();
        PixelShader  = compile ps_2_0 ShadowVolumePS();
        CullMode = Ccw;
        AlphaBlendEnable = true;
        SrcBlend = SrcAlpha;
        DestBlend = InvSrcAlpha;
        // Disable writing to depth buffer
        ZWriteEnable = false;
        ZFunc = Less;
        // Setup stencil states
        StencilEnable = true;
        StencilRef = 1;
        StencilMask = 0xFFFFFFFF;
        StencilWriteMask = 0xFFFFFFFF;
        StencilFunc = Always;
        StencilZFail = Decr;
        StencilPass = Keep;
    }
    pass P1
    {
        VertexShader = compile vs_2_0 ShadowVolumeVS();
        PixelShader  = compile ps_2_0 ShadowVolumePS();
        CullMode = Cw;
        StencilZFail = Incr;
    }
}


//render scene
void SceneVS(float3 posL:POSITION,
	float3 normalL:NORMAL,
	float2 tex0:TEXCOORD0,
	out float4 posH:POSITION,
	out float2 oTex0:TEXCOORD0,
	out float3 oPosW:TEXCOORD1,//for lightVecW&toEyeW
	out float3 oNormalW:TEXCOORD2)
{
	posH=mul(float4(posL,1.0f),gWVP);
	oTex0=tex0;
	oPosW=mul(float4(posL,1.0f),gWorld).xyz;
	oNormalW=mul(float4(normalL,0.0f),gWorldInvTrans).xyz;

}

float4 ScenePS(float2 tex0:TEXCOORD0,
	float3 posW:TEXCOORD1,
	float3 normalW:TEXCOORD2):COLOR0
{
	float3 lightVecW;
	if(gLight.lightW.w==0.0f)
		lightVecW=gLight.lightW.xyz;
	else
		lightVecW=posW-gLight.lightW.xyz;
	lightVecW=normalize(lightVecW);

	float3 toEyeW=gEyePosW-posW;
	toEyeW=normalize(toEyeW);

	//float4 ambientCol=gLight.ambient*gMtrl.ambient;
	//最开始已经用ambient渲染一遍了，这一遍不考虑进去，alpha ADD第一遍的即可

	float s=saturate(dot(-lightVecW,normalW));//点光源情况没有考虑进distance attenuation coefficient
	float4 diffuseCol=s*gLight.diffuse*gMtrl.diffuse;

	float3 reflectVecW=reflect(lightVecW,normalW);
	float t=pow(saturate(dot(reflectVecW,toEyeW)),gMtrl.specPower);
	float4 specularCol=t*gLight.specular*gMtrl.specular;

	return float4(  (diffuseCol*tex2D(Tex0S, tex0) + specularCol).rgb ,diffuseCol.a);

}

technique RenderScene
{
	pass P0
	{
		VertexShader=compile vs_2_0 SceneVS();
		PixelShader=compile ps_2_0 ScenePS();

		 ZEnable = true;
       		 ZFunc = LessEqual;
        		StencilEnable = true;
        		AlphaBlendEnable = true;
        		BlendOp = Add;
        		SrcBlend = One;
        		DestBlend = One;
       		StencilRef = 1;
        		StencilFunc = Greater;
        		StencilPass = Keep;
	}
}