//render points
uniform extern float gTime;
uniform extern float2 gHalfW;

void PointVS(float2 posL :POSITION0,
	float2 velL :TEXCOORD0,
	float time :TEXCOORD1,
	//float amptitude :COLOR0,
	//float amptitude :TEXCOORD2,
	float amptitude :BLENDWEIGHT0,
	out float4 posH :POSITION0,
	//out float oColor :COLOR0,
	//out float oColor :TEXCOORD2,
	out float oColor :BLENDWEIGHT0,
	out float oSize :PSIZE// In pixels.
	)
{
	oColor=amptitude;
	oSize=2;
	posL+=velL*(gTime-time);
	//变换到投影面，-1~1
	posH=float4(posL/gHalfW,0.0f,1.0f);
}

float4 PointPS(float color:BLENDWEIGHT0):COLOR
{
	return float4(color,0,0,0);
	//return float4(-color,0,0,0);
	//正amptitude测试时能显示成负的！说明能保留负值！问题出在:COLOR0上！因为代表颜色，会自动clamp!
}

technique PointTech
{
	pass P0
	{

		//vertexShader = compile vs_2_0 PointVS();
		//for BLENDWEIGHT semantic
		vertexShader = compile vs_3_0 PointVS();
       		//pixelShader  = compile ps_2_0 PointPS();
       		pixelShader  = compile ps_3_0 PointPS();
        
        		PointSpriteEnable = true;
        		AlphaBlendEnable = true;
	      	SrcBlend     = ONE;
	      	DestBlend    =ONE;

	      	ZEnable=FALSE;
        		ZWriteEnable=FALSE;
	}
}










//filter points 
//uniform extern float gFilterRadius;
uniform extern float gRadiusL;
uniform extern float2 gStepLength;
uniform extern int2 gN;//最多能跨越的texel数，=(int)gRadiusL/gStepLength
uniform extern float2 resolutionInverse;
uniform extern texture gPointsTex;

sampler TexS=sampler_state
{
	Texture = <gPointsTex>;
	MinFilter = POINT;
	MagFilter = POINT;
	//MinFilter = LINEAR;
	//MagFilter = LINEAR;
	MipFilter=NONE;
	//AddressU  = CLAMP;
    	//AddressV  = CLAMP;
    	AddressU  = MIRROR;
    	AddressV  = MIRROR;
};

void FilterVS(float3 posL :POSITION,
		float2 tex0 :TEXCOORD0,
		out float4 posH :POSITION,
		out float2 oTex0 :TEXCOORD0)
{
	posH=float4(posL,1.0f);
	oTex0=tex0;
}

#define MAX_CROSS_NUM_X 3
#define MAX_CROSS_NUM_Y 3
//先试一下最基本的双向filter吧
float4 BidirectionalFilterPS(float2 tex0:TEXCOORD0):COLOR
{
	float3 sumDeviation=float3(0.0f,0.0f,0.0f);
	//for(int i=-gN.x;i<=gN.x;i++)
	for(int i=-MAX_CROSS_NUM_X;i<=MAX_CROSS_NUM_X;i++)
	{
		//for(int j=-gN.y;j<=gN.y;j++)
		for(int j=-MAX_CROSS_NUM_Y;j<=MAX_CROSS_NUM_Y;j++)
		{
			if(i==0&&j==0)
			{
				float result=tex2D(TexS, tex0).r;
				if(abs(result)>0.01f)
				{
					sumDeviation.y+=result;
					//sumDeviation.y-=result;
				}
			}
			else
			{
				float2 vecL=float2(i*gStepLength.x,j*gStepLength.y);
				float distL=length(vecL);
				if(distL<=gRadiusL)
				{
					float result=tex2D(TexS, tex0+float2(i*resolutionInverse.x,-j*resolutionInverse.y)).r;
					//if(result>0.01f)//哈哈，这样就只允许正的amptitude通过了，忽略了负的情况
					//一开始测试时负的没反应还以为没用浮点纹理。。。
					if(abs(result)>0.01f)
					{
						float vertical=0.5*(cos(3.14159*distL/gRadiusL)+1)*result;
						sumDeviation.y+=vertical;
						//sumDeviation.y-=vertical;
						//测试时正的能显示成负的，负的却还是没反应（已改了abs），
						//说明后面的渲染都没问题，问题出在point texture上
						//sumDeviation.xz+=sqrt(2)*sin(3.14159*distL/gRadiusL)*vertical*vecL/distL;//偏向particle center
					}
				}
			}
			
		}
	}

	return float4(sumDeviation,0.0f);
}

technique BidirectionalFilter
{
	pass P0
	{
		vertexShader = compile vs_2_0 FilterVS();
       		pixelShader  = compile ps_3_0 BidirectionalFilterPS();//用2.0循环那里寄存器数不足。。。

       		ZEnable=FALSE;
        		ZWriteEnable=FALSE;
	}
}










//render normal，已经统一左手坐标系
//uniform extern float2 gStepLength;
//uniform extern float2 resolutionInverse;
uniform extern texture  gDisplacementTex;
sampler DisplacementS=sampler_state
{
	Texture = < gDisplacementTex>;
	MinFilter = POINT;
	MagFilter = POINT;
	MipFilter=NONE;
	AddressU  = CLAMP;
    	AddressV  = CLAMP;
};
float4 NormalPS(float2 tex0:TEXCOORD0):COLOR
{
	float3 current=tex2D(DisplacementS, tex0).rgb;
	float3 left=tex2D(DisplacementS, tex0+float2(-resolutionInverse.x,0.0f)).rgb;
	float3 right=tex2D(DisplacementS, tex0+float2(resolutionInverse.x,0.0f)).rgb;
	float3 up=tex2D(DisplacementS, tex0+float2(0.0f,-resolutionInverse.y)).rgb;
	float3 down=tex2D(DisplacementS, tex0+float2(0.0f,resolutionInverse.y)).rgb;

	//calculate normal
	float3 normal=float3(0,0,0);
	//左上
	float3 toLeft=left-current+float3(-gStepLength.x,0.0f,0.0f);
	float3 toUp=up-current+float3(0.0f,0.0f,gStepLength.y);
	normal+=cross(toLeft,toUp);
	//右上
	float3 toRight=right-current+float3(gStepLength.x,0.0f,0.0f);
	normal+=cross(toUp,toRight);
	//左下
	float3 toDown=down-current+float3(0.0f,0.0f,-gStepLength.y);
	normal+=cross(toDown,toLeft);
	//右下
	normal+=cross(toRight,toDown);
	normal=normalize(normal);

	//normal=(normal+1.0f)/2.0f;//D3DFMT_R8G8B8格式
	return float4(normal,0.0f);
}

technique CalculateNormal
{
	pass P0
	{
		VertexShader = compile vs_2_0 FilterVS();
        		PixelShader = compile ps_2_0 NormalPS();

        		ZEnable=FALSE;
        		ZWriteEnable=FALSE;
	}
}









//render water surface
struct Mtrl
{
	float4 ambient;
	float4 diffuse;
	float4 spec;
	float  specPower;
};

struct Light
{
	float4 ambient;
	float4 diffuse;
	float4 spec;
	float4 lightW;//direction or postion in world space  
};

uniform extern float4x4 gWorld;
uniform extern float4x4 gWorldInvTrans;
uniform extern float4x4 gWVP;
uniform extern Mtrl     gMtrl;
uniform extern Light     gLight;
uniform extern float3   gEyePosW;
//uniform extern texture  gDisplacementTex;
uniform extern texture gDisplacementNormalTex;//不是normal texture哦
uniform extern texture gEnvMap;

/*sampler DisplacementS=sampler_state
{
	Texture = < gDisplacementTex>;
	MinFilter = POINT;
	MagFilter = POINT;
	MipFilter=NONE;
	AddressU  = CLAMP;
    	AddressV  = CLAMP;
};*/

sampler NormalS=sampler_state
{
	Texture = < gDisplacementNormalTex>;
	MinFilter = POINT;
	MagFilter = POINT;
	MipFilter=NONE;
	AddressU  = CLAMP;
    	AddressV  = CLAMP;
};

sampler EnvMapS = sampler_state
{
	Texture = <gEnvMap>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = WRAP;
          AddressV  = WRAP;
};

void WaterVS(float3 posL :POSITION,
	float2 tex0 :TEXCOORD0,
	out float4 posH :POSITION,
	out float3 normalW : TEXCOORD0,
    	out float3 posW  : TEXCOORD1)
{
	float3 displacementValue=tex2Dlod(DisplacementS, float4(tex0,0,0)).rgb;//查一下是不是必须要用float4接收
	posL+=displacementValue;
	float3 normalL=tex2Dlod(NormalS, float4(tex0,0,0)).rgb;
	//normalL=normalL*2.0f-1.0f;

	posH=mul(float4(posL,1.0f),gWVP);
	normalW=mul(float4(normalL,0.0f),gWorldInvTrans).xyz;
	posW=mul(float4(posL,1.0f),gWorld).xyz;
}

float4 WaterPS(float3 normalW:TEXCOORD0,
		float3 posW:TEXCOORD1):COLOR
{
	float3 lightVecW=gLight.lightW.xyz;
	float disAttenuation=1.0f;
	if(gLight.lightW.w==1.0f)
	{
		lightVecW=posW-lightVecW;
		disAttenuation=saturate(1.0f/dot(lightVecW,lightVecW));
	}
	lightVecW=normalize(lightVecW);

	normalW=normalize(normalW);
	float s=saturate(dot(-lightVecW,normalW));
	float4 diffuseCol=s*gMtrl.diffuse*gLight.diffuse;

	float3 toEyeW=normalize(gEyePosW-posW);
	float3 reflectVecW=reflect(lightVecW,normalW);
	float t=pow(saturate(dot(toEyeW,reflectVecW)),gMtrl.specPower);
	float4 specularCol=t * gLight.spec *  gMtrl.spec;

	float4 ambientCol=gLight.ambient*gMtrl.ambient;

	float3 indexVec=reflect(-toEyeW,normalW);
	float4 relectCol=texCUBE(EnvMapS, indexVec);
	return disAttenuation*(ambientCol+diffuseCol)*relectCol+specularCol;
}

technique Water
{
	pass P0
	{
		VertexShader = compile vs_3_0 WaterVS();
        		PixelShader = compile ps_2_0 WaterPS();
	}
}