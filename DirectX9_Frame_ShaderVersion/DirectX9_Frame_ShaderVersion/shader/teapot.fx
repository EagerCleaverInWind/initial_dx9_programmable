struct Mtrl
{
	float4 ambient;
	float4 diffuse;
	float4 spec;
	float  specPower;
};

struct Light
{
	float4 ambient;
	float4 diffuse;
	float4 spec;
	float4 lightW;//direction or postion in world space  
};

uniform extern float4x4 gWorld;
uniform extern float4x4 gWorldInvTrans;
uniform extern float4x4 gWVP;
uniform extern Mtrl     gMtrl;
uniform extern Light     gLight;
uniform extern float3   gEyePosW;
uniform extern texture  gTex;
uniform extern texture  gEnvMap;

sampler Tex0S = sampler_state
{
	Texture = <gTex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = WRAP;
    	AddressV  = WRAP;
};

sampler EnvMapS = sampler_state
{
	Texture = <gEnvMap>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = WRAP;
          AddressV  = WRAP;
};


//Vertex shader
struct InputVS
{
	float3 posL : POSITION0;
	float3 normalL : NORMAL0;
	//float2 tex0: TEXCOORD0
};

struct OutputVS
{
    float4 posH    : POSITION0;
    //float2 tex0    : TEXCOORD0;
    float3 normalW : TEXCOORD1;
    float3 toEyeW  : TEXCOORD2;
};

OutputVS TeapotVS(InputVS input)
{
	OutputVS output=(OutputVS)0;

	output.posH=mul(float4(input.posL,1.0f),gWVP);

	output.normalW=mul(float4(input.normalL,0.0f),gWorldInvTrans).xyz;

	float3 posW=mul(float4(input.posL,1.0f),gWorld).xyz;
	output.toEyeW=gEyePosW-posW;

	return output;
}


//PixelShader

//to be integrated
// How much does the surface reflect?  Normally this should be a
// property of the material since it may vary over a surface.
static float gReflectivity = 0.3f;
static float gTransparency=0.7f;//利用最终输出颜色的a值即在源码中开启alpha blending并不能代替折射相关shade，因为无法模拟光线扭曲
static float4 texColor=float4(1,1,1,1);
static float gLightIntensity=2.7f;

struct InputPS
{
    //float2 tex0 : TEXCOORD0;
    float3 normalW : TEXCOORD1;
    float3 toEyeW  : TEXCOORD2;
};

float4 TeapotPS(InputPS input):COLOR
{
	// Interpolated normals can become unnormal--so normalize.
	input.normalW=normalize(input.normalW);
	input.toEyeW=normalize(input.toEyeW);

	// Compute the reflection vector.
	float3 lightVecW=gLight.lightW.xyz;
	lightVecW=normalize(lightVecW);
	float3 r=reflect(lightVecW,input.normalW);
	// Determine how much (if any) specular light makes it into the eye.
	float t  = pow(max(dot(r, input.toEyeW), 0.0f), gMtrl.specPower);
	//compute the spec
	float3 spec = t*(gMtrl.spec*gLight.spec).rgb;
	
	// Determine the diffuse light intensity that strikes the vertex.
	float s = max(dot(-lightVecW, input.normalW), 0.0f);

	//compute incident light vector for reflection.
	float3 incident=-reflect(-input.toEyeW,input.normalW);
	//sample the reflection color.
	float4 reflectedColor=texCUBE(EnvMapS, -incident);


	// Weighted average between the reflected color, and usual
	// diffuse/ambient material color modulated with the texture color.
	//float3 ambientMtrl = gReflectivity*reflectedColor + (1.0f-gReflectivity)*(gMtrl.ambient*texColor);
	//float3 diffuseMtrl = gReflectivity*reflectedColor + (1.0f-gReflectivity)*(gMtrl.diffuse*texColor);
	//float3 diffuse = s*(diffuseMtrl*gLight.diffuse.rgb);
	//float3 ambient = ambientMtrl*gLight.ambient;
	//float3 final = ambient + diffuse;// + spec;



	 // Compute the reflection vector using Snell's law
   	// the refract HLSL function does not always work properly
   	// n_i * sin(theta_i) = n_r  * sin(theta_r)
   	// sin(theta_i)
   	float cosine = dot(input.toEyeW, input.normalW);
  	float sine = sqrt(1 - cosine * cosine);
   	// sin(theta_r)
   	float sine2 = saturate(1.14 * sine);
  	float cosine2 = sqrt(1 - sine2 * sine2);
   	// Determine the refraction vector be using the normal and tangent
  	 // vectors as basis to determine the refraction direction
   	float3 x = -input.normalW;
   	float3 y = normalize(cross(cross(input.toEyeW, input.normalW), input.normalW));
   	float3 inRefract = x * cosine2 + y * sine2;
   	//sample the refraction color.
   	float3 refractedColor=texCUBE(EnvMapS, inRefract).rgb;

	//入射光经过材质作用后的光
	float3 ambientLight=(gMtrl.ambient*gLight.ambient).rgb;
	float3 diffuseLight=(s*gMtrl.diffuse*gLight.diffuse).rgb;

	float3 final=(ambientLight+diffuseLight)
		*(	
			sine*gReflectivity*reflectedColor
			+(1-sine2)*gTransparency*refractedColor
			+(1.0f-gReflectivity-gTransparency)*texColor.rgb
		 )+spec;
	final*=gLightIntensity;


	return float4(final, gMtrl.diffuse.a*texColor.a);
}


technique TeapotTech
{
    pass P0
    {
        // Specify the vertex and pixel shader associated with this pass.
        vertexShader = compile vs_2_0 TeapotVS();
        pixelShader  = compile ps_2_0 TeapotPS();

        // Cylindrically interpolate texture coordinates.
        Wrap0=COORD0;
    }
}


//render orbiters
void OrbitersVS(float3 posL:POSITION,
	float3 normalL:NORMAL,
	float2 tex0:TEXCOORD0,
	out float4 posH:POSITION,
	out float2 oTex0:TEXCOORD0,
	out float3 oNormalW:TEXCOORD1,
	out float3 oPosW:TEXCOORD2)
{
	posH=mul(float4(posL,1.0f),gWVP);
	oTex0=tex0;
	oNormalW=mul(float4(normalL,0.0f),gWorldInvTrans).xyz;
	oPosW=mul(float4(posL,1.0f),gWorld).xyz;
}

float4 OrbitersPS(float2 tex0:TEXCOORD0,float3 normalW:TEXCOORD1,float3 posW:TEXCOORD2,uniform bool ifSpecular):COLOR
{
	float3 lightVecW=gLight.lightW.xyz;
	float disAttenuation=1.0f;
	if(gLight.lightW.w==1.0f)
	{
		lightVecW=posW-lightVecW;
		disAttenuation=saturate(1.0f/dot(lightVecW,lightVecW));//1.0f调到100.0f才有亮。。。
	}
	lightVecW=normalize(lightVecW);

	normalW=normalize(normalW);
	float s=saturate(dot(-lightVecW,normalW));
	float4 diffuseCol=s*gMtrl.diffuse*gLight.diffuse;

	float4 specularCol=float4(0,0,0,0);
	if(ifSpecular)
	{
		float3 toEyeW=gEyePosW-posW;
		toEyeW=normalize(toEyeW);
		float3 reflectVecW=reflect(lightVecW,normalW);
		float t=pow(saturate(dot(toEyeW,reflectVecW)),gMtrl.specPower);
		specularCol=t * gLight.spec *  gMtrl.spec;
	}

	float4 ambientCol=gLight.ambient * gMtrl.ambient;

	float3 color=( (diffuseCol+ambientCol)*disAttenuation* tex2D(Tex0S, tex0)+specularCol ).rgb;
	return float4(color,diffuseCol.a);

}

technique RenderOrbiters
{
	pass P0
	{
		VertexShader = compile vs_2_0 OrbitersVS();
        		PixelShader = compile ps_2_0 OrbitersPS(true);
	}
}

technique RenderOrbitersInMirror
{
	pass P0
	{
		VertexShader = compile vs_2_0 OrbitersVS();
        		PixelShader = compile ps_2_0 OrbitersPS(false);
	}
}