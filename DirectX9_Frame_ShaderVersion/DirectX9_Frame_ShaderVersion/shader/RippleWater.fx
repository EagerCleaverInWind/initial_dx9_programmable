
struct Mtrl
{
	float4 ambient;
	float4 diffuse;
	float4 spec;
	float  specPower;
};

struct Light
{
	float4 ambient;
	float4 diffuse;
	float4 spec;
	float4 lightW;//direction or postion in world space  
};

uniform extern float4x4 gWorld;
uniform extern float4x4 gWorldInvTrans;
uniform extern float4x4 gWVP;
uniform extern Mtrl     gMtrl;
uniform extern Light     gLight;
uniform extern float3   gEyePosW;
uniform extern texture  gDisplacementTex;
uniform extern texture gStreamBottomTex;
uniform extern texture gEnvMap;

sampler DisplacementS=sampler_state
{
	Texture = < gDisplacementTex>;
	MinFilter = POINT;
	MagFilter = POINT;
	//MinFilter = LINEAR;
	//MagFilter = LINEAR;
	MipFilter=NONE;
	AddressU  = CLAMP;
    	AddressV  = CLAMP;
};

sampler StreamBottomS=sampler_state
{
	Texture = < gStreamBottomTex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter=LINEAR;
	AddressU  = CLAMP;
    	AddressV  = CLAMP;
};

sampler EnvMapS = sampler_state
{
	Texture = <gEnvMap>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = WRAP;
          AddressV  = WRAP;
};

void WaterVS(float3 posL :POSITION,
	float2 tex0 :TEXCOORD0,
	out float4 posH :POSITION,
	out float3 normalW : TEXCOORD0,
    	out float3 posW  : TEXCOORD1,
    	out float2 oTex0 :TEXCOORD2)
{
	float4 displacementValue=tex2Dlod(DisplacementS, float4(tex0,0,0));
	posL.y=displacementValue.r;
	float4 normalL=float4(displacementValue.gba,0.0f);

	posH=mul(float4(posL,1.0f),gWVP);
	normalW=mul(normalL,gWorldInvTrans).xyz;
	posW=mul(float4(posL,1.0f),gWorld).xyz;
	oTex0=tex0;
}

float4 WaterPS(float3 normalW:TEXCOORD0,
		float3 posW:TEXCOORD1,
		float2 tex0:TEXCOORD2):COLOR
{
	float3 lightVecW=gLight.lightW.xyz;
	float disAttenuation=1.0f;
	if(gLight.lightW.w==1.0f)
	{
		lightVecW=posW-lightVecW;
		disAttenuation=saturate(1.0f/dot(lightVecW,lightVecW));
	}
	lightVecW=normalize(lightVecW);

	normalW=normalize(normalW);
	float s=saturate(dot(-lightVecW,normalW));
	float4 diffuseCol=s*gMtrl.diffuse*gLight.diffuse;

	float3 toEyeW=normalize(gEyePosW-posW);
	float3 reflectVecW=reflect(lightVecW,normalW);
	float t=pow(saturate(dot(toEyeW,reflectVecW)),gMtrl.specPower);
	float4 specularCol=t * gLight.spec *  gMtrl.spec;

	float4 ambientCol=gLight.ambient*gMtrl.ambient;

	float3 indexVec=reflect(-toEyeW,normalW);
	float4 reflectCol=texCUBE(EnvMapS, indexVec);
	float4 refractCol=tex2D(StreamBottomS,tex0-0.25f*float2(normalW.x,-normalW.z));
	//water fresnel
	float fresnelFactor=0.02f+0.98f*pow((1-dot(toEyeW,normalW)),5);
	return disAttenuation*(ambientCol+diffuseCol+specularCol)+fresnelFactor*reflectCol*5.0f+(1-fresnelFactor)*refractCol*0.3f;
}

technique Water
{
	pass P0
	{
		VertexShader = compile vs_3_0 WaterVS();
        		PixelShader = compile ps_3_0 WaterPS();
	}
}



//debug
float4 DebugPS(float3 normalW:TEXCOORD0,
		float3 posW:TEXCOORD1):COLOR
{
	if(-posW.y>20)
	{
		return float4(1,0,0,1);
	}
	else if(-posW.y>5)
	{
		return float4(0,1,0,1);
	}
	else
	{
		return float4(0,0,1,1);
	}
	//return float4( (normalW+1)*0.5f,1.0f);
}
technique DebugTech
{
	pass P0
	{
		VertexShader = compile vs_3_0 WaterVS();
        		PixelShader = compile ps_2_0 DebugPS();
	}
}




//debug2，会不会是tex2Dlod的问题
void WaterVSDebug2(float3 posL :POSITION,
	float2 tex0 :TEXCOORD0,
	out float4 posH :POSITION,
    	out float2 oTex0  : TEXCOORD0)
{
	posH=mul(float4(posL,1.0f),gWVP);
	oTex0=tex0;
}

float4 WaterPSDebug2(float2 tex0 :TEXCOORD0):COLOR
{
	float4 texValue=tex2D(DisplacementS, tex0);
	float outputCol=(texValue.r+250)/500.0f;
	return float4( outputCol,0,0,0);

	//会不会是tex coord问题
	//return float4(tex0.x,tex0.y,0,0);
}
technique Debug2Tech
{
	pass P0
	{
		VertexShader = compile vs_3_0 WaterVSDebug2();
        		PixelShader = compile ps_2_0 WaterPSDebug2();
	}
}




//cpu version
void WaterVSCPUVersion(float3 posL :POSITION,
	float3 normalL :NORMAL0,
	float2 tex0 :TEXCOORD0,
	out float4 posH :POSITION,
	out float3 normalW : TEXCOORD0,
    	out float3 posW  : TEXCOORD1)
{
	posH=mul(float4(posL,1.0f),gWVP);
	normalW=mul(normalL,gWorldInvTrans).xyz;
	posW=mul(float4(posL,1.0f),gWorld).xyz;
}
technique WaterCPUVersion
{
	pass P0
	{
		VertexShader = compile vs_3_0 WaterVSCPUVersion();
        		PixelShader = compile ps_2_0 WaterPS();

        		//FillMode=WIREFRAME;
	}
}





//尝试下函数叠加式ripple
struct WAVE
{
	float2 pos;//相对于water的local space
	float amp;
	float w;
	float age;
	//bool alive;//特么编译的时候说没这么多bool寄存器！
	int alive;
};
#define MAX_WAVE 10	//CPU中的数组长度要和它一致
uniform extern WAVE gWaves[MAX_WAVE];
//uniform extern int gStart;
//uniform extern int gEnd;

uniform extern float gWaveSpeed;//机械波的传播速度只取决去介质，所以不需要放进struct中

void FunctionModeVS(float3 posL :POSITION,
			float2 tex0 :TEXCOORD0,
			out float4 posH :POSITION,
			out float3 normalW : TEXCOORD0,
    			out float3 posW  : TEXCOORD1 )
{
	float3 normalL=float3(0,1,0);
	//for(int i=gStart;i!=gEnd;i=++i%MAX_WAVE)//shader for循环不支持这样的变量限定，只允许常量限定
	for(int i=0;i<MAX_WAVE;++i)
	{

		if(gWaves[i].alive>0)
		{
			float k=gWaves[i].w/gWaveSpeed;//波矢
			float dist=length(float2(posL.xz-gWaves[i].pos));
			float angle=gWaves[i].w*gWaves[i].age-k*dist;
			//if(angle>0)//小于0表示还没传播到该点，就不参与求和了
			if(angle>-1.57f)//为了alleviate外圈狗牙
			{
				angle+=3.14f;//临时改变了一下initial phase
				posL.y+=gWaves[i].amp*sin(angle);
				float pre=-k*gWaves[i].amp*cos(angle)/dist;
				float dydx=pre*(posL.x- gWaves[i].pos.x);
				float dydz=pre*(posL.z- gWaves[i].pos.y);
				normalL+=float3(-dydx,1,-dydz);
			}
		}
		
	}
	normalL=normalize(normalL);

	posH=mul(float4(posL,1.0f),gWVP);
	normalW=mul(float4(normalL,0.0f),gWorldInvTrans).xyz;
	posW=mul(float4(posL,1.0f),gWorld);

}

technique FunctionModeSimulation
{
	pass P0
	{
		VertexShader=compile vs_2_0 FunctionModeVS();
		PixelShader=compile ps_2_0 WaterPS();
	}
}
