#pragma  once
#include "Utility.h"

struct	Object
{
	ID3DXMesh* _mesh;
	ID3DXMesh* _shadowVolume;
	bool _ifCastShadow;
	D3DXMATRIX _matWorld;
	std::vector<Mtrl> _mtrls;
	std::vector<IDirect3DTexture9*> _textures;

	Object()
	{
		_mesh=NULL;
		_shadowVolume=NULL;
		_ifCastShadow=false;
		D3DXMatrixIdentity(&_matWorld);
	}
	~Object()
	{
		SAFE_RELEASE(_mesh);
		if(_ifCastShadow)
			SAFE_RELEASE(_shadowVolume);
	}
};

class ShadowVolume
{
protected:
	std::vector<Object> mObjects;
	std::vector<Object*> mpShadowCasters;
	Light mLight;
	D3DXCOLOR mSVCol;

	ID3DXEffect*	mFX;
	D3DXHANDLE   mhTech;
	D3DXHANDLE   mhWVP;
	D3DXHANDLE	 mhWV;
	D3DXHANDLE   mhWorld;
	D3DXHANDLE	 mhWorldInvTrans;
	D3DXHANDLE	 mhView;
	D3DXHANDLE	 mhProj;
	D3DXHANDLE   mhEyePosW;
	D3DXHANDLE   mhTex;
	D3DXHANDLE   mhMtrl;
	D3DXHANDLE   mhLight;
	D3DXHANDLE	 mhFarClip;
	D3DXHANDLE	 mhSVColor;


protected:
	void BuildFX();
	virtual void LoadObjects()=0;

public:
	ShadowVolume();
	~ShadowVolume();

	virtual void Update(float dt);
	void Draw();
};


class SVDemo:public ShadowVolume
{
private:
	void LoadObjects();

public:
	SVDemo();

	void Update(float dt);
};