//已是paper阶段
#pragma  once
#include "Utility.h"
#include<list>
#include <set>

struct WaveParticle
{
	D3DXVECTOR2 _initialPostion;
	D3DXVECTOR2 _velocity;
	float _initialTime;
	float _amptitude;
	float _dispersionAngle;
	float _damping;//可选
};
//分裂不改变_initialPostion和_initialTime

struct WaveParticleVertex
{
	D3DXVECTOR2 _initialPosL;
	D3DXVECTOR2 _velocityL;
	float _initialTime;
	float _amptitude;
};

class WaveParticleSystem
{
private:
	ID3DXMesh* mWater;
	D3DXMATRIX mMatWorld;

	struct TimetableElement
	{
		std::list<WaveParticle*>::iterator _position;
		float _actTime;

		TimetableElement()
		{

		}

		TimetableElement(std::list<WaveParticle*>::iterator iter,float actTime)
		{
			_position=iter;
			_actTime=actTime;
		}

		friend bool operator<(TimetableElement e1,TimetableElement e2)
		{
			return e1._actTime<e2._actTime;
		}

	};
	float mTime;
	int mMaxNumParticles;
	float mParticleRadiusL;
	std::vector<WaveParticle> mWaveParticles;
	std::list<WaveParticle*> mAliveParticles;
	std::list<WaveParticle*> mDeadParticles;
	std::multiset<TimetableElement> mSubdivisionTimetable;
	//std::set<TimetableElement> mSubdivisionTimetable;
	//哈哈，一开始用的set，发现只有最初的那一支在分裂，因为重载了比较，分裂时间一致的都被set看作相等重复了只保留一个
	//std::set<TimetableElement> mDeadTimetable;
	//不需要，分裂前会去除原particle，如果计算的子particle的amptitude过小，就不新增三个子particle，这样就相当于去除dead particle了
	//反弹暂时不考虑,待实现的还有：与刚体的相互作用、caustics、近处物体的折反射等
	
	LPDIRECT3DTEXTURE9 mPointsMap;
	LPDIRECT3DSURFACE9 mPointsMapSurface;
	LPDIRECT3DTEXTURE9 mDisplacementMap;
	LPDIRECT3DSURFACE9 mDisplacementMapSurface;
	LPDIRECT3DTEXTURE9 mNormalMap;
	LPDIRECT3DSURFACE9 mNormalMapSurface;
	LPDIRECT3DVERTEXBUFFER9 mParticlesVB;
	IDirect3DVertexDeclaration9* m_pDecl;
	LPDIRECT3DVERTEXBUFFER9 mFullScreenQuad;
	IDirect3DVertexDeclaration9* mQuadDecl;
	LPDIRECT3DSURFACE9 mBackBufferRenderTarget;

	int mNumVertsRow;
	int mNumVertsCol;
	D3DXVECTOR2 mGridStep;
	//变换坐标系用
	float mHalfWidthWorld;
	float mHalfDepthWorld;
	float mHalfWidthGrid;
	float mHalfHeightGrid;
	struct mInt2//仅仅为了传递给GPU中的"int2"格式
	{
		int _i1;
		int _i2;
	};
	mInt2 mNum2;//最多能跨越的texel即step数

	IDirect3DCubeTexture9* mEnvMap;//实验性质，到时候重新实现一下reflect&refrect
	Light mLight;
	Mtrl mMtrl;

	ID3DXEffect* mWPFX;
	//point
	D3DXHANDLE mhTime;
	D3DXHANDLE mhHalfW;
	//filter
	D3DXHANDLE mhRadiusL;
	D3DXHANDLE mhGridStep;
	D3DXHANDLE mhNum;
	D3DXHANDLE mhResolutionInv;
	D3DXHANDLE mhPointsTexture;
	//normal
	D3DXHANDLE  mhDisplacementTex;
	//water surface
	D3DXHANDLE	mhWorld;
	D3DXHANDLE	mhWorldInvTrans;
	D3DXHANDLE  mhWVP;
	D3DXHANDLE  mhLight;
	D3DXHANDLE	mhMtrl;
	D3DXHANDLE	mhEyePosW;
	//D3DXHANDLE  mhDisplacementTex;
	D3DXHANDLE mhNormalTex;
	D3DXHANDLE	mhEnvMap;

private:
	void CreateTexture();
	void CreateQuad();
	void CreateParticleVertex();
	void BuildFX();

	void GenerateOneParticle(D3DXVECTOR2 initialPostion,D3DXVECTOR2 velocity,float initialTime,float amptitude,float dispersionAngle);
	D3DXVECTOR2 Vec2RotationCW(D3DXVECTOR2 src,float angle);

public:
	WaveParticleSystem();
	~WaveParticleSystem();

	void Update(float dt);
	void Draw();


};

