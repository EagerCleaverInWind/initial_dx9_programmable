#include "RenderToTexture.h"

DrawableTex2D::DrawableTex2D(UINT width, UINT height, UINT mipLevels,
							 D3DFORMAT texFormat, bool useDepthBuffer,
							 D3DFORMAT depthFormat, D3DVIEWPORT9& viewport,  bool autoGenMips)
							 : mTex(0), mRTS(0), mTopSurf(0), mWidth(width), mHeight(height), 
							 mMipLevels(mipLevels), mTexFormat(texFormat), mUseDepthBuffer(useDepthBuffer),
							 mDepthFormat(depthFormat), mViewPort(viewport), mAutoGenMips(autoGenMips)
{
	UINT usage = D3DUSAGE_RENDERTARGET;
	if(mAutoGenMips)
		usage |= D3DUSAGE_AUTOGENMIPMAP;

	HR(D3DXCreateTexture(e_pd3dDevice, mWidth, mHeight, mMipLevels, usage, mTexFormat, D3DPOOL_DEFAULT, &mTex));
	HR(D3DXCreateRenderToSurface(e_pd3dDevice, mWidth, mHeight, mTexFormat, mUseDepthBuffer, mDepthFormat, &mRTS));
	HR(mTex->GetSurfaceLevel(0, &mTopSurf));
}

DrawableTex2D::~DrawableTex2D()
{
	SAFE_RELEASE(mTex);
	SAFE_RELEASE(mRTS);
	SAFE_RELEASE(mTopSurf);
}

IDirect3DTexture9* DrawableTex2D::GetTex()
{
	return mTex;
}


void DrawableTex2D::beginScene()
{
	mRTS->BeginScene(mTopSurf, &mViewPort);
}

void DrawableTex2D::endScene()
{
	mRTS->EndScene(D3DX_FILTER_NONE);
}