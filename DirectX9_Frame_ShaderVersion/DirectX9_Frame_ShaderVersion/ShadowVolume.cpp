#include "ShadowVolume.h"
#include "Camera.h"

#define ADJACENCY_EPSILON 0.0001f
struct CEdgeMapping
{
	int m_anOldEdge[2];  // vertex index of the original edge
	int m_aanNewEdge[2][2]; // vertex indexes of the new edge
	// First subscript = index of the new edge
	// Second subscript = index of the vertex for the edge

public:
	CEdgeMapping()
	{
		FillMemory( m_anOldEdge, sizeof( m_anOldEdge ), -1 );
		FillMemory( m_aanNewEdge, sizeof( m_aanNewEdge ), -1 );
	}
};

int FindEdgeInMappingTable( int nV1, int nV2, CEdgeMapping* pMapping, int nCount )
{
	for( int i = 0; i < nCount; ++i )
	{
		// If both vertex indexes of the old edge in mapping entry are -1, then
		// we have searched every valid entry without finding a match.  Return
		// this index as a newly created entry.
		if( ( pMapping[i].m_anOldEdge[0] == -1 && pMapping[i].m_anOldEdge[1] == -1 ) ||

			// Or if we find a match, return the index.
				( pMapping[i].m_anOldEdge[1] == nV1 && pMapping[i].m_anOldEdge[0] == nV2 ) )
		{
			return i;
		}
	}

	return -1;  // We should never reach this line
}

//哈哈，从SDK Sample中copy过来的，看懂后不想再碰的那种…
HRESULT GenerateShadowMesh( IDirect3DDevice9* pd3dDevice, ID3DXMesh* pMesh, ID3DXMesh** ppOutMesh )
{
	HRESULT hr = S_OK;
	ID3DXMesh* pInputMesh;

	if( !ppOutMesh )
		return E_INVALIDARG;
	*ppOutMesh = NULL;

	// Convert the input mesh to a format same as the output mesh using 32-bit index.
	hr = pMesh->CloneMesh( D3DXMESH_32BIT, VertexPNElements, pd3dDevice, &pInputMesh );
	if( FAILED( hr ) )
		return hr;

	// Generate adjacency information
	DWORD* pdwAdj = new DWORD[3 * pInputMesh->GetNumFaces()];
	DWORD* pdwPtRep = new DWORD[pInputMesh->GetNumVertices()];
	if( !pdwAdj || !pdwPtRep )
	{
		delete[] pdwAdj; delete[] pdwPtRep;
		pInputMesh->Release();
		return E_OUTOFMEMORY;
	}

	hr = pInputMesh->GenerateAdjacency( ADJACENCY_EPSILON, pdwAdj );
	if( FAILED( hr ) )
	{
		delete[] pdwAdj; delete[] pdwPtRep;
		pInputMesh->Release();
		return hr;
	}

	pInputMesh->ConvertAdjacencyToPointReps( pdwAdj, pdwPtRep );
	delete[] pdwAdj;

	VertexPN* pVBData = NULL;
	DWORD* pdwIBData = NULL;

	pInputMesh->LockVertexBuffer( 0, ( LPVOID* )&pVBData );
	pInputMesh->LockIndexBuffer( 0, ( LPVOID* )&pdwIBData );

	if( pVBData && pdwIBData )
	{
		// Maximum number of unique edges = Number of faces * 3
		DWORD dwNumEdges = pInputMesh->GetNumFaces() * 3;
		CEdgeMapping* pMapping = new CEdgeMapping[dwNumEdges];
		if( pMapping )
		{
			int nNumMaps = 0;  // Number of entries that exist in pMapping

			// Create a new mesh
			ID3DXMesh* pNewMesh;
			hr = D3DXCreateMesh( pInputMesh->GetNumFaces() + dwNumEdges * 2,
				pInputMesh->GetNumFaces() * 3,
				D3DXMESH_32BIT,
				VertexPNElements,
				pd3dDevice,
				&pNewMesh );
			if( SUCCEEDED( hr ) )
			{
				VertexPN* pNewVBData = NULL;
				DWORD* pdwNewIBData = NULL;

				pNewMesh->LockVertexBuffer( 0, ( LPVOID* )&pNewVBData );
				pNewMesh->LockIndexBuffer( 0, ( LPVOID* )&pdwNewIBData );

				// nNextIndex is the array index in IB that the next vertex index value
				// will be store at.
				int nNextIndex = 0;

				if( pNewVBData && pdwNewIBData )
				{
					ZeroMemory( pNewVBData, pNewMesh->GetNumVertices() * pNewMesh->GetNumBytesPerVertex() );
					ZeroMemory( pdwNewIBData, sizeof( DWORD ) * pNewMesh->GetNumFaces() * 3 );

					// pNextOutVertex is the location to write the next
					// vertex to.
					VertexPN* pNextOutVertex = pNewVBData;

					// Iterate through the faces.  For each face, output new
					// vertices and face in the new mesh, and write its edges
					// to the mapping table.

					for( UINT f = 0; f < pInputMesh->GetNumFaces(); ++f )
					{
						// Copy the vertex data for all 3 vertices
						CopyMemory( pNextOutVertex, pVBData + pdwIBData[f * 3], sizeof( VertexPN ) );
						CopyMemory( pNextOutVertex + 1, pVBData + pdwIBData[f * 3 + 1], sizeof( VertexPN ) );
						CopyMemory( pNextOutVertex + 2, pVBData + pdwIBData[f * 3 + 2], sizeof( VertexPN ) );

						// Write out the face
						pdwNewIBData[nNextIndex++] = f * 3;
						pdwNewIBData[nNextIndex++] = f * 3 + 1;
						pdwNewIBData[nNextIndex++] = f * 3 + 2;

						// Compute the face normal and assign it to
						// the normals of the vertices.
						D3DXVECTOR3 v1, v2;  // v1 and v2 are the edge vectors of the face
						D3DXVECTOR3 vNormal;
						v1 = *( D3DXVECTOR3* )( pNextOutVertex + 1 ) - *( D3DXVECTOR3* )pNextOutVertex;
						v2 = *( D3DXVECTOR3* )( pNextOutVertex + 2 ) - *( D3DXVECTOR3* )( pNextOutVertex + 1 );
						D3DXVec3Cross( &vNormal, &v1, &v2 );
						D3DXVec3Normalize( &vNormal, &vNormal );

						pNextOutVertex->normal = vNormal;
						( pNextOutVertex + 1 )->normal = vNormal;
						( pNextOutVertex + 2 )->normal = vNormal;

						pNextOutVertex += 3;

						// Add the face's edges to the edge mapping table

						// Edge 1
						int nIndex;
						int nVertIndex[3] =
						{
							pdwPtRep[pdwIBData[f * 3]],
							pdwPtRep[pdwIBData[f * 3 + 1]],
							pdwPtRep[pdwIBData[f * 3 + 2]]
						};
						nIndex = FindEdgeInMappingTable( nVertIndex[0], nVertIndex[1], pMapping, dwNumEdges );

						// If error, we are not able to proceed, so abort.
						if( -1 == nIndex )
						{
							hr = E_INVALIDARG;
							goto cleanup;
						}

						if( pMapping[nIndex].m_anOldEdge[0] == -1 && pMapping[nIndex].m_anOldEdge[1] == -1 )
						{
							// No entry for this edge yet.  Initialize one.
							pMapping[nIndex].m_anOldEdge[0] = nVertIndex[0];
							pMapping[nIndex].m_anOldEdge[1] = nVertIndex[1];
							pMapping[nIndex].m_aanNewEdge[0][0] = f * 3;
							pMapping[nIndex].m_aanNewEdge[0][1] = f * 3 + 1;

							++nNumMaps;
						}
						else
						{
							// An entry is found for this edge.  Create
							// a quad and output it.
							//assert( nNumMaps > 0 );

							pMapping[nIndex].m_aanNewEdge[1][0] = f * 3;      // For clarity
							pMapping[nIndex].m_aanNewEdge[1][1] = f * 3 + 1;

							// First triangle
							pdwNewIBData[nNextIndex++] = pMapping[nIndex].m_aanNewEdge[0][1];
							pdwNewIBData[nNextIndex++] = pMapping[nIndex].m_aanNewEdge[0][0];
							pdwNewIBData[nNextIndex++] = pMapping[nIndex].m_aanNewEdge[1][0];

							// Second triangle
							pdwNewIBData[nNextIndex++] = pMapping[nIndex].m_aanNewEdge[1][1];
							pdwNewIBData[nNextIndex++] = pMapping[nIndex].m_aanNewEdge[1][0];
							pdwNewIBData[nNextIndex++] = pMapping[nIndex].m_aanNewEdge[0][0];

							// pMapping[nIndex] is no longer needed. Copy the last map entry
							// over and decrement the map count.

							pMapping[nIndex] = pMapping[nNumMaps - 1];
							FillMemory( &pMapping[nNumMaps - 1], sizeof( pMapping[nNumMaps - 1] ), 0xFF );
							--nNumMaps;
						}

						// Edge 2
						nIndex = FindEdgeInMappingTable( nVertIndex[1], nVertIndex[2], pMapping, dwNumEdges );

						// If error, we are not able to proceed, so abort.
						if( -1 == nIndex )
						{
							hr = E_INVALIDARG;
							goto cleanup;
						}

						if( pMapping[nIndex].m_anOldEdge[0] == -1 && pMapping[nIndex].m_anOldEdge[1] == -1 )
						{
							pMapping[nIndex].m_anOldEdge[0] = nVertIndex[1];
							pMapping[nIndex].m_anOldEdge[1] = nVertIndex[2];
							pMapping[nIndex].m_aanNewEdge[0][0] = f * 3 + 1;
							pMapping[nIndex].m_aanNewEdge[0][1] = f * 3 + 2;

							++nNumMaps;
						}
						else
						{
							// An entry is found for this edge.  Create
							// a quad and output it.
							//assert( nNumMaps > 0 );

							pMapping[nIndex].m_aanNewEdge[1][0] = f * 3 + 1;
							pMapping[nIndex].m_aanNewEdge[1][1] = f * 3 + 2;

							// First triangle
							pdwNewIBData[nNextIndex++] = pMapping[nIndex].m_aanNewEdge[0][1];
							pdwNewIBData[nNextIndex++] = pMapping[nIndex].m_aanNewEdge[0][0];
							pdwNewIBData[nNextIndex++] = pMapping[nIndex].m_aanNewEdge[1][0];

							// Second triangle
							pdwNewIBData[nNextIndex++] = pMapping[nIndex].m_aanNewEdge[1][1];
							pdwNewIBData[nNextIndex++] = pMapping[nIndex].m_aanNewEdge[1][0];
							pdwNewIBData[nNextIndex++] = pMapping[nIndex].m_aanNewEdge[0][0];

							// pMapping[nIndex] is no longer needed. Copy the last map entry
							// over and decrement the map count.

							pMapping[nIndex] = pMapping[nNumMaps - 1];
							FillMemory( &pMapping[nNumMaps - 1], sizeof( pMapping[nNumMaps - 1] ), 0xFF );
							--nNumMaps;
						}

						// Edge 3
						nIndex = FindEdgeInMappingTable( nVertIndex[2], nVertIndex[0], pMapping, dwNumEdges );

						// If error, we are not able to proceed, so abort.
						if( -1 == nIndex )
						{
							hr = E_INVALIDARG;
							goto cleanup;
						}

						if( pMapping[nIndex].m_anOldEdge[0] == -1 && pMapping[nIndex].m_anOldEdge[1] == -1 )
						{
							pMapping[nIndex].m_anOldEdge[0] = nVertIndex[2];
							pMapping[nIndex].m_anOldEdge[1] = nVertIndex[0];
							pMapping[nIndex].m_aanNewEdge[0][0] = f * 3 + 2;
							pMapping[nIndex].m_aanNewEdge[0][1] = f * 3;

							++nNumMaps;
						}
						else
						{
							// An entry is found for this edge.  Create
							// a quad and output it.
							//assert( nNumMaps > 0 );

							pMapping[nIndex].m_aanNewEdge[1][0] = f * 3 + 2;
							pMapping[nIndex].m_aanNewEdge[1][1] = f * 3;

							// First triangle
							pdwNewIBData[nNextIndex++] = pMapping[nIndex].m_aanNewEdge[0][1];
							pdwNewIBData[nNextIndex++] = pMapping[nIndex].m_aanNewEdge[0][0];
							pdwNewIBData[nNextIndex++] = pMapping[nIndex].m_aanNewEdge[1][0];

							// Second triangle
							pdwNewIBData[nNextIndex++] = pMapping[nIndex].m_aanNewEdge[1][1];
							pdwNewIBData[nNextIndex++] = pMapping[nIndex].m_aanNewEdge[1][0];
							pdwNewIBData[nNextIndex++] = pMapping[nIndex].m_aanNewEdge[0][0];

							// pMapping[nIndex] is no longer needed. Copy the last map entry
							// over and decrement the map count.

							pMapping[nIndex] = pMapping[nNumMaps - 1];
							FillMemory( &pMapping[nNumMaps - 1], sizeof( pMapping[nNumMaps - 1] ), 0xFF );
							--nNumMaps;
						}
					}

					// Now the entries in the edge mapping table represent
					// non-shared edges.  What they mean is that the original
					// mesh has openings (holes), so we attempt to patch them.
					// First we need to recreate our mesh with a larger vertex
					// and index buffers so the patching geometry could fit.


					// Create a mesh with large enough vertex and
					// index buffers.

					VertexPN* pPatchVBData = NULL;
					DWORD* pdwPatchIBData = NULL;

					ID3DXMesh* pPatchMesh = NULL;
					// Make enough room in IB for the face and up to 3 quads for each patching face
					hr = D3DXCreateMesh( nNextIndex / 3 + nNumMaps * 7,
						( pInputMesh->GetNumFaces() + nNumMaps ) * 3,
						D3DXMESH_32BIT,
						VertexPNElements,
						pd3dDevice,
						&pPatchMesh );

					if( FAILED( hr ) )
						goto cleanup;

					hr = pPatchMesh->LockVertexBuffer( 0, ( LPVOID* )&pPatchVBData );
					if( SUCCEEDED( hr ) )
						hr = pPatchMesh->LockIndexBuffer( 0, ( LPVOID* )&pdwPatchIBData );

					if( pPatchVBData && pdwPatchIBData )
					{
						ZeroMemory( pPatchVBData, sizeof( VertexPN ) * ( pInputMesh->GetNumFaces() + nNumMaps ) *
							3 );
						ZeroMemory( pdwPatchIBData, sizeof( DWORD ) * ( nNextIndex + 3 * nNumMaps * 7 ) );

						// Copy the data from one mesh to the other

						CopyMemory( pPatchVBData, pNewVBData, sizeof( VertexPN ) * pInputMesh->GetNumFaces() * 3 );
						CopyMemory( pdwPatchIBData, pdwNewIBData, sizeof( DWORD ) * nNextIndex );
					}
					else
					{
						// Some serious error is preventing us from locking.
						// Abort and return error.

						pPatchMesh->Release();
						goto cleanup;
					}

					// Replace pNewMesh with the updated one.  Then the code
					// can continue working with the pNewMesh pointer.

					pNewMesh->UnlockVertexBuffer();
					pNewMesh->UnlockIndexBuffer();
					pNewVBData = pPatchVBData;
					pdwNewIBData = pdwPatchIBData;
					pNewMesh->Release();
					pNewMesh = pPatchMesh;

					// Now, we iterate through the edge mapping table and
					// for each shared edge, we generate a quad.
					// For each non-shared edge, we patch the opening
					// with new faces.

					// nNextVertex is the index of the next vertex.
					int nNextVertex = pInputMesh->GetNumFaces() * 3;

					for( int i = 0; i < nNumMaps; ++i )
					{
						if( pMapping[i].m_anOldEdge[0] != -1 &&
							pMapping[i].m_anOldEdge[1] != -1 )
						{
							// If the 2nd new edge indexes is -1,
							// this edge is a non-shared one.
							// We patch the opening by creating new
							// faces.
							if( pMapping[i].m_aanNewEdge[1][0] == -1 ||  // must have only one new edge
								pMapping[i].m_aanNewEdge[1][1] == -1 )
							{
								// Find another non-shared edge that
								// shares a vertex with the current edge.
								for( int i2 = i + 1; i2 < nNumMaps; ++i2 )
								{
									if( pMapping[i2].m_anOldEdge[0] != -1 &&       // must have a valid old edge
										pMapping[i2].m_anOldEdge[1] != -1 &&
										( pMapping[i2].m_aanNewEdge[1][0] == -1 || // must have only one new edge
										pMapping[i2].m_aanNewEdge[1][1] == -1 ) )
									{
										int nVertShared = 0;
										if( pMapping[i2].m_anOldEdge[0] == pMapping[i].m_anOldEdge[1] )
											++nVertShared;
										if( pMapping[i2].m_anOldEdge[1] == pMapping[i].m_anOldEdge[0] )
											++nVertShared;

										if( 2 == nVertShared )
										{
											// These are the last two edges of this particular
											// opening. Mark this edge as shared so that a degenerate
											// quad can be created for it.

											pMapping[i2].m_aanNewEdge[1][0] = pMapping[i].m_aanNewEdge[0][0];
											pMapping[i2].m_aanNewEdge[1][1] = pMapping[i].m_aanNewEdge[0][1];
											break;
										}
										else if( 1 == nVertShared )
										{
											// nBefore and nAfter tell us which edge comes before the other.
											int nBefore, nAfter;
											if( pMapping[i2].m_anOldEdge[0] == pMapping[i].m_anOldEdge[1] )
											{
												nBefore = i;
												nAfter = i2;
											}
											else
											{
												nBefore = i2;
												nAfter = i;
											}

											// Found such an edge. Now create a face along with two
											// degenerate quads from these two edges.

											pNewVBData[nNextVertex] = pNewVBData[pMapping[nAfter].m_aanNewEdge[0][1]];
											pNewVBData[nNextVertex +
												1] = pNewVBData[pMapping[nBefore].m_aanNewEdge[0][1]];
											pNewVBData[nNextVertex +
												2] = pNewVBData[pMapping[nBefore].m_aanNewEdge[0][0]];
											// Recompute the normal
											D3DXVECTOR3 v1 = pNewVBData[nNextVertex + 1].pos -
												pNewVBData[nNextVertex].pos;
											D3DXVECTOR3 v2 = pNewVBData[nNextVertex + 2].pos -
												pNewVBData[nNextVertex + 1].pos;
											D3DXVec3Normalize( &v1, &v1 );
											D3DXVec3Normalize( &v2, &v2 );
											D3DXVec3Cross( &pNewVBData[nNextVertex].normal, &v1, &v2 );
											pNewVBData[nNextVertex + 1].normal = pNewVBData[nNextVertex +
												2].normal = pNewVBData[nNextVertex].normal;

											pdwNewIBData[nNextIndex] = nNextVertex;
											pdwNewIBData[nNextIndex + 1] = nNextVertex + 1;
											pdwNewIBData[nNextIndex + 2] = nNextVertex + 2;

											// 1st quad

											pdwNewIBData[nNextIndex + 3] = pMapping[nBefore].m_aanNewEdge[0][1];
											pdwNewIBData[nNextIndex + 4] = pMapping[nBefore].m_aanNewEdge[0][0];
											pdwNewIBData[nNextIndex + 5] = nNextVertex + 1;

											pdwNewIBData[nNextIndex + 6] = nNextVertex + 2;
											pdwNewIBData[nNextIndex + 7] = nNextVertex + 1;
											pdwNewIBData[nNextIndex + 8] = pMapping[nBefore].m_aanNewEdge[0][0];

											// 2nd quad

											pdwNewIBData[nNextIndex + 9] = pMapping[nAfter].m_aanNewEdge[0][1];
											pdwNewIBData[nNextIndex + 10] = pMapping[nAfter].m_aanNewEdge[0][0];
											pdwNewIBData[nNextIndex + 11] = nNextVertex;

											pdwNewIBData[nNextIndex + 12] = nNextVertex + 1;
											pdwNewIBData[nNextIndex + 13] = nNextVertex;
											pdwNewIBData[nNextIndex + 14] = pMapping[nAfter].m_aanNewEdge[0][0];

											// Modify mapping entry i2 to reflect the third edge
											// of the newly added face.

											if( pMapping[i2].m_anOldEdge[0] == pMapping[i].m_anOldEdge[1] )
											{
												pMapping[i2].m_anOldEdge[0] = pMapping[i].m_anOldEdge[0];
											}
											else
											{
												pMapping[i2].m_anOldEdge[1] = pMapping[i].m_anOldEdge[1];
											}
											pMapping[i2].m_aanNewEdge[0][0] = nNextVertex + 2;
											pMapping[i2].m_aanNewEdge[0][1] = nNextVertex;

											// Update next vertex/index positions

											nNextVertex += 3;
											nNextIndex += 15;

											break;
										}
									}
								}
							}
							else
							{
								// This is a shared edge.  Create the degenerate quad.

								// First triangle
								pdwNewIBData[nNextIndex++] = pMapping[i].m_aanNewEdge[0][1];
								pdwNewIBData[nNextIndex++] = pMapping[i].m_aanNewEdge[0][0];
								pdwNewIBData[nNextIndex++] = pMapping[i].m_aanNewEdge[1][0];

								// Second triangle
								pdwNewIBData[nNextIndex++] = pMapping[i].m_aanNewEdge[1][1];
								pdwNewIBData[nNextIndex++] = pMapping[i].m_aanNewEdge[1][0];
								pdwNewIBData[nNextIndex++] = pMapping[i].m_aanNewEdge[0][0];
							}
						}
					}
				}

cleanup:;
				if( pNewVBData )
				{
					pNewMesh->UnlockVertexBuffer();
					pNewVBData = NULL;
				}
				if( pdwNewIBData )
				{
					pNewMesh->UnlockIndexBuffer();
					pdwNewIBData = NULL;
				}

				if( SUCCEEDED( hr ) )
				{
					// At this time, the output mesh may have an index buffer
					// bigger than what is actually needed, so we create yet
					// another mesh with the exact IB size that we need and
					// output it.  This mesh also uses 16-bit index if
					// 32-bit is not necessary.


					bool bNeed32Bit = ( pInputMesh->GetNumFaces() + nNumMaps ) * 3 > 65535;
					ID3DXMesh* pFinalMesh;
					hr = D3DXCreateMesh( nNextIndex / 3,  // Exact number of faces
						( pInputMesh->GetNumFaces() + nNumMaps ) * 3,
						D3DXMESH_WRITEONLY | ( bNeed32Bit ? D3DXMESH_32BIT : 0 ),
						VertexPNElements,
						pd3dDevice,
						&pFinalMesh );
					if( SUCCEEDED( hr ) )
					{
						pNewMesh->LockVertexBuffer( 0, ( LPVOID* )&pNewVBData );
						pNewMesh->LockIndexBuffer( 0, ( LPVOID* )&pdwNewIBData );

						VertexPN* pFinalVBData = NULL;
						WORD* pwFinalIBData = NULL;

						pFinalMesh->LockVertexBuffer( 0, ( LPVOID* )&pFinalVBData );
						pFinalMesh->LockIndexBuffer( 0, ( LPVOID* )&pwFinalIBData );

						if( pNewVBData && pdwNewIBData && pFinalVBData && pwFinalIBData )
						{
							CopyMemory( pFinalVBData, pNewVBData, sizeof( VertexPN ) *
								( pInputMesh->GetNumFaces() + nNumMaps ) * 3 );

							if( bNeed32Bit )
								CopyMemory( pwFinalIBData, pdwNewIBData, sizeof( DWORD ) * nNextIndex );
							else
							{
								for( int i = 0; i < nNextIndex; ++i )
									pwFinalIBData[i] = ( WORD )pdwNewIBData[i];
							}
						}

						if( pNewVBData )
							pNewMesh->UnlockVertexBuffer();
						if( pdwNewIBData )
							pNewMesh->UnlockIndexBuffer();
						if( pFinalVBData )
							pFinalMesh->UnlockVertexBuffer();
						if( pwFinalIBData )
							pFinalMesh->UnlockIndexBuffer();

						// Release the old
						pNewMesh->Release();
						pNewMesh = pFinalMesh;
					}

					*ppOutMesh = pNewMesh;
				}
				else
					pNewMesh->Release();
			}
			delete[] pMapping;
		}
		else
			hr = E_OUTOFMEMORY;
	}
	else
		hr = E_FAIL;

	if( pVBData )
		pInputMesh->UnlockVertexBuffer();

	if( pdwIBData )
		pInputMesh->UnlockIndexBuffer();

	delete[] pdwPtRep;
	pInputMesh->Release();

	return hr;
}


ShadowVolume::ShadowVolume():mSVCol(D3DCOLOR_XRGB(0,100,0))
{
	BuildFX();
	//LoadObjects();//在这（抽象类构造函数中）调用会报错
}

ShadowVolume::~ShadowVolume()
{
	SAFE_RELEASE(mFX);
	//for(std::vector<Object*>::iterator iter=mpShadowCasters.begin();iter!=mpShadowCasters.end();iter++)
	//{
	//	SAFE_DELETE(*iter);
	//}
	//SB！不行，又不是new出来的（不是堆上分配的不需要自己管理）
}

void ShadowVolume::BuildFX()
{
	// Create the FX from a .fx file.
	ID3DXBuffer* errors = 0;
	HR(D3DXCreateEffectFromFile(e_pd3dDevice, "shader/ShadowVolume.fx", 
		0, 0, D3DXSHADER_DEBUG, 0, &mFX, &errors));
	if( errors )
		MessageBox(0, (char*)errors->GetBufferPointer(), 0, 0);


	// Obtain handles.
	mhWVP                = mFX->GetParameterByName(0, "gWVP");
	mhWV               = mFX->GetParameterByName(0, "gWV");
	mhWorld              = mFX->GetParameterByName(0, "gWorld");
	mhWorldInvTrans	=mFX->GetParameterByName(0,"gWorldInvTrans");
	mhView             = mFX->GetParameterByName(0, "gView");
	mhProj             = mFX->GetParameterByName(0, "gProj");
	mhMtrl               = mFX->GetParameterByName(0, "gMtrl");
	mhLight              = mFX->GetParameterByName(0, "gLight");
	mhEyePosW            = mFX->GetParameterByName(0, "gEyePosW");
	mhTex                = mFX->GetParameterByName(0, "gTex");
	mhFarClip             = mFX->GetParameterByName(0, "gFarClip");
	mhSVColor				=mFX->GetParameterByName(0,"gShadowVolumeColor");

	mFX->SetValue(mhSVColor,&mSVCol,sizeof(mSVCol));
	float farClip=20000.0f;
	mFX->SetValue(mhFarClip,&farClip,sizeof(farClip));

}

void ShadowVolume::Update(float dt)
{

}

void ShadowVolume::Draw()
{
	D3DXMATRIX matView;
	D3DXMATRIX matProj;
	e_pCamera->GetViewMatrix(matView);
	e_pCamera->GetProjMatrix(matProj);
	mFX->SetMatrix(mhView,&matView);
	mFX->SetMatrix(mhProj,&matProj);
	mFX->SetValue(mhEyePosW,&(e_pCamera->GetPosition()),sizeof(D3DXVECTOR3) );

	D3DXMATRIX matWorld;
	UINT numPasses;

	//render scene with ambient only
	mFX->SetTechnique("RenderSceneAmbient");
	mFX->Begin(&numPasses,0);
	for(UINT passIndex=0;passIndex<numPasses;passIndex++)
	{
		mFX->BeginPass(passIndex);

		for(UINT i=0;i<mObjects.size();i++)
		{
			mFX->SetMatrix(mhWVP,&(mObjects[i]._matWorld*matView*matProj));
			for(UINT j=0;j<mObjects[i]._mtrls.size();j++)
			{
				mFX->SetValue(mhMtrl,&mObjects[i]._mtrls[j],sizeof(Mtrl));
				if(mObjects[i]._textures[j]!=0)
					mFX->SetTexture(mhTex,mObjects[i]._textures[j]);
				else
					mFX->SetTexture(mhTex,e_pWhiteTexture);
				mFX->CommitChanges();

				mObjects[i]._mesh->DrawSubset(j);
			}
		}

		mFX->EndPass();
	}
	mFX->End();


	//then render the shadow volume
	e_pd3dDevice->Clear( 0, NULL, D3DCLEAR_STENCIL, D3DCOLOR_ARGB( 0, 170, 170, 170 ), 1.0f, 0 );
	//哼哼！调试时出现闪烁的黑然后很快黑成一大片，老夫就想到是忘记clear stencil buffer了！
	mFX->SetTechnique("RenderShadowVolume");
	//mFX->SetTechnique("ShowShadowVolume");
	mFX->Begin(&numPasses,0);
	for(UINT passIndex=0;passIndex<numPasses;passIndex++)
	{
		mFX->BeginPass(passIndex);

		for(UINT i=0;i<mpShadowCasters.size();i++)
		{
			matWorld=mpShadowCasters[i]->_matWorld;
			mFX->SetMatrix(mhWV,&(matWorld*matView));
			mFX->SetMatrix(mhWVP,&(matWorld*matView*matProj));
			mFX->CommitChanges();
			mpShadowCasters[i]->_shadowVolume->DrawSubset(0);
		}

		mFX->EndPass();
	}
	mFX->End();

	
	//finally,render the scene with stencil and lighting enabled
	mFX->SetTechnique("RenderScene");
	mFX->Begin(&numPasses,0);
	for(UINT passIndex=0;passIndex<numPasses;passIndex++)
	{
		mFX->BeginPass(passIndex);

		for(UINT i=0;i<mObjects.size();i++)
		{
			matWorld=mObjects[i]._matWorld;
			D3DXMATRIX matWorldInvTrans;
			D3DXMatrixInverse(&matWorldInvTrans,0,&matWorld);
			D3DXMatrixTranspose(&matWorldInvTrans,&matWorldInvTrans);
			mFX->SetMatrix(mhWorld,&matWorld);
			mFX->SetMatrix(mhWorldInvTrans,&matWorldInvTrans);
			mFX->SetMatrix(mhWVP,&(matWorld*matView*matProj));
			for(UINT j=0;j<mObjects[i]._mtrls.size();j++)
			{
				mFX->SetValue(mhMtrl,&mObjects[i]._mtrls[j],sizeof(Mtrl));
				if(mObjects[i]._textures[j]!=0)
					mFX->SetTexture(mhTex,mObjects[i]._textures[j]);
				else
					mFX->SetTexture(mhTex,e_pWhiteTexture);
				mFX->CommitChanges();

				mObjects[i]._mesh->DrawSubset(j);
			}
		}

		mFX->EndPass();
	}
	mFX->End();
	
}


void SVDemo::LoadObjects()
{
	mObjects.resize(2);


	LoadXFile("Mesh/BasicColumnScene/BasicColumnScene.x", &mObjects[0]._mesh, mObjects[0]._mtrls, mObjects[0]._textures);
	D3DXMatrixIdentity(&mObjects[0]._matWorld);
	GenerateShadowMesh(e_pd3dDevice,mObjects[0]._mesh,&mObjects[0]._shadowVolume);
	mObjects[0]._ifCastShadow=true;
	mpShadowCasters.push_back(&mObjects[0]);

	LoadXFile("Mesh/car.x", &mObjects[1]._mesh, mObjects[1]._mtrls, mObjects[1]._textures);
	D3DXMATRIX S, T;
	D3DXMatrixScaling(&S, 1.5f, 1.5f, 1.5f);
	D3DXMatrixTranslation(&T, 6.0f, 3.5f, -3.0f);
	mObjects[1]._matWorld= S*T;
	GenerateShadowMesh(e_pd3dDevice,mObjects[1]._mesh,&mObjects[1]._shadowVolume);
	mObjects[1]._ifCastShadow=true;
	mpShadowCasters.push_back(&mObjects[1]);

}

SVDemo::SVDemo()
{
	LoadObjects();

	mLight.ambient   = D3DXCOLOR(0.1f, 0.1f, 0.1f, 1.0f);
	mLight.diffuse   = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mLight.spec      = D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
}

void SVDemo::Update(float dt)
{
	// Animate point light by rotating it on y-axis with respect to time.
	D3DXVECTOR3 lightPosW(125.0f, 50.0f, 0.0f);

	static float t = 0.0f;
	t += dt;
	if( t >= 2.0f*D3DX_PI )
		t = 0.0f;
	D3DXMATRIX Ry;
	D3DXMatrixRotationY(&Ry, t);
	D3DXVec3TransformCoord(&lightPosW, &lightPosW, &Ry);

	mLight.light      = D3DXVECTOR4(lightPosW,1.0f);
	mFX->SetValue(mhLight,&mLight,sizeof(Light));
}


