#include "Water.h"
#include "Camera.h"

void InitWater(Water::InitInfo& out,bool ifbrook)
{
	DirLight envLight;
	envLight.dirW = D3DXVECTOR3(1.0f, -2.0f, -1.0f);
	D3DXVec3Normalize(&envLight.dirW, &envLight.dirW);
	envLight.ambient = D3DXCOLOR(0.3f, 0.3f, 0.3f, 1.0f);
	envLight.diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	envLight.spec    = D3DXCOLOR(0.7f, 0.7f, 0.7f, 1.0f);

	if(ifbrook)
	{
		Mtrl waterMtrl;
		waterMtrl.ambient   = D3DXCOLOR(0.26f, 0.23f, 0.3f, 0.50f);
		waterMtrl.diffuse   = D3DXCOLOR(0.26f, 0.23f, 0.3f, 0.50f);
		waterMtrl.spec      = 1.0f*WHITE;
		waterMtrl.specPower = 64.0f;

		out.dirLight = envLight;
		out.mtrl     = waterMtrl;
		out.vertRows         = 30;
		out.vertCols         = 42;
		out.dx               = 10.0f;
		out.dz               = 10.0f;
		out.waveMapFilename0 = "Texture/Water/wave0.dds";
		out.waveMapFilename1 = "Texture/Water/wave1.dds";
		out.waveNMapVelocity0 = D3DXVECTOR2(0.05f, 0.08f);
		out.waveNMapVelocity1 = D3DXVECTOR2(-0.02f, 0.1f);
		out.texScale = 1.0f;
		D3DXMatrixTranslation(&out.toWorld,0,200,80);
	}
	else
	{

		Mtrl waterMtrl;
		waterMtrl.ambient   = D3DXCOLOR(0.4f, 0.4f, 0.7f, 1.0f);
		waterMtrl.diffuse   = D3DXCOLOR(0.4f, 0.4f, 0.7f, 1.0f);
		waterMtrl.spec      = 0.8f*WHITE;
		waterMtrl.specPower = 128.0f;

		out.dirLight = envLight;
		out.mtrl     = waterMtrl;
		out.vertRows         = 128;
		out.vertCols         = 128;
		out.dx               = 5.0f;
		out.dz               = 3.0f;
		out.waveMapFilename0 = "Texture/Water/wave2.dds";
		out.waveMapFilename1 = "Texture/Water/wave3.dds";
		out.dmapFilename0    = "Texture/Water/waterdmap0.dds";
		out.dmapFilename1    = "Texture/Water/waterdmap1.dds";
		out.waveNMapVelocity0 = D3DXVECTOR2(0.05f, 0.07f);
		out.waveNMapVelocity1 = D3DXVECTOR2(-0.01f, 0.13f);
		out.waveDMapVelocity0 = D3DXVECTOR2(0.012f, 0.015f);
		out.waveDMapVelocity1 = D3DXVECTOR2(0.014f, 0.05f);
		out.scaleHeights      = D3DXVECTOR2(9.0f, 41.0f);
		out.texScale = 8.0f;
		D3DXMatrixTranslation(&out.toWorld,0,180,-900);
	}
}

//-----------------------------------��base��---------------------------------------
Water::Water(InitInfo& initInfo)
{
	mInitInfo=initInfo;

	mWidth = (mInitInfo.vertCols-1)*mInitInfo.dx;
	mDepth = (mInitInfo.vertRows-1)*mInitInfo.dz;

	mWaveNMapOffset0 = D3DXVECTOR2(0.0f, 0.0f);
	mWaveNMapOffset1 = D3DXVECTOR2(0.0f, 0.0f);

	mWaveDMapOffset0 = D3DXVECTOR2(0.0f, 0.0f);
	mWaveDMapOffset1 = D3DXVECTOR2(0.0f, 0.0f);

	GenGridMesh(mMesh,mInitInfo.vertRows,mInitInfo.vertCols,mInitInfo.dx,mInitInfo.dz,false);
}

Water::~Water()
{
	SAFE_RELEASE(mMesh);
	SAFE_RELEASE(mFX);
}

void Water::update(float dt)
{
	// Update texture coordinate offsets.  These offsets are added to the
	// texture coordinates in the vertex shader to animate them.
	mWaveNMapOffset0 += mInitInfo.waveNMapVelocity0 * dt;
	mWaveNMapOffset1 += mInitInfo.waveNMapVelocity1 * dt;

	mWaveDMapOffset0 += mInitInfo.waveDMapVelocity0 * dt;
	mWaveDMapOffset1 += mInitInfo.waveDMapVelocity1 * dt;

	// Textures repeat every 1.0 unit, so reset back down to zero
	// so the coordinates do not grow too large.
	if(mWaveNMapOffset0.x >= 1.0f || mWaveNMapOffset0.x <= -1.0f)
		mWaveNMapOffset0.x = 0.0f;
	if(mWaveNMapOffset1.x >= 1.0f || mWaveNMapOffset1.x <= -1.0f)
		mWaveNMapOffset1.x = 0.0f;
	if(mWaveNMapOffset0.y >= 1.0f || mWaveNMapOffset0.y <= -1.0f)
		mWaveNMapOffset0.y = 0.0f;
	if(mWaveNMapOffset1.y >= 1.0f || mWaveNMapOffset1.y <= -1.0f)
		mWaveNMapOffset1.y = 0.0f;

	if(mWaveDMapOffset0.x >= 1.0f || mWaveDMapOffset0.x <= -1.0f)
		mWaveDMapOffset0.x = 0.0f;
	if(mWaveDMapOffset1.x >= 1.0f || mWaveDMapOffset1.x <= -1.0f)
		mWaveDMapOffset1.x = 0.0f;
	if(mWaveDMapOffset0.y >= 1.0f || mWaveDMapOffset0.y <= -1.0f)
		mWaveDMapOffset0.y = 0.0f;
	if(mWaveDMapOffset1.y >= 1.0f || mWaveDMapOffset1.y <= -1.0f)
		mWaveDMapOffset1.y = 0.0f;
}

//------------------------------------------------------------------------------------------------    




//-----------------------------------��stream��---------------------------------------
Stream::Stream(InitInfo& initInfo):Water(initInfo)
{
	HR(D3DXCreateTextureFromFile(e_pd3dDevice, initInfo.waveMapFilename0.c_str(), &mWaveMap0));
	HR(D3DXCreateTextureFromFile(e_pd3dDevice, initInfo.waveMapFilename1.c_str(), &mWaveMap1));
	buildFX();
}

Stream::~Stream()
{
	SAFE_RELEASE(mWaveMap0);
	SAFE_RELEASE(mWaveMap1);
}

void Stream::buildFX()
{
	ID3DXBuffer* errors = 0;
	HR(D3DXCreateEffectFromFile(e_pd3dDevice, "shader/stream.fx",
		0, 0, D3DXSHADER_DEBUG, 0, &mFX, &errors));
	if( errors )
		MessageBox(0, (char*)errors->GetBufferPointer(), 0, 0);

	mhTech           = mFX->GetTechniqueByName("WaterTech");
	mhWorld          = mFX->GetParameterByName(0, "gWorld");
	mhWorldInv       = mFX->GetParameterByName(0, "gWorldInv");
	mhWVP            = mFX->GetParameterByName(0, "gWVP");
	mhEyePosW        = mFX->GetParameterByName(0, "gEyePosW");
	mhLight          = mFX->GetParameterByName(0, "gLight");
	mhMtrl           = mFX->GetParameterByName(0, "gMtrl");
	mhWaveMap0       = mFX->GetParameterByName(0, "gWaveMap0");
	mhWaveMap1       = mFX->GetParameterByName(0, "gWaveMap1");
	mhWaveMapOffset0 = mFX->GetParameterByName(0, "gWaveMapOffset0");
	mhWaveMapOffset1 = mFX->GetParameterByName(0, "gWaveMapOffset1");
	mhEnvMap         = mFX->GetParameterByName(0, "gEnvMap");
	mhTexScale=mFX->GetParameterByName(0,"g_TexScale");


	// We don't need to set these every frame since they do not change.
	HR(mFX->SetMatrix(mhWorld, &mInitInfo.toWorld));
	D3DXMATRIX worldInv;
	D3DXMatrixInverse(&worldInv, 0, &mInitInfo.toWorld);
	HR(mFX->SetMatrix(mhWorldInv, &worldInv));
	HR(mFX->SetTechnique(mhTech));
	HR(mFX->SetTexture(mhWaveMap0, mWaveMap0));
	HR(mFX->SetTexture(mhWaveMap1, mWaveMap1));
	HR(mFX->SetValue(mhLight, &mInitInfo.dirLight, sizeof(DirLight)));
	HR(mFX->SetValue(mhMtrl, &mInitInfo.mtrl, sizeof(Mtrl)));
	mFX->SetFloat(mhTexScale,mInitInfo.texScale);
}

void Stream::draw()
{
	D3DXMATRIX matViewProj;
	e_pCamera->GetViewProj(matViewProj);
	HR(mFX->SetMatrix(mhWVP, &(mInitInfo.toWorld*matViewProj)));
	HR(mFX->SetValue(mhEyePosW, &e_pCamera->GetPosition(), sizeof(D3DXVECTOR3)));
	HR(mFX->SetValue(mhWaveMapOffset0, &mWaveNMapOffset0, sizeof(D3DXVECTOR2)));
	HR(mFX->SetValue(mhWaveMapOffset1, &mWaveNMapOffset1, sizeof(D3DXVECTOR2)));

	UINT numPasses = 0;
	HR(mFX->Begin(&numPasses, 0));
	HR(mFX->BeginPass(0));

	HR(mMesh->DrawSubset(0));

	HR(mFX->EndPass());
	HR(mFX->End());
}

void Stream::setEnvMap(IDirect3DCubeTexture9* envMap)
{
	HR(mFX->SetTexture(mhEnvMap, envMap));
}
//------------------------------------------------------------------------------------------------    

//-----------------------------------��sea��---------------------------------------
Sea::Sea(InitInfo& initInfo):Water(initInfo)
{
	int m = mInitInfo.vertRows;
	int n = mInitInfo.vertCols;
	HR(D3DXCreateTextureFromFile(e_pd3dDevice, initInfo.waveMapFilename0.c_str(), &mWaveMap0));
	HR(D3DXCreateTextureFromFile(e_pd3dDevice, initInfo.waveMapFilename1.c_str(), &mWaveMap1));
	HR(D3DXCreateTextureFromFileEx(e_pd3dDevice, initInfo.dmapFilename0.c_str(), m, n,
		1, 0, D3DFMT_R32F, D3DPOOL_MANAGED, D3DX_DEFAULT, D3DX_DEFAULT, 0, 0, 0, &mDispMap0));
	HR(D3DXCreateTextureFromFileEx(e_pd3dDevice, initInfo.dmapFilename1.c_str(), m, n,
		1, 0, D3DFMT_R32F, D3DPOOL_MANAGED, D3DX_DEFAULT, D3DX_DEFAULT, 0, 0, 0, &mDispMap1));

	buildFX();
}

Sea::~Sea()
{
	SAFE_RELEASE(mWaveMap0);
	SAFE_RELEASE(mWaveMap1);
	SAFE_RELEASE(mDispMap0);
	SAFE_RELEASE(mDispMap1);
}

void Sea::buildFX()
{
	ID3DXBuffer* errors = 0;
	HR(D3DXCreateEffectFromFile(e_pd3dDevice, "shader/wave.fx",
		0, 0, D3DXSHADER_DEBUG, 0, &mFX, &errors));
	if( errors )
		MessageBox(0, (char*)errors->GetBufferPointer(), 0, 0);

	mhTech            = mFX->GetTechniqueByName("WaterTech");
	mhWorld           = mFX->GetParameterByName(0, "gWorld");
	mhWorldInv        = mFX->GetParameterByName(0, "gWorldInv");
	mhWVP             = mFX->GetParameterByName(0, "gWVP");
	mhEyePosW         = mFX->GetParameterByName(0, "gEyePosW");
	mhLight           = mFX->GetParameterByName(0, "gLight");
	mhMtrl            = mFX->GetParameterByName(0, "gMtrl");
	mhWaveMap0        = mFX->GetParameterByName(0, "gWaveMap0");
	mhWaveMap1        = mFX->GetParameterByName(0, "gWaveMap1");
	mhWaveNMapOffset0 = mFX->GetParameterByName(0, "gWaveNMapOffset0");
	mhWaveNMapOffset1 = mFX->GetParameterByName(0, "gWaveNMapOffset1");
	mhWaveDMapOffset0 = mFX->GetParameterByName(0, "gWaveDMapOffset0");
	mhWaveDMapOffset1 = mFX->GetParameterByName(0, "gWaveDMapOffset1");
	mhWaveDispMap0    = mFX->GetParameterByName(0, "gWaveDispMap0");
	mhWaveDispMap1    = mFX->GetParameterByName(0, "gWaveDispMap1");
	mhScaleHeights    = mFX->GetParameterByName(0, "gScaleHeights");
	mhGridStepSizeL   = mFX->GetParameterByName(0, "gGridStepSizeL");
	mhTexScale=mFX->GetParameterByName(0,"g_TexScale");


	// We don't need to set these every frame since they do not change.
	HR(mFX->SetMatrix(mhWorld, &mInitInfo.toWorld));
	D3DXMATRIX worldInv;
	D3DXMatrixInverse(&worldInv, 0, &mInitInfo.toWorld);
	HR(mFX->SetMatrix(mhWorldInv, &worldInv));
	HR(mFX->SetTechnique(mhTech));
	HR(mFX->SetTexture(mhWaveMap0, mWaveMap0));
	HR(mFX->SetTexture(mhWaveMap1, mWaveMap1));
	HR(mFX->SetTexture(mhWaveDispMap0, mDispMap0));
	HR(mFX->SetTexture(mhWaveDispMap1, mDispMap1));
	HR(mFX->SetValue(mhLight, &mInitInfo.dirLight, sizeof(DirLight)));
	HR(mFX->SetValue(mhMtrl, &mInitInfo.mtrl, sizeof(Mtrl)));
	HR(mFX->SetValue(mhScaleHeights, &mInitInfo.scaleHeights, sizeof(D3DXVECTOR2)));
	mFX->SetFloat(mhTexScale,mInitInfo.texScale);

	D3DXVECTOR2 stepSizes(mInitInfo.dx, mInitInfo.dz);
	HR(mFX->SetValue(mhGridStepSizeL, &stepSizes, sizeof(D3DXVECTOR2)));
}

void Sea::draw()
{
	D3DXMATRIX matViewProj;
	e_pCamera->GetViewProj(matViewProj);
	HR(mFX->SetMatrix(mhWVP, &(mInitInfo.toWorld*matViewProj)));
	HR(mFX->SetValue(mhEyePosW, &e_pCamera->GetPosition(), sizeof(D3DXVECTOR3)));
	HR(mFX->SetValue(mhWaveNMapOffset0, &mWaveNMapOffset0, sizeof(D3DXVECTOR2)));
	HR(mFX->SetValue(mhWaveNMapOffset1, &mWaveNMapOffset1, sizeof(D3DXVECTOR2)));
	HR(mFX->SetValue(mhWaveDMapOffset0, &mWaveDMapOffset0, sizeof(D3DXVECTOR2)));
	HR(mFX->SetValue(mhWaveDMapOffset1, &mWaveDMapOffset1, sizeof(D3DXVECTOR2)));

	UINT numPasses = 0;
	HR(mFX->Begin(&numPasses, 0));
	HR(mFX->BeginPass(0));

	HR(mMesh->DrawSubset(0));

	HR(mFX->EndPass());
	HR(mFX->End());
}
//------------------------------------------------------------------------------------------------    