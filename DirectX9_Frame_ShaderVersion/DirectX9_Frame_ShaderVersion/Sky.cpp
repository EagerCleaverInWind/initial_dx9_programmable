#include "Sky.h"
#include "Camera.h"

Sky::Sky(const std::string& envmapFilename):mRadius(1.0f)
{
	D3DXCreateSphere(e_pd3dDevice,mRadius,30,30,&mSphere,0);
	D3DXCreateCubeTextureFromFile(e_pd3dDevice,envmapFilename.c_str(),&mEnvMap);

	ID3DXBuffer* errors = 0;
	HR(D3DXCreateEffectFromFile(e_pd3dDevice, "shader/sky.fx", 0, 0, 0, 
		0, &mFX, &errors));
	if( errors )
		MessageBox(0, (char*)errors->GetBufferPointer(), 0, 0);

	mhTech   = mFX->GetTechniqueByName("SkyTech");
	mhWVP    = mFX->GetParameterByName(0, "gWVP");
	mhEnvMap = mFX->GetParameterByName(0, "gEnvMap");

	// Set effect parameters that do not vary.
	HR(mFX->SetTechnique(mhTech));
	HR(mFX->SetTexture(mhEnvMap, mEnvMap));
}

Sky::~Sky()
{
	SAFE_RELEASE(mSphere);
	SAFE_RELEASE(mEnvMap);
	SAFE_RELEASE(mFX);
}

void Sky::Draw()
{
	D3DXVECTOR3 posW=e_pCamera->GetPosition();
	D3DXMATRIX matWorld;
	D3DXMatrixTranslation(&matWorld,posW.x,posW.y,posW.z);
	D3DXMATRIX matViewProj;
	e_pCamera->GetViewProj(matViewProj);
	mFX->SetMatrix(mhWVP,&(matWorld*matViewProj));

	UINT numPasses=0;
	mFX->Begin(&numPasses,0);
	mFX->BeginPass(0);

	mSphere->DrawSubset(0);

	mFX->EndPass();
	mFX->End();
}

void Sky::DrawToCubemap(const D3DXMATRIX& WVP)
{
	mFX->SetMatrix(mhWVP,&WVP);

	UINT numPasses=0;
	mFX->Begin(&numPasses,0);
	mFX->BeginPass(0);

	mSphere->DrawSubset(0);

	mFX->EndPass();
	mFX->End();
}

void Sky::ChangeScene(const std::string& envmapFilename)
{
	SAFE_RELEASE(mEnvMap);
	D3DXCreateCubeTextureFromFile(e_pd3dDevice,envmapFilename.c_str(),&mEnvMap);
}

