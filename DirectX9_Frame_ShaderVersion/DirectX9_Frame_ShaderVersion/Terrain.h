#pragma  once
#include "Utility.h"
#include <fstream>

class Terrain
{
private:
	struct SubGrid
	{
		ID3DXMesh* mesh;
		BoundingBox box;

		// For sorting.
		bool operator<(const SubGrid& rhs)const;

		const static int NUM_ROWS  = 33;
		const static int NUM_COLS  = 33;
		const static int NUM_TRIS  = (NUM_ROWS-1)*(NUM_COLS-1)*2;
		const static int NUM_VERTS = NUM_ROWS*NUM_COLS;
	};

	std::vector<SubGrid> mSubGrids;

	Table<float> mHeightMap;

	DWORD mVertRows;
	DWORD mVertCols;
	float mDX;
	float mDZ;
	float mWidth;
	float mDepth;

	IDirect3DTexture9* mTex0;
	IDirect3DTexture9* mTex1;
	IDirect3DTexture9* mTex2;
	IDirect3DTexture9* mBlendMap;
	ID3DXEffect*			mFX;
	D3DXHANDLE         mhTech;
	D3DXHANDLE         mhViewProj;
	D3DXHANDLE         mhDirToSunW;
	D3DXHANDLE         mhTex0;
	D3DXHANDLE         mhTex1;
	D3DXHANDLE         mhTex2;
	D3DXHANDLE         mhBlendMap;


private:
	//about heightmap
	bool  inBounds(int i, int j);
	float sampleHeight3x3(int i, int j);
	void  filter3x3();
	void loadRAW(const std::string& filename, float heightScale, float heightOffset);

	void buildGeometry();
	void buildSubGridMesh(RECT& R, VertexPNT* gridVerts);

	void buildEffect();


public:
	Terrain(UINT vertRows, UINT vertCols, float dx, float dz, 
		std::string heightmap, std::string tex0, std::string tex1, 
		std::string tex2, std::string blendMap, float heightScale, 
		float yOffset);
	~Terrain();

	// (x, z) relative to terrain's local space.
	float getHeight(float x, float z);

	void setDirToSunW(const D3DXVECTOR3& d);

	void draw();

};
