#pragma  once
#include "Utility.h"

class Sky
{
private:
	ID3DXMesh* mSphere;
	float mRadius;
	IDirect3DCubeTexture9* mEnvMap;
	ID3DXEffect* mFX;
	D3DXHANDLE mhTech;
	D3DXHANDLE mhEnvMap;
	D3DXHANDLE mhWVP;

public:
	Sky(const std::string& envmapFilename);
	~Sky();

	void Draw();
	void DrawToCubemap(const D3DXMATRIX& WVP);
	void ChangeScene(const std::string& envmapFilename);
	IDirect3DCubeTexture9* getEnvMap() {return mEnvMap;}

};