#include "Terrain.h"
#include "Camera.h"//不在头文件中（Terrain.h）包含它是因为Camera.h中包含了Terrain.h，
							//如果放在头文件中会造成无限递归,
							//并且，需要的只是Camera类的一个全局对象，头文件并不需要关于它的类型
#include <list>

Terrain::Terrain(UINT vertRows, UINT vertCols, float dx, float dz, 
				 std::string heightmap, std::string tex0, std::string tex1, std::string tex2, 
				 std::string blendMap, float heightScale, float yOffset):
mVertRows(vertRows),mVertCols(vertCols),mDX(dx),mDZ(dz)
{
	mWidth=(vertCols-1)*dx;
	mDepth=(vertRows-1)*dz;

	loadRAW(heightmap,heightScale,yOffset);

	D3DXCreateTextureFromFile(e_pd3dDevice,tex0.c_str(),&mTex0);
	D3DXCreateTextureFromFile(e_pd3dDevice,tex1.c_str(),&mTex1);
	D3DXCreateTextureFromFile(e_pd3dDevice,tex2.c_str(),&mTex2);
	D3DXCreateTextureFromFile(e_pd3dDevice,blendMap.c_str(),&mBlendMap);

	buildGeometry();
	buildEffect();

	setDirToSunW(D3DXVECTOR3(0.0f, 1.0f, 0.0f));
}

Terrain::~Terrain()
{
	for(UINT i=0;i<mSubGrids.size();i++)
		SAFE_RELEASE(mSubGrids[i].mesh);

	SAFE_RELEASE(mTex0);
	SAFE_RELEASE(mTex1);
	SAFE_RELEASE(mTex2);
	SAFE_RELEASE(mBlendMap);
	SAFE_RELEASE(mFX);
}

float Terrain::getHeight(float x, float z)
{
	// Transform from terrain local space to "cell" space.
	float c = (x + 0.5f*mWidth) /  mDX;
	float d = (z - 0.5f*mDepth) / -mDZ;

	// Get the row and column we are in.
	int row = (int)floorf(d);
	int col = (int)floorf(c);

	// Grab the heights of the cell we are in.
	// A*--*B
	//  | /|
	//  |/ |
	// C*--*D
	float A = mHeightMap(row, col);
	float B = mHeightMap(row, col+1);
	float C = mHeightMap(row+1, col);
	float D = mHeightMap(row+1, col+1);

	// Where we are relative to the cell.
	float s = c - (float)col;
	float t = d - (float)row;

	// If upper triangle ABC.
	if(t < 1.0f - s)
	{
		float uy = B - A;
		float vy = C - A;
		return A + s*uy + t*vy;
	}
	else // lower triangle DCB.
	{
		float uy = C - D;
		float vy = B - D;
		return D + (1.0f-s)*uy + (1.0f-t)*vy;
	}
}

void Terrain::setDirToSunW(const D3DXVECTOR3& d)
{
	mFX->SetValue(mhDirToSunW, &d, sizeof(D3DXVECTOR3));
}

// Sort by distance from nearest to farthest from the camera.  In this
// way, we draw objects in front to back order to reduce overdraw 
// (i.e., depth test will prevent them from being processed further.
bool Terrain::SubGrid::operator<(const SubGrid& rhs) const
{
	D3DXVECTOR3 d1 = box._center - e_pCamera->GetPosition();
	D3DXVECTOR3 d2 = rhs.box._center - e_pCamera->GetPosition();
	return D3DXVec3LengthSq(&d1) < D3DXVec3LengthSq(&d2);
}

void Terrain::draw()
{
	// Frustum cull sub-grids.
	std::list<SubGrid> visibleSubGrids;
	for(UINT i = 0; i < mSubGrids.size(); ++i)
	{
		if( e_pCamera->IsVisible(mSubGrids[i].box) )
			visibleSubGrids.push_back(mSubGrids[i]);
	}

	// Sort front-to-back from camera.
	visibleSubGrids.sort();

	D3DXMATRIX matViewProj;
	e_pCamera->GetViewProj(matViewProj);
	//拓Camera类还要用到Terrian,两个类这样相互调用不好，容易无限递归
	//但只要不是两个方法互相调用，是不会造成这种现象的，相当于两个类连接成了一个类，耦合性很大而已
	mFX->SetMatrix(mhViewProj,&matViewProj);

	UINT numPasses = 0;
	mFX->Begin(&numPasses,0);
	mFX->BeginPass(0);

	for(std::list<SubGrid>::iterator iter = visibleSubGrids.begin(); iter != visibleSubGrids.end(); ++iter)
		HR(iter->mesh->DrawSubset(0));

	mFX->EndPass();
	mFX->End();
}


bool  Terrain::inBounds(int i, int j)
{
	return 
		i >= 0 && 
		i < (int)mHeightMap.numRows() && 
		j >= 0 && 
		j < (int)mHeightMap.numCols();
}

float Terrain::sampleHeight3x3(int i, int j)
{
	float sum = 0.0f;
	float num = 0.0f;

	for(int m = i-1; m <= i+1; ++m)
	{
		for(int n = j-1; n <= j+1; ++n)
		{
			if( inBounds(m,n) )
			{
				sum += mHeightMap(m,n);
				num += 1.0f;
			}
		}
	}
	return sum/num;
}

void  Terrain::filter3x3()
{
	Table<float> temp(mHeightMap.numRows(),mHeightMap.numCols());

	for(int i=0;i<mHeightMap.numRows();i++)
		for(int j=0;j<mHeightMap.numCols();j++)
			temp(i,j)=sampleHeight3x3(i,j);

	mHeightMap=temp;
}

void Terrain::loadRAW( const std::string& filename, float heightScale, float heightOffset)
{
	using namespace std;

	vector<unsigned char> in(mVertRows*mVertCols);
	// Open the file.
	std::ifstream inFile;
	inFile.open(filename.c_str(), ios_base::binary);
	if(!inFile) return;
	// Read the RAW bytes.
	inFile.read((char*)&in[0], (streamsize)in.size());
	// Done with file.
	inFile.close();

	mHeightMap.resize(mVertRows, mVertCols, 0);
	for(int i = 0; i < mVertRows; ++i)
	{
		for(int j = 0; j < mVertCols; ++j)
		{
			int k = i * mVertCols + j;
			mHeightMap(i, j) = (float)in[k] * heightScale + heightOffset;
		}
	}

	filter3x3();
}



void Terrain::buildGeometry()
{

	DWORD numTris  = (mVertRows-1)*(mVertCols-1)*2;
	DWORD numVerts = mVertRows*mVertCols;
	ID3DXMesh* mesh = 0;
	D3DXCreateMesh(numTris,numVerts,D3DXMESH_SYSTEMMEM|D3DXMESH_32BIT, VertexPNTElements, e_pd3dDevice, &mesh);

	std::vector<D3DXVECTOR3> verts;
	std::vector<DWORD> indices;
	GenTriGrid(mVertRows, mVertCols, mDX, mDZ, D3DXVECTOR3(0.0f, 0.0f, 0.0f), verts, indices);

	VertexPNT* v=0;
	mesh->LockVertexBuffer(0,(void**)&v);
	for(UINT i=0;i<mesh->GetNumVertices();i++)
	{
		int r = i / mVertCols;
		int c = i % mVertCols;

		v[i].pos   = verts[i];
		v[i].pos.y = mHeightMap(r, c);

		v[i].tex0.x = (v[i].pos.x + (0.5f*mWidth)) / mWidth;
		v[i].tex0.y = (v[i].pos.z - (0.5f*mDepth)) / -mDepth;
	}

	DWORD* indexBuffPtr = 0;
	mesh->LockIndexBuffer(0, (void**)&indexBuffPtr);
	for(UINT i = 0; i < mesh->GetNumFaces(); ++i)
	{
		indexBuffPtr[i*3+0] = indices[i*3+0];
		indexBuffPtr[i*3+1] = indices[i*3+1];
		indexBuffPtr[i*3+2] = indices[i*3+2];
	}
	mesh->UnlockIndexBuffer();

	// Compute Vertex Normals.
	D3DXComputeNormals(mesh, 0);

	//break the grid up into subgrid meshes.
	int subGridRows = (mVertRows-1) / (SubGrid::NUM_ROWS-1);
	int subGridCols = (mVertCols-1) / (SubGrid::NUM_COLS-1);

	for(int r = 0; r < subGridRows; ++r)
	{
		for(int c = 0; c < subGridCols; ++c)
		{
			// Rectangle that indicates (via matrix indices ij) the
			// portion of global grid vertices to use for this subgrid.
			RECT R = 
			{
				//左上顶点
				c * (SubGrid::NUM_COLS-1), //这样理解：c * (SubGrid::NUM_COLS-1)格+1-1，点==空+1，-1是因为从0开始计数
				r * (SubGrid::NUM_ROWS-1),
				//右下顶点
				(c+1) * (SubGrid::NUM_COLS-1),
				(r+1) * (SubGrid::NUM_ROWS-1)
			};

			buildSubGridMesh(R, v); 
		}
	}

	mesh->UnlockVertexBuffer();

	SAFE_RELEASE(mesh); // Done with global mesh.
}

void Terrain::buildSubGridMesh(RECT& R, VertexPNT* gridVerts)
{
	//===============================================================
	// Create the subgrid mesh.
	ID3DXMesh* subMesh = 0;
	D3DXCreateMesh(SubGrid::NUM_TRIS, SubGrid::NUM_VERTS, 
		D3DXMESH_MANAGED, VertexPNTElements, e_pd3dDevice, &subMesh);


	//===============================================================
	// Build Vertex Buffer.  Copy rectangle of vertices from the
	// grid into the subgrid structure.
	VertexPNT* v = 0;
	subMesh->LockVertexBuffer(0, (void**)&v);
	int k = 0;
	for(int i = R.top; i <= R.bottom; ++i)
	{
		for(int j = R.left; j <= R.right; ++j)
		{
			v[k++] = gridVerts[i*mVertCols+j];
		}
	}

	//===============================================================
	// Compute the bounding box before unlocking the vertex buffer.
	BoundingBox bndBox;
	D3DXComputeBoundingBox((D3DXVECTOR3*)v, subMesh->GetNumVertices(), 
		sizeof(VertexPNT), &bndBox._min, &bndBox._max);

	subMesh->UnlockVertexBuffer();


	//===============================================================
	// Build Index and Attribute Buffer.
	// Get indices for subgrid (we don't use the verts here--the verts
	// are given by the parameter gridVerts).
	std::vector<D3DXVECTOR3> tempVerts;
	std::vector<DWORD> tempIndices;
	GenTriGrid(SubGrid::NUM_ROWS, SubGrid::NUM_COLS, mDX, mDZ, 
		D3DXVECTOR3(0.0f, 0.0f, 0.0f), tempVerts, tempIndices);

	WORD* indices  = 0;
	DWORD* attBuff = 0;
	subMesh->LockIndexBuffer(0, (void**)&indices);
	subMesh->LockAttributeBuffer(0, &attBuff);
	for(int i = 0; i < SubGrid::NUM_TRIS; ++i)
	{
		indices[i*3+0] = (WORD)tempIndices[i*3+0];
		indices[i*3+1] = (WORD)tempIndices[i*3+1];
		indices[i*3+2] = (WORD)tempIndices[i*3+2];

		attBuff[i] = 0; // All in subset 0.
	}
	subMesh->UnlockIndexBuffer();
	subMesh->UnlockAttributeBuffer();


	//===============================================================
	// Optimize for the vertex cache and build attribute table.
	DWORD* adj = new DWORD[subMesh->GetNumFaces()*3];
	subMesh->GenerateAdjacency(EPSILON, adj);
	subMesh->OptimizeInplace(D3DXMESHOPT_VERTEXCACHE|D3DXMESHOPT_ATTRSORT,
		adj, 0, 0, 0);
	delete[] adj;


	//===============================================================
	// Save the mesh and bounding box.
	SubGrid g;
	g.mesh = subMesh;
	g.box  = bndBox;
	mSubGrids.push_back(g);
}



void Terrain::buildEffect()
{
	ID3DXBuffer *errors;
	HR(D3DXCreateEffectFromFile(e_pd3dDevice, "shader/Terrain.fx",
		0, 0, D3DXSHADER_DEBUG, 0, &mFX, &errors));
	if(errors)
		MessageBox(0, (char*)errors->GetBufferPointer(), 0, 0);

	mhTech      = mFX->GetTechniqueByName("TerrainTech");
	mhViewProj  = mFX->GetParameterByName(0, "gViewProj");
	mhDirToSunW = mFX->GetParameterByName(0, "gDirToSunW");
	mhTex0      = mFX->GetParameterByName(0, "gTex0");
	mhTex1      = mFX->GetParameterByName(0, "gTex1");
	mhTex2      = mFX->GetParameterByName(0, "gTex2");
	mhBlendMap  = mFX->GetParameterByName(0, "gBlendMap");

	HR(mFX->SetTexture(mhTex0, mTex0));
	HR(mFX->SetTexture(mhTex1, mTex1));
	HR(mFX->SetTexture(mhTex2, mTex2));
	HR(mFX->SetTexture(mhBlendMap, mBlendMap));
	HR(mFX->SetTechnique(mhTech));
}