#include "ParticleSystem.h"

PSystem::PSystem(
				const std::string& fxName, 
				const std::string& techName, 
				const std::string& texName, 
				const D3DXVECTOR3& accel, 
				const BoundingBox& box,
				int maxNumParticles, 
				float timePerParticle):
				mAccel(accel),mSystemBox(box),mTime(0.0f),
				mVBSize(2048),mVBBatchSize(512),mVBOffset(0),
				mMaxNumParticles(maxNumParticles),mTimePerParticle(timePerParticle)
{
	D3DVERTEXELEMENT9 ParticleElements[] = 
	{
		{0, 0,  D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		{0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
		{0, 24, D3DDECLTYPE_FLOAT1, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1},
		{0, 28, D3DDECLTYPE_FLOAT1, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2},
		{0, 32, D3DDECLTYPE_FLOAT1, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 3},
		{0, 36, D3DDECLTYPE_FLOAT1, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 4},
		{0, 40, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0},
		D3DDECL_END()
	};	
	e_pd3dDevice->CreateVertexDeclaration(ParticleElements,&m_pDecl);

	e_pd3dDevice->CreateVertexBuffer(mMaxNumParticles*sizeof(Particle),			//mVBSize
		D3DUSAGE_DYNAMIC|D3DUSAGE_WRITEONLY|D3DUSAGE_POINTS,
		0, D3DPOOL_DEFAULT, &m_pVB, 0);

	D3DXCreateTextureFromFile(e_pd3dDevice,texName.c_str(), &m_pTexture);

	// Create the FX.
	ID3DXBuffer* errors = 0;
	D3DXCreateEffectFromFile(e_pd3dDevice, fxName.c_str(),
		0, 0, D3DXSHADER_DEBUG, 0, &m_pFX, &errors);
	if( errors )
		MessageBox(0, (char*)errors->GetBufferPointer(), 0, 0);

	mhTech    = m_pFX->GetTechniqueByName(techName.c_str());
	mhWVP     = m_pFX->GetParameterByName(0, "gWVP");
	mhEyePosL = m_pFX->GetParameterByName(0, "gEyePosL");
	mhTex     =m_pFX->GetParameterByName(0, "gTex");
	mhTime    =m_pFX->GetParameterByName(0, "gTime");
	mhAccel   = m_pFX->GetParameterByName(0, "gAccel");
	mhViewportHeight = m_pFX->GetParameterByName(0, "gViewportHeight");

	// We don't need to set these every frame since they do not change.
	m_pFX->SetTechnique(mhTech);
	m_pFX->SetValue(mhAccel, mAccel, sizeof(D3DXVECTOR3));
	m_pFX->SetTexture(mhTex, m_pTexture);

	D3DXMatrixIdentity(&mWorldMat);
	D3DXMatrixIdentity(&mInvWorld);

	// Allocate memory for maximum number of particles.
	mParticles.resize(mMaxNumParticles);
	mAliveParticles.reserve(mMaxNumParticles);
	mDeadParticles.reserve(mMaxNumParticles);
	// They start off all dead.
	for(int i = 0; i < mMaxNumParticles; ++i)
	{
		mParticles[i].lifeTime = -1.0f;
		mParticles[i].initialTime = 0.0f;
	}

}


PSystem::~PSystem()
{
	SAFE_RELEASE(m_pFX);
	SAFE_RELEASE(m_pTexture);
	SAFE_RELEASE(m_pVB);
	SAFE_RELEASE(m_pDecl);
}

void PSystem::SetWorldMatrix(D3DXMATRIX& world)
{
	mWorldMat=world;
	D3DXMatrixInverse(&mInvWorld,0,&mWorldMat);
}

void PSystem::AddParticle()
{
	if(mDeadParticles.size()> 0)
	{
		Particle* p=mDeadParticles.back();
		InitialParticle(*p);
		mAliveParticles.push_back(p);
		mDeadParticles.pop_back();
	}
}

void PSystem::Update(float dt)
{
	mTime+=dt;

	// Rebuild the dead and alive list.  Note that resize(0) does
	// not deallocate memory (i.e., the capacity of the vector does
	// not change).
	mDeadParticles.resize(0);
	mAliveParticles.resize(0);
	for(int i=0;i<mMaxNumParticles;i++)
	{
		if(mTime-mParticles[i].initialTime>mParticles[i].lifeTime)
		{
			mDeadParticles.push_back(&mParticles[i]);
		}
		else
		{
			mAliveParticles.push_back(&mParticles[i]);
		}
	}

	// A negative or zero mTimePerParticle value denotes
	// not to emit any particles.
	if(mTimePerParticle>0)
	{
		static float timeAccum=0.0f;
		timeAccum+=dt;
		if(timeAccum>mTimePerParticle)
		{
			AddParticle();
			timeAccum-=mTimePerParticle;
		}
	}

}

void PSystem::Draw(HWND hwnd)
{
	// Get camera position relative to world space system and make it 
	// relative to the particle system's local system.
	D3DXVECTOR3 eyePos;
	e_pCamera->GetPosition(&eyePos);
	D3DXVec3TransformCoord(&eyePos,&eyePos,&mInvWorld);
	m_pFX->SetValue(mhEyePosL,&eyePos,sizeof(D3DXVECTOR3));
	m_pFX->SetFloat(mhTime,mTime);
	D3DXMATRIX viewProj;
	e_pCamera->GetViewProj(viewProj);
	m_pFX->SetMatrix(mhWVP,&(mWorldMat*viewProj));

	// Point sprite sizes are given in pixels,and we want they vary with the  viewport dimensions.
	RECT clientRect;
	GetClientRect(hwnd, &clientRect);
	m_pFX->SetInt(mhViewportHeight, clientRect.bottom);

	UINT numPasses=0;
	m_pFX->Begin(&numPasses,0);
	m_pFX->BeginPass(0);
	
	e_pd3dDevice->SetStreamSource(0,m_pVB,0,sizeof(Particle));
	e_pd3dDevice->SetVertexDeclaration(m_pDecl);

	BoundingBox systemBoxW;
	mSystemBox.LocalToWorld(systemBoxW,mWorldMat);
	if(e_pCamera->IsVisible(mSystemBox))
	{
		//-----------------------------------【 optimization version 】---------------------------------------
		//特么为什么programming pipeline下会出现闪烁甚至崩溃，而固定管线就可以这样优化？！

		//if(mVBOffset>mVBSize)
		//	mVBOffset=0;

		//Particle* p=0;
		//m_pVB->Lock(mVBOffset*sizeof(Particle),mVBBatchSize*sizeof(Particle),(void**)&p,D3DLOCK_DISCARD);
		//DWORD numParticlesInBatch=0;
		//for(int i=0;i<mAliveParticles.size();i++)
		//{
		//	*p=*mAliveParticles[i];
		//	p++;
		//	numParticlesInBatch++;

		//	if(numParticlesInBatch==mVBBatchSize)
		//	{
		//		m_pVB->Unlock();
		//		e_pd3dDevice->DrawPrimitive(D3DPT_POINTLIST,mVBOffset,mVBBatchSize);
		//		mVBOffset+=mVBBatchSize;
		//		if(mVBOffset>mVBSize)
		//			mVBOffset=0;
		//		m_pVB->Lock(mVBOffset*sizeof(Particle),mVBBatchSize*sizeof(Particle),(void**)&p,D3DLOCK_DISCARD);
		//		numParticlesInBatch=0;
		//	}
		//}

		//m_pVB->Unlock();
		//if(numParticlesInBatch>0)
		//{
		//	e_pd3dDevice->DrawPrimitive(D3DPT_POINTLIST,mVBOffset,numParticlesInBatch);
		//	mVBOffset+=numParticlesInBatch;
		//}
		//------------------------------------------------------------------------------------------------    


		//-----------------------------------【原版】---------------------------------------
		Particle* p = 0;
		m_pVB->Lock(0, 0, (void**)&p, D3DLOCK_DISCARD);
		int vbIndex = 0;

		// For each living particle.
		for(UINT i = 0; i < mAliveParticles.size(); ++i)
		{
			// Copy particle to VB
			p[vbIndex] = *mAliveParticles[i];
			++vbIndex;
		}
		m_pVB->Unlock();

		// Render however many particles we copied over.
		if(vbIndex > 0)
		{
			e_pd3dDevice->DrawPrimitive(D3DPT_POINTLIST, 0, vbIndex);
		}
		//------------------------------------------------------------------------------------------------    
	}


	m_pFX->EndPass();
	m_pFX->End();

}

//-----------------------------------【Rain】---------------------------------------
void Rain::InitialParticle(Particle& out)
{
	// Generate about the camera.
	out.initialPos = e_pCamera->GetPosition();

	// Spread the particles out on xz-plane.
	out.initialPos.x += GetRandomFloat(-100.0f, 100.0f);
	out.initialPos.z += GetRandomFloat(-100.0f, 100.0f);

	// Generate above the camera.
	out.initialPos.y += GetRandomFloat(50.0f, 55.0f);

	out.initialTime      = mTime;
	out.lifeTime        = GetRandomFloat(2.0f, 2.5f);
	out.initialColor    = WHITE;
	out.initialSize     = GetRandomFloat(6.0f, 7.0f);

	// Give them an initial falling down velocity.
	out.initialVelocity.x = GetRandomFloat(-1.5f, 0.0f);
	out.initialVelocity.y = GetRandomFloat(-50.0f, -45.0f);
	out.initialVelocity.z = GetRandomFloat(-0.5f, 0.5f);
}
//------------------------------------------------------------------------------------------------    


//-----------------------------------【Snow】---------------------------------------
void Snow::InitialParticle(Particle&out)
{
	// Generate about the camera.
	e_pCamera->GetPosition(&out.initialPos );

	// Spread the particles out on xz-plane.
	out.initialPos.x += GetRandomFloat(-100, 100);
	out.initialPos.z += GetRandomFloat(-100, 100);

	// Generate above the camera.
	out.initialPos.y += GetRandomFloat(50.0f, 55.0f);

	out.initialTime      = mTime;
	out.lifeTime        = GetRandomFloat(5.0f, 7.5f);
	out.initialColor    = WHITE;
	out.initialSize     = GetRandomFloat(20.0f,25.0f);

	// Give them an initial falling down velocity.
	out.initialVelocity.x = GetRandomFloat(-3.0f, 0.0f);
	out.initialVelocity.y = GetRandomFloat(-30.0f, -20.0f);
	out.initialVelocity.z = GetRandomFloat(-0.5f, 0.5f);

}

void Snow::Update(float dt)
{
	mTime+=dt;

	mAliveParticles.resize(0);
	mDeadParticles.resize(0);
	float currentCameraY=e_pCamera->GetPosition().y ;
	for(int i=0;i<mMaxNumParticles;i++)
	{
		mParticles[i].initialPos+=mParticles[i].initialVelocity*dt;
		if(mParticles[i].initialPos.y<currentCameraY-30)
		{
			mDeadParticles.push_back(&mParticles[i]);
		}
		else
		{
			mAliveParticles.push_back(&mParticles[i]);
		}

	}

	if(mTimePerParticle>0)
	{
		static float timeAccum=0;
		timeAccum+=dt;
		if(timeAccum>mTimePerParticle)
		{
			AddParticle();
			timeAccum-=mTimePerParticle;
		}
	}
}
//------------------------------------------------------------------------------------------------    


//-----------------------------------【FireWork】---------------------------------------
void FireWork::Reset()
{
	mAliveParticles.resize(0);
	mDeadParticles.resize(0);
	for(int i=0;i<mMaxNumParticles;i++)
	{
		InitialParticle(mParticles[i]);
		mAliveParticles.push_back(&mParticles[i]);
	}
}

void FireWork::InitialParticle(Particle& out)
{
	out.initialPos=mInitialPoint;
	GetRandomVector(out.initialVelocity,D3DXVECTOR3(-1.0f,-1.0f,-1.0f),D3DXVECTOR3(1.0f,1.0f,1.0f));
	D3DXVec3Normalize(&out.initialVelocity,&out.initialVelocity);//为了球形，各方向速度大小须相同
	out.initialColor=D3DXCOLOR(GetRandomFloat(0.0f,1.0f),GetRandomFloat(0.0f,1.0f),GetRandomFloat(0.0f,1.0f),1.0f);
	out.initialVelocity*=100.0f;
	out.initialTime = mTime;
	out.lifeTime = 2.0f;
	out.initialSize	=18.0f;
}

void FireWork::Update(float dt)
{
	PSystem::Update(dt);
	if(IsDead())
		Reset();
}
//------------------------------------------------------------------------------------------------    

//-----------------------------------【Trail】---------------------------------------
void HelixL(D3DXVECTOR3& p,float dt )
{
	static float t=0.0f;
	t+=dt;
	p.x=10.0f*cos(10.0f*t);
	p.y=10.0f*sin(10.0f*t);
	p.z=-10.0f*t;
}
void HelixS(D3DXVECTOR3& p,float dt )
{
	static float t=0.0f;
	t+=dt;
	p.x=5.0f*cos(0.5f*t);
	p.y=5.0f*sin(0.5f*t);
	p.z=-10.0f*t;
}

void HelixCircle(D3DXVECTOR3& p,float dt)
{
	static float t=0.0f;
	t+=dt;
	//假定R=30.0f,r=3.0f,待拓为宏定义
	D3DXVECTOR3 c;
	c.x=30.0f*cos(1.5f*t);
	c.y=30.0f*sin(1.5f*t);
	c.z=0.0f;
	D3DXVECTOR3 T,N;
	D3DXVec3Normalize(&T,&c);
	D3DXVec3Cross(&N,&T,&D3DXVECTOR3(0,0,-1));
	D3DXVec3Normalize(&N,&N);
	static float angle=0.0f;
	angle+=dt*60.0f;
	if(angle>2.0f*D3DX_PI)
		angle=0.0f;
	D3DXMATRIX R;
	D3DXMatrixRotationAxis(&R,&N,angle);
	D3DXVec3TransformCoord(&T,&T,&R);
	T*=3.0f;
	p=c+T;

}

void Trail::InitialParticle(Particle& out)
{
	out.initialPos=mPoint;
	out.initialTime=mTime;
	out.lifeTime=GetRandomFloat(2.0f,4.0f);
	out.initialSize=GetRandomFloat(10.0f,15.0f);
	out.mass=GetRandomFloat(0.0f, 2.0f);
}

void Trail::Update(float dt)
{
	PSystem::Update(dt);
	mPointUpdate(mPoint,dt);
}
//------------------------------------------------------------------------------------------------    