#include "FunctionModeRipple.h"
#include "Camera.h"
#include "DirectInput.h"

FunctionModeWater::FunctionModeWater():mAttenuation(0.59f),mWaveSpeed(80.0f),
	mNumVertsRow(128),mNumVertsCol(128),mGridStep(D3DXVECTOR2(20.0f,20.0f))
{
	D3DXMatrixIdentity(&mMatWorld);
	GenGridMesh(mWater,mNumVertsCol,mNumVertsRow,mGridStep.x,mGridStep.y,false);
	mHalfWidth=(mNumVertsRow-1)*mGridStep.x/2.0f;
	mHalfDepth=(mNumVertsCol-1)*mGridStep.y/2.0f;

	mDeadWave.resize(WAVE_BUFFER_SIZE);
	for(int i=0;i<WAVE_BUFFER_SIZE;i++)
		mDeadWave[i]=&mWaveBuffer[i];

	D3DXCreateCubeTextureFromFile(e_pd3dDevice,"Texture/CubeMap/CubeSceneForWater.dds",&mEnvMap);

	//实验性质
	e_pCamera->SetPosition(D3DXVECTOR3(300,750,0));
	mLight.ambient=D3DXCOLOR(0.55,0.55,0.55,1.0f);
	mLight.light=D3DXVECTOR4(1,-1,1,0.0f);

	mMtrl.ambient=D3DXCOLOR(0.1f,0.1f,0.1f,1.0f);
	mMtrl.specPower=16.0f;

	BuildFX();//哈哈，差点前加，数据都还没初始化
}

FunctionModeWater::~FunctionModeWater()
{
	SAFE_RELEASE(mWater);
	SAFE_RELEASE(mFX);
	SAFE_RELEASE(mEnvMap);
}

void FunctionModeWater::BuildFX()
{
	//water effect
	ID3DXBuffer *errors;
	HR(D3DXCreateEffectFromFile(e_pd3dDevice, "shader/RippleWater.fx",
		0, 0, D3DXSHADER_DEBUG, 0, &mFX, &errors));
	if(errors)
		MessageBox(0, (char*)errors->GetBufferPointer(), 0, 0);

	mhWorld=mFX->GetParameterByName(0,"gWorld");
	mhWorldInvTrans=mFX->GetParameterByName(0,"gWorldInvTrans");
	mhWVP=mFX->GetParameterByName(0,"gWVP");
	mhLight=mFX->GetParameterByName(0,"gLight");
	mhMtrl=mFX->GetParameterByName(0,"gMtrl");
	mhEyePosW=mFX->GetParameterByName(0,"gEyePosW");
	mhEnvMap=mFX->GetParameterByName(0,"gEnvMap");
	mhWaveArray=mFX->GetParameterByName(0,"gWaves");
	mhWaveSpeed=mFX->GetParameterByName(0,"gWaveSpeed");

	mFX->SetTechnique("FunctionModeSimulation");
	mFX->SetValue(mhLight,&mLight,sizeof(Light));
	mFX->SetValue(mhMtrl,&mMtrl,sizeof(Mtrl));
	mFX->SetMatrix(mhWorld,&mMatWorld);
	mFX->SetMatrix(mhWorldInvTrans,&mMatWorld);
	mFX->SetTexture(mhEnvMap,mEnvMap);
	mFX->SetFloat(mhWaveSpeed,mWaveSpeed);

}

void FunctionModeWater::Push(float x,float z,float depth)
{
	if(!mDeadWave.empty())
	{
		std::vector<Wave*>::iterator final=mDeadWave.end()-1;//哈哈，当stack来用
		(**final).pos=D3DXVECTOR2(x,z);
		(**final).amp=depth;
		(**final).w=GetRandomFloat(3.0f,5.0f);
		(**final).age=0;
		(**final).alive=1;
		mDeadWave.pop_back();
	}



}

void FunctionModeWater::Update(float dt)
{
	for(int i=0;i<WAVE_BUFFER_SIZE;i++)
	{
		if(mWaveBuffer[i].alive>0)
		{
			mWaveBuffer[i].amp*=pow(mAttenuation,dt);
			if(mWaveBuffer[i].amp<0.01f)
			{
				mWaveBuffer[i].alive=false;
				mDeadWave.push_back(&mWaveBuffer[i]);
				continue;
			}
			mWaveBuffer[i].age+=dt;
		}
		
	}
	mFX->SetValue(mhWaveArray,mWaveBuffer,sizeof(mWaveBuffer));


	//实验性质，待实现鼠标pick
	static bool isKeyDown=false;
	if(e_pDInput->IsKeyDown(DIK_P))
	{
		if(!isKeyDown)
		{
			isKeyDown=true;
			Push(GetRandomFloat(-mHalfWidth/2.0f,mHalfWidth/2.0f),GetRandomFloat(-mHalfDepth/2.0f,mHalfDepth/2.0f),GetRandomFloat(18.0f,20.0f));
		}
	}
	else
		isKeyDown=false;


}

void FunctionModeWater::Draw()
{
	D3DXMATRIX matViewProj;
	e_pCamera->GetViewProj(matViewProj);
	mFX->SetMatrix(mhWVP,&(mMatWorld*matViewProj));
	mFX->SetValue(mhEyePosW,&(e_pCamera->GetPosition()),sizeof(D3DXVECTOR3));

	UINT numPasses=0;
	mFX->Begin(&numPasses,0);
	mFX->BeginPass(0);
	mWater->DrawSubset(0);
	mFX->EndPass();
	mFX->End();

}