//-----------------------------------【引用头文件】---------------------------------------
#include <time.h>
#include "Utility.h"
#include "DirectInput.h"
#include "Camera.h"
#include "ArcBall.h"
#include "ParticleSystem.h"
#include "Terrain.h"
#include "Teapot.h"
#include "Sky.h"
#include "Water.h"
#include "ShadowMapping.h"
#include "ShadowVolume.h"
#include "InteractionalWater.h"
#include "FunctionModeRipple.h"
#include "WaveParticles.h"
//------------------------------------------------------------------------------------------------    

//-----------------------------------【引用库文件】---------------------------------------
#pragma comment(lib,"winmm.lib")//调用PlaySound函数所需库文件
#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")
//------------------------------------------------------------------------------------------------    

//-----------------------------------【宏定义】---------------------------------------
#define  WINDOW_TITTLE _T("hehe")
#define  WNDCLASS_NAME _T("haha")
//------------------------------------------------------------------------------------------------    

//-----------------------------------【全局变量声明】---------------------------------------
LPD3DXFONT g_pFont=NULL;
PSystem *g_pParticles=NULL;
Terrain *g_pTerrain=NULL;
Sky* g_pSky=NULL;
Teapot* g_pteapot=NULL;
Water* g_pWater=NULL;
ShadowMapDemo* g_pShadowMapDemo=NULL;
SVDemo* g_pShadowVolumeDemo=NULL;
RippleWater* g_pRippleWater=nullptr;
FunctionModeWater* g_pFunModeWater=nullptr;
WaveParticleSystem* g_pWaveParticleSystem=nullptr;
//------------------------------------------------------------------------------------------------    

//-----------------------------------【函数声明】---------------------------------------
LRESULT  CALLBACK  WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam);
HRESULT  Direct3D_Init(HWND hwnd);
LRESULT  Object_Init(HINSTANCE hInstance,HWND hwnd);
void Direct3D_Update(float timeDelta);
void  Direct3D_Render(HWND hwnd,float timeDelta);
void  Direct3D_CleanUp();
//------------------------------------------------------------------------------------------------    




int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nShowCmd)
{

	WNDCLASSEX wndClass={0};//是c结构体，一定要初始化！不然会注册失败
	wndClass.cbSize=sizeof(WNDCLASSEX);
	wndClass.style=CS_HREDRAW|CS_VREDRAW;
	wndClass.lpfnWndProc=WndProc;
	wndClass.cbClsExtra=0;
	wndClass.cbWndExtra=0;
	wndClass.hInstance=hInstance;
	wndClass.hIcon=(HICON)::LoadImage(NULL,_T("icon.ico"),IMAGE_ICON,0,0,LR_DEFAULTSIZE|LR_LOADFROMFILE);
	wndClass.hCursor=LoadCursor(NULL,IDC_ARROW);
	wndClass.hbrBackground=(HBRUSH)GetStockObject(GRAY_BRUSH);
	wndClass.lpszMenuName=NULL;
	wndClass.lpszClassName=WNDCLASS_NAME;

	if(!RegisterClassEx(&wndClass))
		return -1;

	HWND hwnd=CreateWindow(WNDCLASS_NAME,WINDOW_TITTLE,WS_OVERLAPPEDWINDOW,0,0,
		WINDOW_WIDTH,WINDOW_HEIGHT,NULL,NULL,hInstance,NULL);
	MoveWindow(hwnd,250,80,WINDOW_WIDTH,WINDOW_HEIGHT,true);
	ShowWindow(hwnd,nShowCmd);
	UpdateWindow(hwnd);

	//PlaySound(_T("音频文件名.wav"),NULL,SND_FILENAME|SND_ASYNC|SND_LOOP);

	Direct3D_Init(hwnd);
	Object_Init(hInstance,hwnd);


	MSG msg={0};
	__int64 cntsPerSec=0;
	QueryPerformanceFrequency((LARGE_INTEGER*)&cntsPerSec);
	float secsPerCnt=1.0f/(float)cntsPerSec;
	__int64 prevCnts,currentCnts;
	float dt;
	QueryPerformanceCounter((LARGE_INTEGER*)&prevCnts);
	while (msg.message!=WM_QUIT)
	{
		if (PeekMessage(&msg,0,0,0,PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			QueryPerformanceCounter((LARGE_INTEGER*)&currentCnts);
			dt=(currentCnts-prevCnts)*secsPerCnt;
			Direct3D_Update(dt);
			Direct3D_Render(hwnd,dt);
			prevCnts=currentCnts;
		}
	}

	UnregisterClass(WNDCLASS_NAME,wndClass.hInstance);
	return 0;
}

LRESULT  CALLBACK  WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam)
{
	//e_pCamera->HandleMessages(hwnd,message,wParam,lParam);//如果不if，连窗口都不会出现，因为消息处理函数比Object_Init()先被调用
	if(e_pCamera)
		e_pCamera->HandleMessages(hwnd,message,wParam,lParam);

	switch (message)
	{
	case   WM_KEYDOWN:
		if(wParam==VK_ESCAPE)
			DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		Direct3D_CleanUp();
		PostQuitMessage(0);
		break;
		//-----------------------------------【测试用】---------------------------------------

		//------------------------------------------------------------------------------------------------    

	default:
		return DefWindowProc(hwnd,message,wParam,lParam);
	}

	return 0;
}

HRESULT  Direct3D_Init(HWND hwnd)
{
	//创建D3D接口
	LPDIRECT3D9		PD3D=NULL;
	if(NULL==(PD3D=Direct3DCreate9(D3D_SDK_VERSION)))
		return E_FAIL;

	//获取硬件设备信息
	D3DCAPS9 caps;
	int vp=0;
	if(FAILED(PD3D->GetDeviceCaps(D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL,&caps) ) )
		return E_FAIL;
	if(caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT)
		vp=D3DCREATE_HARDWARE_VERTEXPROCESSING;
	else
		vp=D3DCREATE_SOFTWARE_VERTEXPROCESSING;

	// Check for vertex shader version 2.0 support.
	if( caps.VertexShaderVersion < D3DVS_VERSION(2, 0) )
		return false;

	// Check for pixel shader version 2.0 support.
	if( caps.PixelShaderVersion < D3DPS_VERSION(2, 0) )
		return false;


	//填充D3DPRESENT_PARAMETERS结构体
	D3DPRESENT_PARAMETERS d3dpp;
	ZeroMemory(&d3dpp,sizeof(d3dpp));
	d3dpp.BackBufferWidth=WINDOW_WIDTH;
	d3dpp.BackBufferHeight=WINDOW_HEIGHT;
	d3dpp.BackBufferFormat=D3DFMT_A8R8G8B8;
	d3dpp.BackBufferCount=1;
	d3dpp.MultiSampleType=D3DMULTISAMPLE_NONE;
	d3dpp.MultiSampleQuality=0;
	d3dpp.SwapEffect=D3DSWAPEFFECT_DISCARD;
	d3dpp.hDeviceWindow=hwnd;
	d3dpp.Windowed=true;
	d3dpp.EnableAutoDepthStencil=true;
	d3dpp.AutoDepthStencilFormat=D3DFMT_D24S8;
	d3dpp.Flags=0;
	d3dpp.FullScreen_RefreshRateInHz=0;
	d3dpp.PresentationInterval=D3DPRESENT_INTERVAL_IMMEDIATE;

	//创建D3D设备
	if(FAILED(PD3D->CreateDevice(D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL,hwnd,vp,&d3dpp,&e_pd3dDevice) ) )
		return E_FAIL;

	SAFE_RELEASE(PD3D);
	return S_OK;
}


LRESULT  Object_Init(HINSTANCE hInstance,HWND hwnd)
{

	srand((unsigned int)time(0));

	D3DXCreateFont(e_pd3dDevice, 28, 0, 1000, 0, false, DEFAULT_CHARSET, 
		OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, 0, _T("微软雅黑"), &g_pFont); 
	
	e_hMainWnd=hwnd;
	e_pDInput=new DirectInput(hInstance,hwnd);

	e_pArcball=new ArcBall(WINDOW_WIDTH,WINDOW_HEIGHT);

	//e_pCamera=new FPLandCamera();
	e_pCamera=new FPAirCamera();
	//e_pCamera=new ArcballCamera(30,D3DXVECTOR3(0,0,0));

	D3DXCreateTextureFromFile(e_pd3dDevice,"Texture/whitetex.dds",&e_pWhiteTexture);

	g_pParticles=new Rain("shader/rain.fx", "RainTech", "Texture/Particles/raindrop.dds", D3DXVECTOR3(-1.0f, -9.8f, 0.0f),  4000, 0.001f);
	//g_pParticles=new Snow("shader/Snow.fx","SnowTech","Texture/Particles/snowflake.dds",3000,0.001f);
	//g_pParticles=new FireWork("shader/FireWork.fx","FireWork","Texture/Particles/flare.bmp",3000,D3DXVECTOR3(0,150,0) );
	//g_pParticles=new Trail(HelixCircle,"shader/Trail.fx","Trail","Texture/Particles/torch.dds",3000,0.001f );
	g_pTerrain=new Terrain(257, 257, 10.0f, 10.0f, 
		"Texture/Terrain/castlehm257.raw",  
		"Texture/Terrain/grass.dds",
		"Texture/Terrain/dirt.dds",
		"Texture/Terrain/stone.dds",
		"Texture/Terrain/blend_castle.dds",
		3.0f, 0.0f);

	g_pSky=new Sky("Texture/CubeMap/grassenvmap1024.dds");
	g_pteapot=new Teapot();
	g_pteapot->SetEnvMap(g_pSky->getEnvMap());
	Water::InitInfo initinfo;
	InitWater(initinfo,true);
	g_pWater=new Stream(initinfo);
	g_pWater->setEnvMap(g_pSky->getEnvMap());
	//InitWater(initinfo,false);
	//g_pWater=new Sea(initinfo);

	g_pShadowMapDemo=new ShadowMapDemo();
	g_pShadowVolumeDemo=new SVDemo();

	g_pRippleWater=new RippleWater();
	g_pFunModeWater=new FunctionModeWater();
	g_pWaveParticleSystem=new WaveParticleSystem();

	return S_OK;
}

void Direct3D_Update(float timeDelta)
{
	//-----------------------------------【utility objects】---------------------------------------
	//注意顺序
	e_pDInput->Poll();
	e_pCamera->Update(timeDelta,g_pTerrain);
	//------------------------------------------------------------------------------------------------    

	//g_pParticles->Update(timeDelta);
	//g_pteapot->Update(timeDelta);
	//g_pWater->update(timeDelta);
	//g_pShadowMapDemo->Update(timeDelta);
	//g_pShadowVolumeDemo->Update(timeDelta);
	g_pRippleWater->Update(timeDelta);
	//g_pFunModeWater->Update(timeDelta);
	//g_pWaveParticleSystem->Update(timeDelta);
}

void  Direct3D_Render( HWND hwnd,float timeDelta)
{
	e_pd3dDevice->Clear(0,NULL,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,BLACK,1.0f,0);//最开始没有添加D3DCLEAR_ZBUFFER的清理，静态还好，动态的时候老是出现逐渐被黑色吞没的问题……快哭了
	e_pd3dDevice->BeginScene();

	g_pSky->Draw();

	//g_pteapot->Draw(g_pSky);

	//g_pTerrain->draw();
	//g_pWater->draw();

	//g_pShadowMapDemo->Draw();
	//g_pShadowVolumeDemo->Draw();

	//g_pParticles->Draw(hwnd);//粒子最后画，alpha融合
		
	g_pRippleWater->Draw();
	//g_pFunModeWater->Draw();
	//g_pWaveParticleSystem->Draw();

	ShowFPS(g_pFont,D3DCOLOR_XRGB(180,96,96),WINDOW_WIDTH,WINDOW_HEIGHT,timeDelta,true);

	e_pd3dDevice->EndScene();
	e_pd3dDevice->Present(NULL,NULL,NULL,NULL);

	ScreenShot();
}


void  Direct3D_CleanUp()
{
	SAFE_DELETE(g_pShadowVolumeDemo);
	SAFE_DELETE(g_pShadowMapDemo);
	SAFE_DELETE(g_pWater);
	SAFE_DELETE(g_pteapot);
	SAFE_DELETE(g_pParticles);
	SAFE_DELETE(g_pTerrain);
	SAFE_DELETE(g_pSky);
	SAFE_DELETE(g_pRippleWater);
	SAFE_DELETE(g_pFunModeWater);
	SAFE_DELETE(g_pWaveParticleSystem);
	SAFE_RELEASE(g_pFont);

	SAFE_RELEASE(e_pWhiteTexture);
	SAFE_DELETE(e_pCamera);
	SAFE_DELETE(e_pArcball);
	SAFE_DELETE(e_pDInput)
	SAFE_RELEASE(e_pd3dDevice);
}
