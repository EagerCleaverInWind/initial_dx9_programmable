#pragma  once
#include "Utility.h"
#include "DirectInput.h"
#include "Terrain.h"

//-----------------------------------【base】---------------------------------------
class	 Camera
{
protected:
	D3DXVECTOR3			m_up;
	D3DXVECTOR3			m_look;
	D3DXVECTOR3			m_right;
	D3DXVECTOR3			m_position;
	D3DXMATRIX			m_ViewMatrix;
	D3DXMATRIX			m_ProjMatrix;

	D3DXPLANE m_FrustumPlanes[6]; // [0] = near,[1] = far,[2] = left,[3] = right,[4] = top,[5] = bottom

protected:
	void	CalculateViewMatrix();
	void	UpdateWorldFrustumPlanes();

public:
	void	SetPosition(D3DXVECTOR3	new_position);
	void	SetLens(float fov, float aspect, float nearZ, float farZ);

	void	LookAt(D3DXVECTOR3& pos, D3DXVECTOR3& target, D3DXVECTOR3& up);

	virtual void	Update(float dt,Terrain* terrain)=0;

	//for son class ArcballCamera
	virtual void  HandleMessages(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam){}
	virtual D3DXMATRIX GetObjMatWorld()
	{
		D3DXMATRIX E;
		D3DXMatrixIdentity(&E);
		return E;
	}
	virtual void SetTarget(D3DXVECTOR3 target){}

	void	GetViewMatrix(D3DXMATRIX &out){out=m_ViewMatrix;}
	void	GetProjMatrix(D3DXMATRIX &out){out=m_ProjMatrix;}
	void	GetViewProj(D3DXMATRIX &out){out=m_ViewMatrix*m_ProjMatrix;}
	void	GetPosition(D3DXVECTOR3 *out) { *out=m_position; }
	D3DXVECTOR3		GetPosition() { return m_position;}
	D3DXVECTOR3 	GetLookDirection() {return m_look;}
	D3DXVECTOR3		GetUpVec() {return m_up;}
	D3DXVECTOR3		GetRightVec() {return m_right;}

	bool IsVisible(const BoundingBox &box);
public:
	Camera();
	~Camera();
};
extern Camera* e_pCamera;
//------------------------------------------------------------------------------------------------    



//-----------------------------------【air craft】---------------------------------------
class FPAirCamera:public Camera
{
private:
	float m_speed;

public:
	//会先自动调用父类构造函数
	FPAirCamera():m_speed(80.0f)
	{

	}

	void	Update(float dt,Terrain* terrain);

	void	SetSpeed(float v) {m_speed=v;}
};
//------------------------------------------------------------------------------------------------    



//-----------------------------------【land object】---------------------------------------
class FPLandCamera:public Camera
{
private:
	float m_speed;
	float m_height; //offset from the ground if there is the terrain to interact with 
	bool m_isJump;
	float m_jumpPower;
	D3DXVECTOR3 m_Vy;

public:
	FPLandCamera():m_speed(50.0f),m_height(5.0f),
		m_isJump(false),m_jumpPower(250),m_Vy(D3DXVECTOR3(0,0,0))
	{

	}

	void Update(float dt,Terrain* terrain);

	void	SetSpeed(float v) {m_speed=v;}
	void  SetHeight(float h) {m_height=h;}
	void  SetJumpPower(float v) {m_jumpPower=v;}
};
//------------------------------------------------------------------------------------------------    



//-----------------------------------【Arcball show】---------------------------------------
#define  MAX_DISTANCE  200
#define  MIN_DISTANCE  5
class ArcballCamera:public Camera
{
private:
	//for object being showed
	D3DXMATRIX  mObjMatWorld;
	bool mRotateObject;
	D3DXVECTOR3		mObjBeginPoint;
	D3DXVECTOR3		mObjCurrentPoint;
	D3DXQUATERNION		mObjLastQuaternion;
	D3DXQUATERNION		mObjCurrentQuaternion;
	D3DXMATRIX mViewInverse;

	//for camera 
	float mDistance;
	D3DXVECTOR3 mTarget;
	bool mRotateCamera;
	D3DXVECTOR3		mCamBeginPoint;
	D3DXVECTOR3		mCamCurrentPoint;
	D3DXQUATERNION		mCamLastQuaternion;
	D3DXQUATERNION		mCamCurrentQuaternion;

	float mWindowWidth;
	float mWindowHeight;

private:
	void CalculateCamera();
	void CalculateObject();

public:
	ArcballCamera(float distance,D3DXVECTOR3 target);

	void Update(float dt,Terrain* terrain);
	void  HandleMessages(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam);

	//以下方法并不能通过e_pCamera调用，待统一。或者有没有更好的设计模式？！
	D3DXMATRIX GetObjMatWorld(){return mObjMatWorld;}
	void SetTarget(D3DXVECTOR3 target);
};

//------------------------------------------------------------------------------------------------    


