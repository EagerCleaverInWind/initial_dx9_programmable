#include "Teapot.h"
#include "Camera.h"

Teapot::Teapot()
{
	D3DXCreateTeapot(e_pd3dDevice,&mTeapot,0);
	genSphericalTexCoords();
	BuildFX();

	LoadXFile("Mesh/Airplane/airplane 2.x",&mOrbiter,mOrbMtrls,mOrbTextures);
	D3DXMatrixTranslation(&mOrb1InitWorld,0.0f,15.0f,0.0f);
	D3DXMatrixTranslation(&mOrb2InitWorld,-15.0f,0.0f,0.0f);

	e_pd3dDevice->CreateCubeTexture( ENVMAPSIZE,1,D3DUSAGE_RENDERTARGET,
		D3DFMT_A16B16G16R16F,D3DPOOL_DEFAULT,&mCubeTex,NULL );
	e_pd3dDevice->CreateDepthStencilSurface( ENVMAPSIZE,	ENVMAPSIZE,
		D3DFMT_A8R8G8B8,D3DMULTISAMPLE_NONE,
		0,TRUE,&mDepthStencilSurface,NULL ) ;
}

Teapot::~Teapot()
{
	SAFE_RELEASE(mTeapot);
	SAFE_RELEASE(mOrbiter);
	for(int i=0;i<mOrbTextures.size();i++)
		SAFE_RELEASE(mOrbTextures[i]);
	SAFE_RELEASE(mCubeTex);
	SAFE_RELEASE(mDepthStencilSurface);
}

void Teapot::BuildFX()
{
	ID3DXBuffer *errors;
	HR(D3DXCreateEffectFromFile(e_pd3dDevice, "shader/teapot.fx",
		0, 0, D3DXSHADER_DEBUG, 0, &mFX, &errors));
	if(errors)
		MessageBox(0, (char*)errors->GetBufferPointer(), 0, 0);

	mhWorld=mFX->GetParameterByName(0,"gWorld");
	mhWorldInvTrans=mFX->GetParameterByName(0,"gWorldInvTrans");
	mhWVP=mFX->GetParameterByName(0,"gWVP");
	mhLight=mFX->GetParameterByName(0,"gLight");
	mhMtrl=mFX->GetParameterByName(0,"gMtrl");
	mhEyePosW=mFX->GetParameterByName(0,"gEyePosW");
	mhTex0=mFX->GetParameterByName(0,"gTex");
	mhEnvMap=mFX->GetParameterByName(0,"gEnvMap");
	
	mFX->SetValue(mhLight,&mLight,sizeof(Light));
}

void Teapot::genSphericalTexCoords()
{
	// D3DXCreate* functions generate vertices with position 
	// and normal data.  But for texturing, we also need
	// tex-coords.  So clone the mesh to change the vertex
	// format to a format with tex-coords.
	ID3DXMesh* temp = 0;
	HR(mTeapot->CloneMesh(D3DXMESH_SYSTEMMEM, 
		VertexPNTElements, e_pd3dDevice, &temp));

	SAFE_RELEASE(mTeapot);

	// Now generate texture coordinates for each vertex.
	VertexPNT* vertices = 0;
	HR(temp->LockVertexBuffer(0, (void**)&vertices));

	for(UINT i = 0; i < temp->GetNumVertices(); ++i)
	{
		// Convert to spherical coordinates.
		D3DXVECTOR3 p = vertices[i].pos;

		float theta = atan2f(p.z, p.x);
		float phi   = acosf(p.y / sqrtf(p.x*p.x+p.y*p.y+p.z*p.z));

		// Phi and theta give the texture coordinates, but are not in 
		// the range [0, 1], so scale them into that range.

		float u = theta / (2.0f*D3DX_PI);
		float v = phi   / D3DX_PI;

		// Save texture coordinates.

		vertices[i].tex0.x = u;
		vertices[i].tex0.y = v;
	}
	HR(temp->UnlockVertexBuffer());

	// Clone back to a hardware mesh.
	HR(temp->CloneMesh(D3DXMESH_MANAGED | D3DXMESH_WRITEONLY,
		VertexPNTElements, e_pd3dDevice, &mTeapot));

	SAFE_RELEASE(temp);
}

D3DXMATRIX Teapot::GetCubeMapViewMatrix( DWORD dwFace )
{
	D3DXVECTOR3 vEyePt = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	D3DXVECTOR3 vLookDir;
	D3DXVECTOR3 vUpDir;

	switch( dwFace )
	{
	case D3DCUBEMAP_FACE_POSITIVE_X:
		vLookDir = D3DXVECTOR3( 1.0f, 0.0f, 0.0f );
		vUpDir = D3DXVECTOR3( 0.0f, 1.0f, 0.0f );
		break;
	case D3DCUBEMAP_FACE_NEGATIVE_X:
		vLookDir = D3DXVECTOR3( -1.0f, 0.0f, 0.0f );
		vUpDir = D3DXVECTOR3( 0.0f, 1.0f, 0.0f );
		break;
	case D3DCUBEMAP_FACE_POSITIVE_Y:
		vLookDir = D3DXVECTOR3( 0.0f, 1.0f, 0.0f );
		vUpDir = D3DXVECTOR3( 0.0f, 0.0f, -1.0f );
		break;
	case D3DCUBEMAP_FACE_NEGATIVE_Y:
		vLookDir = D3DXVECTOR3( 0.0f, -1.0f, 0.0f );
		vUpDir = D3DXVECTOR3( 0.0f, 0.0f, 1.0f );
		break;
	case D3DCUBEMAP_FACE_POSITIVE_Z:
		vLookDir = D3DXVECTOR3( 0.0f, 0.0f, 1.0f );
		vUpDir = D3DXVECTOR3( 0.0f, 1.0f, 0.0f );
		break;
	case D3DCUBEMAP_FACE_NEGATIVE_Z:
		vLookDir = D3DXVECTOR3( 0.0f, 0.0f, -1.0f );
		vUpDir = D3DXVECTOR3( 0.0f, 1.0f, 0.0f );
		break;
	}

	// Set the view transform for this cubemap surface
	D3DXMATRIXA16 mView;
	D3DXMatrixLookAtLH( &mView, &vEyePt, &vLookDir, &vUpDir );
	return mView;
}

void Teapot::SetEnvMap(IDirect3DCubeTexture9* envmap)
{
	mFX->SetTexture(mhEnvMap,envmap);
}

void Teapot::SetLight(Light& newLight)
{
	mLight=newLight;
}

void Teapot::SetMtrl(Mtrl& newMtrl)
{
	mMtrl=newMtrl;
}

void Teapot::Update(float dt)
{
	static float t = 0.0f;
	t += 1.5f*dt;
	if( t >= 2.0f*D3DX_PI )
		t = 0.0f;

	D3DXMATRIX Rx;
	D3DXMatrixRotationX(&Rx,-t);
	mOrb1MatWorld=mOrb1InitWorld*Rx;

	D3DXMATRIX Ry;
	D3DXMatrixRotationY(&Ry, -t);
	mOrb2MatWorld=mOrb2InitWorld*Ry;
}

void Teapot::Draw(Sky* pSky)
{ 
	//render to cubeTexture
	LPDIRECT3DSURFACE9 pRTOld = NULL;
	e_pd3dDevice->GetRenderTarget( 0, &pRTOld );
	LPDIRECT3DSURFACE9 pDSOld = NULL;
	e_pd3dDevice->GetDepthStencilSurface( &pDSOld );
	e_pd3dDevice->SetDepthStencilSurface( mDepthStencilSurface);
	mFX->SetTechnique("RenderOrbitersInMirror");
	// The projection matrix has a FOV of 90 degrees and asp ratio of 1
	D3DXMATRIXA16 mProj;
	D3DXMatrixPerspectiveFovLH( &mProj, D3DX_PI * 0.5f, 1.0f, 0.01f, 100.0f );
	for(int nFace = 0; nFace < 6; ++nFace)
	{
		LPDIRECT3DSURFACE9 pSurf;
		mCubeTex->GetCubeMapSurface( ( D3DCUBEMAP_FACES )nFace, 0, &pSurf );
		e_pd3dDevice->SetRenderTarget( 0, pSurf );
		SAFE_RELEASE( pSurf );
		D3DXMATRIX matView=GetCubeMapViewMatrix(nFace);
		D3DXMATRIX matViewProj=matView*mProj;

		e_pd3dDevice->Clear(0,NULL,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,BLACK,1.0f,0);
		e_pd3dDevice->BeginScene();

		//render Sky
		pSky->DrawToCubemap(matViewProj);
		//render orbiters
		DrawOrbiters(matViewProj);

		e_pd3dDevice->EndScene();
	}
	//��ԭ�� render to back buffer
	if( pDSOld )
	{
		e_pd3dDevice->SetDepthStencilSurface( pDSOld );
		SAFE_RELEASE( pDSOld );
	}
	e_pd3dDevice->SetRenderTarget( 0, pRTOld );
	SAFE_RELEASE( pRTOld );


	//render teapot
	mFX->SetTechnique("TeapotTech");
	mFX->SetTexture(mhEnvMap,mCubeTex);

	D3DXMATRIX matWorld=e_pCamera->GetObjMatWorld();
	mFX->SetMatrix(mhWorld,&matWorld);

	D3DXMATRIX matWorldInvTrans;
	D3DXMatrixInverse(&matWorldInvTrans,NULL,&matWorld);
	D3DXMatrixTranspose(&matWorldInvTrans,&matWorldInvTrans);
	mFX->SetMatrix(mhWorldInvTrans,&matWorldInvTrans);

	D3DXMATRIX matViewProj;
	e_pCamera->GetViewProj(matViewProj);
	mFX->SetMatrix(mhWVP,&(matWorld*matViewProj));

	mFX->SetValue(mhEyePosW,&e_pCamera->GetPosition(),sizeof(D3DXVECTOR3));
	mFX->SetValue(mhMtrl,&mMtrl,sizeof(Mtrl));
	UINT numPass=0;
	mFX->Begin(&numPass,0);
	mFX->BeginPass(0);

	mTeapot->DrawSubset(0);

	mFX->EndPass();
	mFX->End();


	//render orbiters
	mFX->SetTechnique("RenderOrbiters");
	DrawOrbiters(matViewProj);

}

void Teapot::DrawOrbiters(D3DXMATRIX& matViewProj)
{
	UINT numPasses=0;
	mFX->Begin(&numPasses,0);
	mFX->BeginPass(0);

	for(int i=0;i<mOrbMtrls.size();i++)
	{
		mFX->SetValue(mhMtrl,&mOrbMtrls[i],sizeof(Mtrl));
		if(mOrbTextures[i]!=0)
			mFX->SetTexture(mhTex0,mOrbTextures[i]);
		else
			mFX->SetTexture(mhTex0,e_pWhiteTexture);

		D3DXMATRIX matWIT=mOrb1MatWorld;
		D3DXMatrixInverse(&matWIT,0,&matWIT);
		D3DXMatrixTranspose(&matWIT,&matWIT);
		mFX->SetMatrix(mhWorld,&mOrb1MatWorld);
		mFX->SetMatrix(mhWorldInvTrans,&matWIT);
		mFX->SetMatrix(mhWVP,&(mOrb1MatWorld*matViewProj));
		mFX->CommitChanges();
		mOrbiter->DrawSubset(i);

		matWIT=mOrb2MatWorld;
		D3DXMatrixInverse(&matWIT,0,&matWIT);
		D3DXMatrixTranspose(&matWIT,&matWIT);
		mFX->SetMatrix(mhWorld,&mOrb2MatWorld);
		mFX->SetMatrix(mhWorldInvTrans,&matWIT);
		mFX->SetMatrix(mhWVP,&(mOrb2MatWorld*matViewProj));
		mFX->CommitChanges();
		mOrbiter->DrawSubset(i);
	}

	mFX->EndPass();
	mFX->End();
}